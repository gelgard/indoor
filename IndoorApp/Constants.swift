//
//  Constants.swift
//  IndoorApp
//
//  Created by Oleg on 14.12.15.
//  Copyright © 2015 UranCompany. All rights reserved.
//

import Foundation
import SpriteKit
import CoreLocation



//MARK: Enum settings


  enum SettingRotate : Int {
    case map = 0
    case onlyMarker = 1
  }
  
  enum SettingSelectObject : Int {
    case withTouch = 0
    case inLook = 1
  }
  
  enum SettingKeys : String {
    case rotate = "rotate"
    case showDebugPanel = "showDebugPanel"
    case selectObject = "selectObject"
  }
  
  //MARK: Enum
  
  enum ActionState {
    case go, stop
  }
  
  enum BeaconDataSource {
    case beaconData,debugData,notSet
  }
  
  enum DialogType {
    case error, edit, warning, info, notice
    func image()->String {
      switch self {
      case .error:
        return "error"
      case .edit:
        return "edit"
      case .warning:
        return "warning"
      case .notice:
        return "notice"
      case .info:
        return "info"
      }
    }
  }
  
  enum JsonTypes {
    case intType, stringType, doubleType, layerType, objectType, paramType, pointType, dictType, arrType
  }
  
  enum MarkersTypes {
    case lookMarker, locationMarker, realPosMarker
  }
  
  enum PlanType:Int {
    case building = 0
    case pavilion = 1
    case openSpace = 2
    
  }
  
  enum LayerType:Int {
    case buildingPlan = 0
    case infoObjects = 1
    case routesGrid = 2
    case empty = 3
    
    func getZPosition() -> CGFloat {
      switch self {
      case .buildingPlan :
        return 50
      case .infoObjects :
        return 100
      case .routesGrid :
        return 20
      case .empty :
        return 0
      }
    }
  }
  
  
  enum LayerFormat:Int {
    case objects = 0
    case grid = 1
  }
  
  enum ObjectType:Int {
    case wall = 1
    case window = 2
    case door = 3
    case barrier = 4
    case stairs = 5
    case pavilion = 1001
    case informationDesk = 1002
    case showRoom = 1003
    case conferenceHall = 1004
    case scene = 1005
    case shop = 1006
    case wc = 1007
    case foodCourt = 1008
    case beacon = 1009
    case wifiPoint = 1010
    case room = 1011
    case exhibit = 1012
    case grid = 2000
    
    func isSelectable() -> Bool
    {
      switch self
      {
      case .pavilion :
        return true
      case .exhibit :
        return true
      default:
        return false
        
      }
    }
    
  }
  
  //Don`t forget add in getParamValue
  enum ParamsId:String
  {
    case Exit = "1"
    case Name = "1001"
    case Description = "1002"
    case Sponsor = "1003"
    case Model = "1004"
    case OpenTime = "1005"
    case CloseTime = "1006"
    case Images = "1007"
    case GroupColor = "1008"
    case Index = "1009"
    case UUID = "1010"
    case Minor = "1011"
    case Major = "1012"
    case Latitude = "1013"
    case Longitude = "1014"
    case ParentObject = "1015"
    case TxPower = "1016"
  }
  
  enum ParamsVal {
    case exit(Bool)
    case name(String)
    case description(String)
    case sponsor(Bool)
    case model(String)
    case openTime(Date)
    case closeTime(Date)
    case images(NSArray)
    case groupColor(UIColor)
    case index(Int)
    case uuid(String)
    case minor(Int)
    case major(Int)
    case latitude(String)
    case longitude(String)
    case parentObject(Int)
    case txPower(Int)
    
    case defVal
  }
  
  //MARK: JSONFields
  let jf_plan_plan : KeyType = ("plan", JsonTypes.stringType)
  let jf_plan_type : KeyType = ("type", JsonTypes.intType)
  let jf_plan_unit : KeyType = ("unit", JsonTypes.stringType)
  let jf_plan_width : KeyType = ("plan_width", JsonTypes.intType)
  let jf_plan_base_angle : KeyType = ("base_angle", JsonTypes.intType)
  let jf_plan_beacon_major : KeyType = ("beacon_major", JsonTypes.intType)
  let jf_plan_height : KeyType = ("plan_length", JsonTypes.intType)
  let jf_plan_layers : KeyType = ("layers", JsonTypes.dictType)
  
  let plan_block : JsonKeyParams = ("", [jf_plan_plan,jf_plan_type,jf_plan_unit,jf_plan_width,jf_plan_height,jf_plan_layers])
  
  
  let jf_layers_layerId : KeyType = ("layer_id", JsonTypes.intType)
  let jf_layers_type : KeyType = ("type", JsonTypes.intType)
  let jf_layers_index : KeyType = ("index",JsonTypes.intType)
  let jf_layers_format : KeyType = ("format", JsonTypes.intType)
  let jf_layers_unit : KeyType = ("unit",JsonTypes.stringType)
  let jf_layers_objects : KeyType = ("objects",JsonTypes.dictType)
  let jf_layers_step : KeyType = ("grid_step",JsonTypes.intType)
  let jf_layers_filling : KeyType = ("grid_filling",JsonTypes.arrType)
  
  let layers_block : JsonKeyParams = ("layers", [jf_layers_layerId,jf_layers_type,jf_layers_index,jf_layers_format,jf_layers_unit,jf_layers_objects])
  
  
  let jf_object_id : KeyType = ("id", JsonTypes.intType)
  let jf_parent_id : KeyType = ("parent_id", JsonTypes.intType)
  let jf_object_type : KeyType = ("type",JsonTypes.intType)
  let jf_object_params : KeyType = ("params",JsonTypes.dictType)
  let jf_object_coord : KeyType = ("coordinates",JsonTypes.arrType)
  
  let jf_debug_points : KeyType = ("points",JsonTypes.arrType)
  let jf_debug_data : KeyType = ("data",JsonTypes.dictType)
  let jf_debug_beaconData : KeyType = ("beaconData",JsonTypes.arrType)
  
  
  let object_block : JsonKeyParams = ("objects", [jf_object_id,jf_object_type,jf_object_params,jf_object_coord])
  
  //MARK: Notifications constants
  
  let errorNotification = "ErrorNotification"
  let errorNotificationSelector: Selector = Selector(("errorReceived:"))
  let updateLogNotification = "UpdateLog"
  let updateLogNotificationSelector: Selector = Selector(("updateLog:"))
  
  let enterBeaconRegion : String = "EnterBeaconRegion"
  let enterBeaconRegionSelector: Selector = Selector(("beaconRegionInside:"))
  let leaveBeaconRegion : String = "LeaveBeaconRegion"
  let leaveBeaconRegionSelector: Selector = Selector(("beaconRegionOutside:"))
  let nearByBeacon : String = "NearByBeacon"
  let nearByBeaconSelector: Selector = Selector(("nearByBeaconFnc:"))
  let unselectBeacon : String = "UnselectBeacon"
  let unselectBeaconSelector: Selector = Selector(("unselectBeaconFnc:"))
  
  //MARK: Enum Error
  
  
  
  enum InitError : Error {
    case negativeId
    case negativeIndex
    case notEnoughCoordinates
    case nullWidth
    case nullHeight
    case absetParamsId
    case incorectParamsColor
    case impossibleCreateImmutable
    
  }
  
  enum ConstructorError: IErrorTypeWithNSError {
    case objectNotBeacon, absentBaseParametrs, creationError;
    func getAsNSError() -> NSError {
      switch self {
      case .objectNotBeacon:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_OBJECT_NOT_BEACON".localized])
      case .absentBaseParametrs:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_ABSENT_PARAMETR_BEACON".localized])
      case .creationError:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_CONSTRUCTOR".localized])
      }
    }
  }
  
  enum JSONError: IErrorTypeWithNSError {
    case readingError,parsingError,fileAbsent,unknownField,paramsIdNotString,incorrectParamsValue;
    func getAsNSError() -> NSError
    {
      switch self
      {
      case .fileAbsent:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_TEST_JSON_ABSENT".localized])
      case .paramsIdNotString:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_PARAMS_ID_NOT_STRING".localized])
      case .incorrectParamsValue:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_PARAMS_INCORRECT_VALUE".localized])
      case .unknownField:
        return NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_UNKNOWN_FIELD".localized])
      default:
        return capError
      }
    }
  }
  
  
  enum ErrorCheckColor: IErrorTypeWithNSError {
    case incorrectRGBValue,incorrectRGBCount,incorrectRGBType;
    func getAsNSError() -> NSError
    {
      switch self
      {
      case .incorrectRGBValue:
        return NSError(domain: errDomain, code: 8001, userInfo: [NSLocalizedDescriptionKey : "ERR_INCORRECT_RGB_VALUE".localized])
      case .incorrectRGBCount:
        return NSError(domain: errDomain, code: 8002, userInfo: [NSLocalizedDescriptionKey : "ERR_INCORRECT_RGB_COUNT".localized])
      case .incorrectRGBType:
        return NSError(domain: errDomain, code: 8003, userInfo: [NSLocalizedDescriptionKey : "ERR_INCORRECT_RGB_TYPE".localized])
      }
    }
  }
  
  enum ErrorBeacons: IErrorTypeWithNSError {
    case monitoringNotRonning;
    func getAsNSError() -> NSError
    {
      switch self
      {
      case .monitoringNotRonning:
        return NSError(domain: errDomain, code: 9001, userInfo: [NSLocalizedDescriptionKey : "ERR_MONITORING_NOT_RUNNING".localized])
      }
    }
  }
  
  enum ErrorPlanObject: IErrorTypeWithNSError {
    case layerWithThisTypeAbsent, nodeBuildingFailed;
    func getAsNSError() -> NSError {
      switch self {
      case .layerWithThisTypeAbsent:
        return NSError(domain: errDomain, code: 9001, userInfo: [NSLocalizedDescriptionKey : "ERR_LAYER_ABSENT".localized])
      case .nodeBuildingFailed:
        return NSError(domain: errDomain, code: 9001, userInfo: [NSLocalizedDescriptionKey : "ERR_NODE_FAILED".localized])
      }
    }
  }
  
  
  
  //MARK: - Constants
  let textCellIdentifier = "DebugCell"
  let kCollectionSize = CGSize(width: 300, height: 180)
  let prefixRootNodes = "parent_"
  let segues = ["motion","ibeacon","calibrate","settings", "option 2", "option 3"]
  let seguesСaptions = ["Sensors data","iBeacon","Calibrate","Settings", "option 2", "option 3"]
  let minCoordinates = 4
  let panVelocity : CGFloat = 1.5
  let errDomain = "indoorDomainErr"
  let capError = NSError(domain: errDomain, code: 1001, userInfo: [NSLocalizedDescriptionKey : "ERR_UNKNOWN".localized])
  let infoPanelHeightValue : CGFloat = 200.0
  
  let unknErrorMessage : String = "ERR_UNKNOWN".localized+"\(#file) -  \(#line)"
  let unknownError = NSError(domain: errDomain, code: 9001, userInfo: [NSLocalizedDescriptionKey : unknErrorMessage])
  let kDebugWidth = 300
  
  //Locationmarker
  
  //MARK - Debug Constant
  
  struct DebugConstant {
    static let productFlag = "product"
    static let testFlag = "test"
  }
  //MARK - Navifation constants
  
  struct CalculationConstant {
    static let nearestBeaconsCount = 4
    static let algorithmVersion = "1.0.0"
    static let factorKalman : CGFloat = 0.8
    static let infinitePath : Int = 99999
    static let velocity  = 100.0
    static let planId = 5 //HARDCODE
    static let maxRelocation = 500.0
    static let showCalculationLog = false
    static let updatePeriod = 3.0
    static let updateHeadingPeriod = 2.0
    static let maxFakePoints = 2
  }
  
  //MARK - Debug JSON constants
  
  struct JSONConstant {
    static let planIdKey = "planId"
    static let dateKey = "date"
    static let algVersionKey = "algorithmVersion"
    static let algConfigKey = "algorithmConfig"
    static let beaconCountKey = "nearestBeaconsCount"
    static let kalmanKey = "factorKalman"
    static let periodKey = "updatePeriod"
    static let maxRelocationKey = "maxRelocation"
    static let dataKey = "data"
    static let pointsKey = "points"
    static let pathKey = "path"
    static let beacDataKey = "beaconData"
    static let resultPointKey = "resultPoint"
    static let startCalcKey = "timestampBeginCalculate"
    static let endCalcKey = "timestampEndCalculate"
    static let diffTimeKey = "calcTime"
    static let moveKey = "move"
    static let startActionKey = "startAction"
    static let endActionKey = "endActions"
    static let durationActionKey = "durationAction"
    static let stopPointKey = "stopPoint"
    static let pointId = "pointId"
  }
  
  struct BeaconConstant {
    static let uuid = UUID(uuidString: "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0")!
    static let beaconIdentifier = "com.uran"
    //static let major : CLBeaconMajorValue = 2
  }
  
  struct MarkersConstants {
    static let kLookRadius : CGFloat = 300.0
    static let kLookStartRadius : CGFloat = 165.0
    static let kLookEndRadius : CGFloat = 15.0
    static let kLocationInnerRadius : CGFloat = 15.0
    static let kLocationOuterRadius : CGFloat = 100.0
    static let kRealMarkerRadius : CGFloat = 25.0
  }
  
  
  
  
  
  
  //let hardcodeJSON = "testJson"
  //let hardcodeJSON = "museum_demo"
  
  
  let hardcodeJSON = "gelgard_flat_full"
  //let hardcodeJSON = "museum_exhibit"
  //let hardcodeJSON = "office_exhibit"
  
  let debugJSON = "debug_34"



