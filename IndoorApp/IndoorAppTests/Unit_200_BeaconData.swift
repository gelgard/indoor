//
//  Unit_200_BeaconData.swift
//  IndoorApp
//
//  Created by Oleg on 03.05.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import XCTest
import CoreLocation
import IndoorEngine
import IndoorModel


@testable import IndoorApp



class Unit_200_BeaconData: XCTestCase {
    
    
    var storyboard : UIStoryboard?
    var vc : VCBeacon?
    
    override func setUp()
    {
        storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: type(of: self)))
        vc = storyboard!.instantiateViewController(withIdentifier: "ibeaconVC") as! VCBeacon
        super.setUp()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    func test_1_getDataFromBeacon()
    {
        XCTAssertNotNil(vc, "iBeacon VC not exist")
        vc!.viewDidLoad()
        
        XCTAssertNotNil(vc!.hardwareData, "Absent beacon Data")
        let major : CLBeaconMajorValue = 2
        
        vc!.hardwareData?.startMonitoringHardwareWith(BeaconConstant.uuid, major: major, minor: nil, stepCouting: false)
        vc!.hardwareData?.stopMonitoringHardware()
        XCTAssertFalse(vc!.hardwareData!.isMonitoring, "Should be false")
    }

}
