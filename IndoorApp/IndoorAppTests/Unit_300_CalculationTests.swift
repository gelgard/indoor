//
//  Unit_300_CalculationTests.swift
//  IndoorApp
//
//  Created by Oleg on 25.05.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import XCTest
import IndoorEngine
import IndoorModel



class Unit_300_CalculationTests : XCTestCase {
  
  var plan : IPlan?
  var beaconData : iHardwareData?
  var mutablePlan : MutablePlan?
  var position : Position!
//  
//  override func setUp() {
//    
//    let planBuilder : PlanBuilder = PlanBuilder()
//    do {
//      mutablePlan = try planBuilder.getPlan(hardcodeJSON)
//      
//      do {
//        self.plan = try mutablePlan!.getImmutablePlan()
//        beaconData = HardwareDataManager(beaconsInLayer: (self.plan?.beaconsInInfoLayer)!)
//        
//      } catch let error as IErrorTypeWithNSError
//      {
//        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: error.getAsNSError())
//      } catch
//      {
//        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: IndError.unknownError)
//      }
//    } catch let error as IErrorTypeWithNSError
//    {
//      NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: error.getAsNSError())
//    } catch
//    {
//      NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: IndError.unknownError)
//    }
//    
//    do {
//      position = try Position(plan: self.plan!,vc: UIViewController())
//    } catch let error as IErrorTypeWithNSError
//    {
//      NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: error.getAsNSError())
//    } catch
//    {
//      NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: IndError.unknownError)
//    }
//    
//    super.setUp()
//    // Put setup code here. This method is called before the invocation of each test method in the class.
//  }
//  
//  override func tearDown() {
//    // Put teardown code here. This method is called after the invocation of each test method in the class.
//    super.tearDown()
//  }
//
//  
//  func test_1_getLocationTests() {
//    
//    let demoBeaconsData : [BeaconsData] = [(1,16.68) , (2 , 35.94) , (8 , 87.99) , (5 , 113.65) , (7 , 316.23) , (6 , 464.16) , (1 , 92.43) , (2 , 45.95) , (8 , 87.99) , (5 , 90.07) , (4 , 244.84) , (6 , 318.72) , (7 , 381.69) , (2 , 49.62) , (5 , 88.97) , (1 , 139.17) , (4 , 244.84) , (8 , 99.89) , (6 , 411.73) , (7 , 414.54) , (2 , 30.49) , (1 , 64.78) , (5 , 102.29) , (8 , 105.15) , (6 , 299.84) , (2 , 25.13) , (1 , 66.08) , (8 , 71.81) , (5 , 91.4) , (6 , 233.27) , (4 , 314.11) , (2 , 28.31) , (8 , 66.81) , (5 , 67.79) , (1 , 76.43) , (6 , 202.46) , (4 , 331.13) , (7 , 394.66)];
//    
//    
//    
//    do {
//      
//      let loc = position.getLocation(demoBeaconsData, maxRelocation: 300, stepCount: 0)
//      
//      print("\(loc.x) : \(loc.y)")
//      
//      XCTAssertEqual(loc, CGPoint(x: 675, y: 375))
//      
//      
//      
//    } catch {
//      
//      XCTFail()
//      
//    }
//    
//  }
//  
//  
//  
//  func test_2_ForPathLength() {
//    
//    let path18 = position.pathLength((14,12), endGridCell: (16,12))
//    
//    let path1 = position.pathLength((10,10), endGridCell: (10,10))
//    
//    let path999 = position.pathLength((10,10), endGridCell: (15,10))
//    
//    
//    
//    XCTAssertTrue(path18 == 18 && path1 == 1 && path999 > 999)
//    
//    
//    
//  }
//  
//  func test_3_correctPosition() {
//    let cp = position.correctPosition(CGPoint(x: 675, y: 675), currentPosition: CGPoint(x: 660, y: 585),maxRelocation: 300)
//    
//    XCTAssertTrue(cp.x == 675 && cp.y == 675)
//  }
//  
//  func test_4_nextFreeCell() {
//    
//    let cell1 = position.nextFreeCell((14,12), currentCell: (16,12))
//    
//    let cell2 = position.nextFreeCell((10,10), currentCell: (10,10))
//    
//    
//    
//    XCTAssertTrue(cell1.x == 16 && cell1.y == 11 && cell2.x == 11 && cell2.y == 10)
//    
//    
//  }
//  
//  func test_5_anotherCorrectPosition() {
//    let cp1 = position.correctPosition(CGPoint(x: 675,y: 675), currentPosition: CGPoint(x: 660, y: 585),maxRelocation: 300)
//    let cp2 = position.correctPosition(CGPoint(x: 675,y: 460), currentPosition: CGPoint(x: 730, y: 360),maxRelocation: 300)
//    let cp3 = position.correctPosition(CGPoint(x: 970,y: 80), currentPosition: CGPoint(x: 990, y: 720),maxRelocation: 300)
//    let cp4 = position.correctPosition(CGPoint(x: 480,y: 460), currentPosition: CGPoint(x: 610, y: 460),maxRelocation: 300)
//    
//    print("X : \(cp1.x) Y: \(cp1.y)")
//    print("X : \(cp2.x) Y: \(cp2.y)")
//    print("X : \(cp3.x) Y: \(cp3.y)")
//    print("X : \(cp4.x) Y: \(cp4.y)")
//    
//    XCTAssertTrue((cp1.x == 675 && cp1.y == 675), "Incorrect calculation for cp1")
//    XCTAssertTrue((cp2.x == 675 && cp2.y == 425), "Incorrect calculation for cp2")
//    XCTAssertTrue((cp3.x == 975 && cp3.y == 75), "Incorrect calculation for cp1")
//    XCTAssertTrue((cp4.x == 625 && cp4.y == 475), "Incorrect calculation for cp1")
//  }
//  
//  func test_6_intersection_test() {
//    let intr1 = NodeIntersection.isIntersect(x1: 600, y1: 100, R: 100, axisAngle: 70, startAngle: 150, endAngle: 30, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr1 == false, "Incorrect intersection")
//    let intr2 = NodeIntersection.isIntersect(x1: 600, y1: 100, R: 250, axisAngle: 80, startAngle: 170, endAngle: 30, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr2 == true, "Incorrect intersection")
//    let intr4 = NodeIntersection.isIntersect(x1: 500, y1: 200, R: 100, axisAngle: 50, startAngle: 140, endAngle: 50, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr4 == true, "Incorrect intersection")
//    let intr3 = NodeIntersection.isIntersect(x1: 500, y1: 200, R: 100, axisAngle: 0, startAngle: 100, endAngle: 80, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr3 == false, "Incorrect intersection")
//    let intr5 = NodeIntersection.isIntersect(x1: 500, y1: 500, R: 100, axisAngle: 70, startAngle: 150, endAngle: 30, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr5 == false, "Incorrect intersection")
//    
//    let intr11 = NodeIntersection.isIntersect(x1: 600, y1: 100, R: 100, axisAngle: 70, startAngle: 150, endAngle: 30, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr11 == false, "Incorrect intersection")
//    let intr12 = NodeIntersection.isIntersect(x1: 600, y1: 100, R: 250, axisAngle: 80, startAngle: 170, endAngle: 30, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr12 == true, "Incorrect intersection")
//    let intr13 = NodeIntersection.isIntersect(x1: 500, y1: 200, R: 100, axisAngle: 0, startAngle: 100, endAngle: 80, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr13 == false, "Incorrect intersection")
//    let intr14 = NodeIntersection.isIntersect(x1: 500, y1: 200, R: 100, axisAngle: 50, startAngle: 140, endAngle: 50, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr14 == true, "Incorrect intersection")
//    
//    let intr15 = NodeIntersection.isIntersect(x1: 500, y1: 500, R: 100, axisAngle: 70, startAngle: 150, endAngle: 30, x2: 373, y2: 254, w: 167, h: 180)
//    XCTAssert(intr15 == false, "Incorrect intersection")
//    
//    let intr1_2012 = NodeIntersection.isIntersect(x1: 619.91943359375, y1: 227.989562988281, R: 300, axisAngle: 90, startAngle: 165, endAngle: 15, x2: 371.149993896484, y2: 252.149993896484, w: 170.699996948242, h: 183.699996948242)
//    XCTAssert(intr1_2012 == true, "Incorrect intersection")
//  
//    let intr1_2013 = NodeIntersection.isIntersect(x1: 619.91943359375, y1: 227.989562988281, R: 300, axisAngle: 0, startAngle: 165, endAngle: 15, x2: 649.150024414062, y2: 434.149993896484, w: 56.7000007629395, h: 153.699996948242)
//    XCTAssert(intr1_2013 == true, "Incorrect intersection")
//    
//    let intr2_2012 = NodeIntersection.isIntersect(x1: 478.605804443359, y1: 507.7470703125, R: 300, axisAngle: 7.04641497304601, startAngle: 165, endAngle: 15, x2: 371.149993896484, y2: 252.149993896484, w: 170.699996948242, h: 183.699996948242)
//    XCTAssert(intr2_2012 == true, "Incorrect intersection")
//    
//    let intr2_2013 = NodeIntersection.isIntersect(x1: 478.605804443359, y1: 507.7470703125, R: 300, axisAngle: 7.04641497304601, startAngle: 165, endAngle: 15, x2: 649.150024414062, y2: 434.149993896484, w: 56.7000007629395, h: 153.699996948242)
//    XCTAssert(intr2_2013 == true, "Incorrect intersection")
//    
//    let intr3_2012 = NodeIntersection.isIntersect(x1: 625.36767578125, y1: 376.955261230469, R: 300, axisAngle: 210.199280938529, startAngle: 165, endAngle: 15, x2: 370.649993896484, y2: 251.649993896484, w: 170.699996948242, h: 184.699996948242)
//    XCTAssert(intr3_2012 == true, "Incorrect intersection")
//    
//    let intr3_2013 = NodeIntersection.isIntersect(x1: 625.36767578125, y1: 376.955261230469, R: 300, axisAngle: 210.199280938529, startAngle: 165, endAngle: 15, x2: 649.150024414062, y2: 434.149993896484, w: 56.7000007629395, h: 153.699996948242)
//    XCTAssert(intr3_2013 == false, "Incorrect intersection")
//  
//  }
}
