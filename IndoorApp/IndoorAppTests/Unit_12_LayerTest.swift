//
//  Unit_12_LayerTest.swift
//  IndoorApp
//
//  Created by Oleg on 02.01.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import XCTest
import IndoorEngine
import IndoorModel




class Unit_12_LayerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_1_createLayer()
    {
        do
        {
            let layer : ILayer = try Layer(layer_id: 0, index: 0, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)
            XCTAssertNotNil(layer,"Incorrect Object Init")
            XCTAssertEqual(layer.layer_id, 0,"Incorrect Layer Id")
            XCTAssertEqual(layer.index, 0,"Incorrect Index")
            XCTAssertEqual(layer.type, IndTypes.LayerType.buildingPlan,"Incorrect Type")
            XCTAssertEqual(layer.format, IndTypes.LayerFormat.objects,"Incorrect Format")
        } catch
        {
            
        }
        
    }
    
    func test_2_createLayerWithIncorrectLayerId()
    {
        do
        {
            try Layer(layer_id: -10, index: 0, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)
            
        } catch let error as IndError.InitError
        {
            XCTAssertEqual(error, IndError.InitError.negativeId,"Error not catched")
        }
        catch
        {
            XCTFail("Unknown Error")
        }
        
    }
    
    func test_3_createLayerWithIncorrectLayerIndex()
    {
        do
        {
            try Layer(layer_id: 0, index: -10, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)
            
        } catch let error as IndError.InitError
        {
            XCTAssertEqual(error, IndError.InitError.negativeIndex,"Error not catched")
        }
        catch
        {
            XCTFail("Unknown Error")
        }
        
    }
    
        func test_4_addObjects()
        {
            do
            {
                
                let layer = try Layer(layer_id: 0, index: 0, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)

                layer.addObject(try Object(objectId: 0, type: IndTypes.ObjectType.wall))
                layer.addObject(try Object(objectId: 1, type: IndTypes.ObjectType.wall))
                layer.addObject(try Object(objectId: 2, type: IndTypes.ObjectType.wall))
                XCTAssertEqual(layer.objects.count, 3,"Incorrect Count")
            }
            catch
            {
                XCTFail("Unknown Error")
            }
        }
    
    
    
    
}
