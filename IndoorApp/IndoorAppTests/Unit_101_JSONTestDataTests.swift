//
//  Unit_10_JSONTestDataTests.swift
//  Motivator
//
//  Created by Oleg on 08.09.15.
//  Copyright (c) 2015 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import XCTest
import IndoorEngine
import IndoorModel



class Unit_101_JSONTestDataTests: XCTestCase {
    
    
    
    
    override func setUp() {
        super.setUp()
        
    }
    
    func test_1_ThatThatJsonFileExist()
    {
        let path = Bundle.main.path(forResource: hardcodeJSON, ofType: "json")
        let filemgr = FileManager.default
        
        XCTAssertTrue(filemgr.fileExists(atPath: path!), "JSON file absent")
    }
    
    func test_2_ThatThatJsonIsCorrect()
    {
        do
        {
            _ = try JsonHelper().jsonAsArray(hardcodeJSON)
        } catch
        {
            XCTFail("Incorrect JSON")
        }

        
        
    }
    
    func test_3_ThatAllPlanFieldCorrectly()
    {
        let jsonHelper = JsonHelper()
        
        do
        {
            let jsonResultArr = try jsonHelper.jsonAsArray(hardcodeJSON)
            
            do
            {
                let name = try jsonHelper.getFieldValue(IndJSONFields.jf_plan_plan, dict: jsonResultArr)
    
                XCTAssertNotNil(name, "Absent value for FIELD: \(IndJSONFields.jf_plan_plan.key)")
            } catch
            {
                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_plan_plan.key)")
            }
            
            do
            {
                let type =  try jsonHelper.getFieldValue(IndJSONFields.jf_plan_type, dict: jsonResultArr)
                XCTAssertNotNil(type, "Absent value for FIELD: \(IndJSONFields.jf_plan_type.key)")
            } catch
            {
                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_plan_type.key)")
            }
            
            
            do
            {
                let width = try jsonHelper.getFieldValue(IndJSONFields.jf_plan_width, dict: jsonResultArr)
                XCTAssertNotNil(width, "Absent value for FIELD: \(IndJSONFields.jf_plan_width.key)")
            } catch
            {
                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_plan_width.key)")
            }
            
            do
            {
                let height = try jsonHelper.getFieldValue(IndJSONFields.jf_plan_height, dict: jsonResultArr)
                XCTAssertNotNil(height, "Absent value for FIELD: \(IndJSONFields.jf_plan_height.key)")
            } catch
            {
                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_plan_height.key)")
            }
            

            
            
        }
        catch
        {
            XCTFail("Parsing plan failed.")
        }
    }
    
    func test_3_ThatAllLayersFieldCorrectly()
    {
        let jsonHelper = JsonHelper()
        
        
        do
        {
            let jsonResultArr = try jsonHelper.jsonAsArray(hardcodeJSON)
            print(jsonResultArr)
            do
            {
                let layersDict = try jsonHelper.getFieldValue(IndJSONFields.jf_plan_layers, dict: jsonResultArr)
              print(layersDict)
                let layersArray = layersDict as! [NSDictionary]
                XCTAssertEqual(layersArray.count, 3, "Incorrect layers count")
                XCTAssertNotNil(layersDict, "Absent value for FIELD: \(IndJSONFields.jf_plan_layers.key)")
                
                for dict in layersArray
                {
                    do
                    {
                        let layer_id = try jsonHelper.getFieldValue(IndJSONFields.jf_layers_layerId, dict: dict)
                        XCTAssertNotNil(layer_id, "Absent value for FIELD: \(IndJSONFields.jf_layers_layerId.key)")
                    } catch
                    {
                        XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_layers_layerId.key)")
                    }
                    
                    do
                    {
                        let index = try jsonHelper.getFieldValue(IndJSONFields.jf_layers_index, dict: dict)
                        XCTAssertNotNil(index, "Absent value for FIELD: \(IndJSONFields.jf_layers_index.key)")
                    } catch
                    {
                        XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_layers_index.key)")
                    }
                    
                    do
                    {
                        let type = try jsonHelper.getFieldValue(IndJSONFields.jf_layers_type, dict: dict)
                        XCTAssertNotNil(type, "Absent value for FIELD: \(IndJSONFields.jf_layers_type.key)")
                    } catch
                    {
                        XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_layers_type.key)")
                    }
                    
                    do
                    {
                        let format = try jsonHelper.getFieldValue(IndJSONFields.jf_layers_format, dict: dict)
                        XCTAssertNotNil(format, "Absent value for FIELD: \(IndJSONFields.jf_layers_format.key)")
                    } catch
                    {
                        XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_layers_format.key)")
                    }
                    
                }

                
                
            } catch
            {
                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_plan_layers.key)")
            }
            
            
            
            
            
            
        } catch
        {
            XCTFail("Parsing layers failed.")
        }
        
        
        
    }
    
    
    func test_3_ThatAllObjectsFieldCorrectly()
    {
        let jsonHelper = JsonHelper()
        
        let objBuild = ObjectBuilder()
        do
        {
            let jsonResultArr = try jsonHelper.jsonAsArray(hardcodeJSON)
            
            do
            {
                let layersDict = try jsonHelper.getFieldValue(IndJSONFields.jf_plan_layers, dict: jsonResultArr)
                let layersArray = layersDict as! [NSDictionary]
                XCTAssertEqual(layersArray.count, 3, "Incorrect layers count")
                XCTAssertNotNil(layersDict, "Absent value for FIELD: \(IndJSONFields.jf_plan_layers.key)")
                
                
                
                
                for dict in layersArray
                {
                    
                    
                    let type = try jsonHelper.getFieldValue(IndJSONFields.jf_layers_type, dict: dict)
                    if (IndTypes.LayerType(rawValue: type as! Int) != IndTypes.LayerType.routesGrid)
                    {
                        let objectsDict = try jsonHelper.getFieldValue(IndJSONFields.jf_layers_objects, dict: dict)
                        let objects = objectsDict as! [NSDictionary]
                        
                        for dict in objects
                        {
                            
                            
                            let paramsDict = try jsonHelper.getFieldValue(IndJSONFields.jf_object_params, dict: dict) as! NSDictionary
                            
                            for (key,value) in paramsDict
                            {
                                
                                
                                if let keyV = key as? String
                                {
                                    let enumId = IndParams.ParamsId(rawValue: keyV)
                                    XCTAssertNotNil(enumId, "\(keyV) object not exist")
                                    print("TEST KEY \(keyV)")
                                    do
                                    {
                                        _ = try objBuild.getParamValue(enumId!, paramsVal: value as AnyObject)
                                    }catch let error as IErrorTypeWithNSError
                                    {
                                        XCTFail("\(error.getAsNSError().localizedDescription)")
                                    }
                                    catch
                                    {
                                        XCTFail("INCORRECT VALUE IN PARAMS")
                                    }
                                    
                                    
                                } else
                                {
                                    XCTFail("KEY VALUE NOT STRING")
                                }
                                
                                
                                
                                
                                
                            }

                            
                            do
                            {
                                let object_id = try jsonHelper.getFieldValue(IndJSONFields.jf_object_id, dict: dict)
                                print("VALUE \(object_id)")
                                XCTAssertNotNil(object_id, "Absent value for FIELD: \(IndJSONFields.jf_object_id.key)")
                            } catch
                            {
                                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_object_id.key)")
                            }
                            
                            
                            do
                            {
                                let type = try jsonHelper.getFieldValue(IndJSONFields.jf_object_type, dict: dict)
                                XCTAssertNotNil(type, "Absent value for FIELD: \(IndJSONFields.jf_object_type.key)")
                            } catch
                            {
                                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_object_type.key)")
                            }
                            
                            do
                            {
                                let pointArr = try jsonHelper.getFieldValue(IndJSONFields.jf_object_coord, dict: dict) as! [[CGFloat]]
                                XCTAssertNotNil(pointArr, "Absent value for FIELD: \(IndJSONFields.jf_object_coord.key)")
                                
                                
                            } catch
                            {
                                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_object_coord.key)")
                            }
                            
                        }
                    }
                    
                }
                
                
                
                
                
            } catch
            {
                XCTFail("Parsing failed. FIELD: \(IndJSONFields.jf_plan_layers.key)")
            }
            
            
            
            
            
            
        } catch
        {
            XCTFail("Parsing layers failed.")
        }
        
        
        
    }
    
    func test_4_ThatAllPlanCorrectly()
    {
        let planBuilder : PlanBuilder = PlanBuilder()
        do
        {
            try planBuilder.getPlan(hardcodeJSON)
            
        } catch
        {
            XCTFail("Plan building failed.")
        }
    }
    
}
