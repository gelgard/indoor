//
//  Unit_10_ObjectTests.swift
//  IndoorApp
//
//  Created by Oleg on 02.01.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import XCTest
import IndoorEngine
import IndoorModel


class Unit_10_ObjectTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_1_createObject()
    {
        do
        {
            let object : IObject = try Object(objectId: 0, type: IndTypes.ObjectType.wall)
            XCTAssertNotNil(object,"Incorrect Object Init")
        } catch
        {
            
        }

    }
    
    func test_2_createObjectWithNegativeId()
    {
        do
        {
            try Object(objectId: -10, type: IndTypes.ObjectType.wall)
            
        } catch let error as IndError.InitError
        {
            XCTAssertEqual(error, IndError.InitError.negativeId,"Error not catched")
        }
        catch
        {
           XCTFail("Unknown Error")
        }
        
    }
    
    func test_3_addCoordinates()
    {
        do
        {
           let object = try Object(objectId: 0, type: IndTypes.ObjectType.wall)
           object.addCoordinates(CGPoint(x: 0, y: 20))
           object.addCoordinates(CGPoint(x: 0, y: 20))
            object.addCoordinates(CGPoint(x: 0, y: 20))
            
            XCTAssertEqual(object.coordinates.count, 3,"Incorrect Count")
        }
        catch
        {
            XCTFail("Unknown Error")
        }
    }
    
    
    
    
}
