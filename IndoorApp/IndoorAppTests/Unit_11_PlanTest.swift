//
//  Unit_11_PlanTest.swift
//  IndoorApp
//
//  Created by Oleg on 02.01.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import XCTest
import IndoorEngine
import IndoorModel


class Unit_11_PlanTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_1_createPlan()
    {
        do
        {
          let plan : IPlan = try Plan(name: "test", type: IndTypes.PlanType.building, width: 1000, height: 2000, angle: 90 , major: 2)
            XCTAssertNotNil(plan,"Incorrect Object Init")
            XCTAssertEqual(plan.name, "test","Incorrect Name")
            XCTAssertEqual(plan.type, IndTypes.PlanType.building,"Incorrect Type")
            XCTAssertEqual(plan.width, 1000,"Incorrect Width")
            XCTAssertEqual(plan.height, 2000,"Incorrect Height")
        } catch
        {
            
        }
        
    }
    
    func test_2_createPlanWithIncorrectWidth()
    {
        do
        {
             try Plan(name: "test", type: IndTypes.PlanType.building, width: 0, height: 2000, angle: 90 , major: 2)
            
        } catch let error as IndError.InitError
        {
            XCTAssertEqual(error, IndError.InitError.nullWidth,"Error not catched")
        }
        catch
        {
            XCTFail("Unknown Error")
        }
        
    }
    
    func test_3_createPlanWithIncorrectHeight()
    {
        do
        {
            try Plan(name: "test", type: IndTypes.PlanType.building, width: 1000, height: 0, angle: 90 , major: 2)
            
        } catch let error as IndError.InitError
        {
            XCTAssertEqual(error, IndError.InitError.nullHeight,"Error not catched")
        }
        catch
        {
            XCTFail("Unknown Error")
        }
        
    }
    
    func test_4_addLayers()
    {
        do
        {
            let plan = try MutablePlan(name: "test", type: IndTypes.PlanType.building, width: 1000, height: 2000, angle: 90 , major: 2)
            
            let layer1 = try Layer(layer_id: 0, index: 0, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)
            let layer2 = try Layer(layer_id: 0, index: 0, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)
            let layer3 = try Layer(layer_id: 0, index: 0, type: IndTypes.LayerType.buildingPlan, format: IndTypes.LayerFormat.objects)
            plan.addLayer(layer1)
            plan.addLayer(layer2)
            plan.addLayer(layer3)
            
            XCTAssertEqual(plan.layers.count, 3,"Incorrect Count")
        }
        catch
        {
            XCTFail("Unknown Error")
        }
    }
    
    
    
    
}

