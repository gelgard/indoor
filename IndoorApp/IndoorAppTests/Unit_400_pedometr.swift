//
//  Unit_1_pedometr.swift
//  IndoorNavigation
//
//  Created by Oleg on 14.01.17.
//  Copyright © 2017 UranCompany. All rights reserved.
//

import Foundation

import XCTest
import IndoorEngine
import IndoorModel







class Unit_400_pedometr: XCTestCase {
  
  var pedometr : Pedometr?

  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.pedometr = Pedometr()
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
  }
  
  func test_1_checkMilisecFromTime() {
    do {
      let csvHelper = try CSVHelper(withCSVUrl: "http://indoor-logs.uranium.pp.ua/Sensors_data_31.08.16_14_2410_long_pauses.csv", delimetr : ";")
      
      let motionBuiler = MotionBuilder(withCSVHelper: csvHelper)
      let motionArray : [iMotion] = try motionBuiler.motionArrayFromCSV()
      
      for motion in motionArray {
        print(motion.timeMeasurement.timeIntervalSince1970)
      }
      
      XCTAssertTrue(motionArray .count == 6555, "Incorret items count")
      
      
    } catch let err as NSError {
      print(err.localizedDescription)
      XCTFail(err.localizedDescription)
    }
  }
  
  func test_2_getStepCount() {
    typealias UrlCount = (url : String, count : Int)
    let urlArray : [UrlCount] = [("http://indoor-logs.uranium.pp.ua/Sensors_data_11.01.17_12_31_10_step_in_hand.csv",5),("http://indoor-logs.uranium.pp.ua/Sensors_data_11.01.17_12_33_10_step_pause_5_step.csv",0),("http://indoor-logs.uranium.pp.ua/Sensors_data_11.01.17_12_30_no_step_device_on_table.csv",0),("http://indoor-logs.uranium.pp.ua/Sensors_data_11.01.17_12_30_no_step.csv",0),("http://indoor-logs.uranium.pp.ua/Sensors_data_11.01.17_12_29_10_step_slow.csv",11),("http://indoor-logs.uranium.pp.ua/Sensors_data_11.01.17_12_28_10_step_normal.csv",10)]
    

    for urlItem in urlArray {
      do {
        print("URL : \(urlItem.url)")
        let csvHelper = try CSVHelper(withCSVUrl: urlItem.url, delimetr : ";")
        let motionBuiler = MotionBuilder(withCSVHelper: csvHelper)
        let motionArray : [iMotion] = try motionBuiler.motionArrayFromCSV()
        
        let count = pedometr?.getStepCount(motionData: motionArray)
        print("URL: \(urlItem.url) COUNT: \(count)")
        XCTAssert(urlItem.count == count, "Incorrect step count. Now : \(count) should be \(urlItem.count)")
        
        
        
      } catch let err as NSError {
        print(err.localizedDescription)
        XCTFail(err.localizedDescription)
      }
      
    }

  }
  
  
}

