//
//  Unit_102_TestConstantValuesThatExist.swift
//  IndoorApp
//
//  Created by Oleg on 14.02.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import XCTest
import IndoorEngine
import IndoorModel


class Unit_102_TestConstantValuesThatExist: XCTestCase {
    
    func test_1_That_PlanType_ConstantsExists()
    {
        let buildind = IndTypes.PlanType(rawValue: 0)
        XCTAssertNotNil(buildind, "Building type not exist")
        let pavilion = IndTypes.PlanType(rawValue: 1)
        XCTAssertNotNil(pavilion, "Pavilion type not exist")
        let openSpace = IndTypes.PlanType(rawValue: 2)
        XCTAssertNotNil(openSpace, "Pavilion type not exist")
        
    }
    
    func test_2_That_TypeLayer_ConstantsExists()
    {
        let buildindPlan = IndTypes.LayerType(rawValue: 0)
        XCTAssertNotNil(buildindPlan, "BuildingPlan type not exist")
        let infoObject = IndTypes.LayerType(rawValue: 1)
        XCTAssertNotNil(infoObject, "Info Object type not exist")
        let routesGrid = IndTypes.LayerType(rawValue: 2)
        XCTAssertNotNil(routesGrid, "Routes Grid type not exist")
        
    }
    
    func test_3_That_LayerFormat_ConstantsExists()
    {
        let objects = IndTypes.LayerFormat(rawValue: 0)
        XCTAssertNotNil(objects, "Objects type not exist")
        let grid = IndTypes.LayerFormat(rawValue: 1)
        XCTAssertNotNil(grid, "Grid Format type not exist")
        
        
    }
    
    func test_4_That_ObjectType_ConstantsExists()
    {
        let wall = IndTypes.ObjectType(rawValue: 1)
        XCTAssertNotNil(wall, "WALL object not exist")
        let window = IndTypes.ObjectType(rawValue: 2)
        XCTAssertNotNil(window, "WINDOW object not exist")
        let door = IndTypes.ObjectType(rawValue: 3)
        XCTAssertNotNil(door, "DOOR object not exist")
        let barrier = IndTypes.ObjectType(rawValue: 4)
        XCTAssertNotNil(barrier, "BARRIER object not exist")
        let stairs = IndTypes.ObjectType(rawValue: 5)
        XCTAssertNotNil(stairs, "STAIRS object not exist")
        let pavilion = IndTypes.ObjectType(rawValue: 1001)
        XCTAssertNotNil(pavilion, "PAVILION object not exist")
        let infoDesk = IndTypes.ObjectType(rawValue: 1002)
        XCTAssertNotNil(infoDesk, "INFO DESK object not exist")
        let showRoom = IndTypes.ObjectType(rawValue: 1003)
        XCTAssertNotNil(showRoom, "SHOW ROOM object not exist")
        let conferenceHall = IndTypes.ObjectType(rawValue: 1004)
        XCTAssertNotNil(conferenceHall, "CONFERENCE HALL object not exist")
        let scene = IndTypes.ObjectType(rawValue: 1005)
        XCTAssertNotNil(scene, "SCENE object not exist")
        let shop = IndTypes.ObjectType(rawValue: 1006)
        XCTAssertNotNil(shop, "SHOP object not exist")
        let wc = IndTypes.ObjectType(rawValue: 1007)
        XCTAssertNotNil(wc, "WC object not exist")
        let foodcourt = IndTypes.ObjectType(rawValue: 1008)
        XCTAssertNotNil(foodcourt, "FOOD COURT object not exist")
        let beacon = IndTypes.ObjectType(rawValue: 1009)
        XCTAssertNotNil(beacon, "BEACON object not exist")
        let wifi = IndTypes.ObjectType(rawValue: 1010)
        XCTAssertNotNil(wifi, "WIFI POINT object not exist")
    }
    
    func test_5_That_ObjectParams_ConstantsExists()
    {
        let wall = IndTypes.ObjectType(rawValue: 1)
        XCTAssertNotNil(wall, "WALL object not exist")
        let window = IndTypes.ObjectType(rawValue: 2)
        XCTAssertNotNil(window, "WINDOW object not exist")
        let door = IndTypes.ObjectType(rawValue: 3)
        XCTAssertNotNil(door, "DOOR object not exist")
        let barrier = IndTypes.ObjectType(rawValue: 4)
        XCTAssertNotNil(barrier, "BARRIER object not exist")
        let stairs = IndTypes.ObjectType(rawValue: 5)
        XCTAssertNotNil(stairs, "STAIRS object not exist")
        let pavilion = IndTypes.ObjectType(rawValue: 1001)
        XCTAssertNotNil(pavilion, "PAVILION object not exist")
        let infoDesk = IndTypes.ObjectType(rawValue: 1002)
        XCTAssertNotNil(infoDesk, "INFO DESK object not exist")
        let showRoom = IndTypes.ObjectType(rawValue: 1003)
        XCTAssertNotNil(showRoom, "SHOW ROOM object not exist")
        let conferenceHall = IndTypes.ObjectType(rawValue: 1004)
        XCTAssertNotNil(conferenceHall, "CONFERENCE HALL object not exist")
        let scene = IndTypes.ObjectType(rawValue: 1005)
        XCTAssertNotNil(scene, "SCENE object not exist")
        let shop = IndTypes.ObjectType(rawValue: 1006)
        XCTAssertNotNil(shop, "SHOP object not exist")
        let wc = IndTypes.ObjectType(rawValue: 1007)
        XCTAssertNotNil(wc, "WC object not exist")
        let foodcourt = IndTypes.ObjectType(rawValue: 1008)
        XCTAssertNotNil(foodcourt, "FOOD COURT object not exist")
        let beacon = IndTypes.ObjectType(rawValue: 1009)
        XCTAssertNotNil(beacon, "BEACON object not exist")
        let wifi = IndTypes.ObjectType(rawValue: 1010)
        XCTAssertNotNil(wifi, "WIFI POINT object not exist")
    }

    func test_6_That_ParamsId_ConstantsExists()
    {
      let exit = IndParams.ParamsId(rawValue: "1")
      XCTAssertNotNil(exit, "EXIT params not exist")
      let name = IndParams.ParamsId(rawValue: "1001")
      XCTAssertNotNil(name, "NAME params not exist")
      let desc = IndParams.ParamsId(rawValue: "1002")
      XCTAssertNotNil(desc, "DESCRIPTION params not exist")
      let sponsor = IndParams.ParamsId(rawValue: "1003")
      XCTAssertNotNil(sponsor, "SPONSOR params not exist")
      let model = IndParams.ParamsId(rawValue: "1004")
      XCTAssertNotNil(model, "MODEL params not exist")
      let openTime = IndParams.ParamsId(rawValue: "1005")
      XCTAssertNotNil(openTime, "OPEN TIME params not exist")
      let closeTime = IndParams.ParamsId(rawValue: "1006")
      XCTAssertNotNil(closeTime, "CLOSE TIME params not exist")
      let mainImage = IndParams.ParamsId(rawValue: "1007")
      XCTAssertNotNil(mainImage, "MAIN IMAGE params not exist")
      let groupColor = IndParams.ParamsId(rawValue: "1008")
      XCTAssertNotNil(groupColor, "GROUP COLOR params not exist")
      let index = IndParams.ParamsId(rawValue: "1009")
      XCTAssertNotNil(index, "INDEX params not exist")
      let uuid = IndParams.ParamsId(rawValue: "1010")
      XCTAssertNotNil(uuid, "UUID params not exist")
      let minor = IndParams.ParamsId(rawValue: "1011")
      XCTAssertNotNil(minor, "MINOR params not exist")
      let major = IndParams.ParamsId(rawValue: "1012")
      XCTAssertNotNil(major, "MAJOR params not exist")
      let latitude = IndParams.ParamsId(rawValue: "1013")
      XCTAssertNotNil(latitude, "LATITUDE params not exist")
      let longitude = IndParams.ParamsId(rawValue: "1014")
      XCTAssertNotNil(longitude, "LONGITUDE params not exist")
      let parentObject = IndParams.ParamsId(rawValue: "1015")
      XCTAssertNotNil(parentObject, "PARENT OBJECT params not exist")
      let txPower = IndParams.ParamsId(rawValue: "1016")
      XCTAssertNotNil(txPower, "TXPOWER params not exist")
    }
    
    override func setUp() {
        super.setUp()
        }
    
}
