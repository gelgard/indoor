//
//  DataWrapperTest.swift
//  museumGuideNav
//
//  Created by Oleg on 22.04.17.
//  Copyright © 2017 UranCompany. All rights reserved.
//

import XCTest
import UIKit
import IndoorAPIManager
import IndoorModel
import IndoorParser
@testable import IndoorApp

class DataWrapperTest: XCTestCase {
  
 
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
    
  }
  
  func test_1_executeFullPlanApi() {
    weak var expect = expectation(description: "Testing full plan - should pass")
    let manager = IndoorAPIManager()
    do {
      try manager.getAPIData(requestType: .ActionFullplan, params: ["id":7,"":"en"], completionHandler: {
        (result,dictionary) in
        XCTAssert((dictionary != nil))
        print("\(dictionary!.data.count)")
        XCTAssert(dictionary!.data.count == 15)
        expect!.fulfill()
      })
      waitForExpectations(timeout: 30, handler: { (error) in
        if let error = error {
          XCTFail("waitForExpectationsWithTimeout errored: \(error)")
        }
      })
    }
    catch _ {
      XCTFail()
    }
  }
  
  func test_2_executeCallApiInWrapperPassed() {
    
    weak var expect = expectation(description: "Testing full plan - should pass")
    
    do {
      let wrapper = try DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.getDataFromSource(completition: {(error, data, isNewData) in
        XCTAssertTrue(error == nil)
        XCTAssert(data?.count == 15)
        
        expect?.fulfill()
        
      })
    } catch {
      XCTFail()
    }
    
    waitForExpectations(timeout: 15, handler: { (error) in
      if let error = error {
        XCTFail("waitForExpectationsWithTimeout errored: \(error)")
      }
    })
  }
  
    func test_3_executeCallApiInWrapperNotPassed() {
      
      weak var expect = expectation(description: "Testing full plan - should pass")
      
      do {
        let wrapper = try FakeDataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
        
        wrapper.getDataFromSource(completition: {(error, data, isNewData) in
          
          XCTAssertTrue(error != nil)
          XCTAssert(data == nil)
          
          expect?.fulfill()
          
        })
      } catch {
        XCTFail()
      }
      
      
      
      
      waitForExpectations(timeout: 15, handler: { (error) in
        if let error = error {
          XCTFail("waitForExpectationsWithTimeout errored: \(error)")
        }
      })
      
      
    
  }
  
  func test_3_1_getDataFromJSON_Passed() {
    weak var expect = expectation(description: "Testing full plan - should pass")
    
    let bundle = Bundle(for: type(of:self))
    do {
      let wrapper = try DataStorageWrapper(json: bundle.url(forResource: "test", withExtension: "json")!, locale: "en", folderPath: "")
      wrapper.getDataFromSource(completition: {(error, data, isNewData) in
        XCTAssertTrue(error == nil)
        print(data?.count)
        XCTAssert(data?.count == 13)
        
        expect?.fulfill()
        
      })
    } catch {
      XCTFail()
    }
    
    waitForExpectations(timeout: 15, handler: { (error) in
      if let error = error {
        XCTFail("waitForExpectationsWithTimeout errored: \(error)")
      }
    })
  }
  
  func test_4_executeParserPassed() {
   
    let expect = expectation(description: "Testing Api parse - should pass")
    
    let progressValue : ((Double, IndTypes.TaskList) -> Void) = { (value : Double, task : IndTypes.TaskList)
      in
      print("VALUEPROGRESS \(value)")
      XCTAssert(task == IndTypes.TaskList.parser)
      
      if value > 1.0 {
        XCTFail()
      }
      
      if value == 1.0 {
        expect.fulfill()
      }
      
    }
    
    do {
      
      let wrapper = try DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      
      wrapper.getDataFromSource(completition: {(error, data, isNewData) in
        
        XCTAssertTrue(error == nil)
        XCTAssert(data?.count == 15)
        
        wrapper.currentProgress = progressValue
        
        wrapper.parseData(data: data as! [String : Any], completition: { (error) in
          print("\(String(describing: error?.localizedDescription))")
          XCTAssertTrue(error == nil)
        })
        
      })
      
    } catch {
      XCTFail()
    }
    
    
    

    
    waitForExpectations(timeout: 30) { error in
      if let error = error {
        XCTFail("waitForExpectationsWithTimeout errored: \(error)")
      }
    }

  }
  
  
  func test_5_executeParserFailed() {
    
    var testVar : [String : Any] = [:]
    testVar["fonar"] = 1
    testVar["fonar_2"] = 2
    
    do {
      let wrapper = try DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.parseData(data: testVar, completition: { (error) in
        XCTAssertTrue(error != nil)
      })
      
    } catch {
      
    }
  }
  
  func test_6_executeDownloadContent() {
    let expect = expectation(description: "Testing Api parse - should pass")
    
    do {
      let wrapper = try DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.downloadContent(completition: { (error) in
        
        XCTAssert(error == nil)
        expect.fulfill()
        
      })
      waitForExpectations(timeout: 60) { error in
        if let error = error {
          XCTFail("waitForExpectationsWithTimeout errored: \(error)")
        }
      }
     
    } catch {
      XCTFail()
    }
  }
  
  func test_7_executeDownloadContentFailed() {
    let expect = expectation(description: "Testing Api parse - should pass")
    
    do {
      let wrapper = try FakeDataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.downloadContent(completition: { (error) in
        
        XCTAssert(error != nil)
        expect.fulfill()
        
      })
      waitForExpectations(timeout: 30) { error in
        if let error = error {
          XCTFail("waitForExpectationsWithTimeout errored: \(error)")
        }
      }
      
    } catch {
      XCTFail()
    }
  }
  
  func test_7_executeDownloadContentFailed2() {
    let expect = expectation(description: "Testing Api parse - should pass")
    
    do {
      let wrapper = try Fake2DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.downloadContent(completition: { (error) in
        
        XCTAssert(error != nil)
        expect.fulfill()
        
      })
      waitForExpectations(timeout: 30) { error in
        if let error = error {
          XCTFail("waitForExpectationsWithTimeout errored: \(error)")
        }
      }
      
    } catch {
      XCTFail()
    }
  }
  
  
  func test_8_DownloadContentProgress() {
    
    let expect = expectation(description: "Testing Api parse - should pass")
    
    let progressValue : ((Double, IndTypes.TaskList) -> Void) = { (value : Double, task : IndTypes.TaskList)
      in
      print("VALUEPROGRESS \(value)")
      //XCTAssert(task == IndTypes.TaskList.allcontent)
      
      if value > 1.0 {
        XCTFail()
      }
      
      if value == 1.0 {
        expect.fulfill()
      }
      
    }
    
    do {
      
      let wrapper = try DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.currentProgress = progressValue
      wrapper.downloadContent(completition: { (error) in
      
        
        XCTAssert(error == nil)
       
        
      })
      
    } catch {
      XCTFail()
    }
    
    
    
    
    
    waitForExpectations(timeout: 60) { error in
      if let error = error {
        XCTFail("waitForExpectationsWithTimeout errored: \(error)")
      }
    }
    
  }

  func test_9_DownloadContentProgressFromJSON2Asset() {
    
    let bundle = Bundle(for: type(of:self))
    let expect = expectation(description: "Testing Api parse - should pass")
    
    let parser = IndoorParser(withJSONFile: bundle.url(forResource: "test2", withExtension: "json")!, folderPath: "", locale: "en", delegate: nil)
   
    do {
        try parser.parse(progress: { (value) in
          if value > 1.0 {
            XCTFail()
          }
          
          if value == 1.0 {
            let progressValue : ((Double, IndTypes.TaskList) -> Void) = { (value : Double, task : IndTypes.TaskList)
              in
              print("VALUEPROGRESS \(value)")
              //XCTAssert(task == IndTypes.TaskList.allcontent)
              
              if value > 1.0 {
                XCTFail()
              }
              
              if value == 1.0 {
                expect.fulfill()
              }
              
            }
            
            do {
              
              let wrapper = try DataStorageWrapper(json: bundle.url(forResource: "test2", withExtension: "json")!, locale: "en", folderPath: "")
              wrapper.currentProgress = progressValue
              wrapper.downloadContent(completition: { (error) in
                
                
                XCTAssert(error == nil)
                
                
              })
              
            } catch {
              XCTFail()
            }
            
            
            
            
            
            waitForExpectations(timeout: 60) { error in
              if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
              }
            }
          }
        })
    } catch {
      XCTFail()
    }
    
    
    
    
  }
  
  func test_10_executeUpdate() {
    let expect = expectation(description: "Testing Api parse - should pass")
    
    let progressValue : ((Double, IndTypes.TaskList) -> Void) = { (value : Double, task : IndTypes.TaskList)
      in
      print("VALUEPROGRESS \(value)")
      //XCTAssert(task == IndTypes.TaskList.allcontent)
      
      if value > 1.0 {
        XCTFail()
      }
      
    }
    
    var step : Int = 0
    
    
    let currentTaskTest : ((IndTypes.TaskList,IndTypes.TaskProcess,String,String,IndTypes.DialogType) -> Void ) = {
      ( task : IndTypes.TaskList,process : IndTypes.TaskProcess,title : String,message : String,type : IndTypes.DialogType) in
      
      
      print("STEP \(step) TASK \(task.rawValue) MESSAGE \(message)" )
      
      step = step + 1
      
      if step == 1 {
        XCTAssertTrue(task == IndTypes.TaskList.api)
        
      }
      
      if step == 2 {
        XCTAssertTrue(task == IndTypes.TaskList.parser || task == IndTypes.TaskList.api )
      }
      
      if step == 3 {
        XCTAssertTrue(task == IndTypes.TaskList.allcontent)
      }
    }
    
    do {
      
      let wrapper = try DataStorageWrapper(planId: 7, locale: "en", folderPath: "images")
      wrapper.currentProgress = progressValue
      wrapper .currentTask = currentTaskTest
      wrapper.executeUpdate(completion: { (error) in
        
        XCTAssertTrue(error == nil)
        expect.fulfill()
        
      })
      
      waitForExpectations(timeout: 60) { error in
        if let error = error {
          XCTFail("waitForExpectationsWithTimeout errored: \(error)")
        }
      }
      
    } catch {
      
      XCTFail()
      
    }
    
    

  }
  
  
}
