//
//  Unit_10_JSONHelperTests.swift
//  Motivator
//
//  Created by Oleg on 30.05.15.
//  Copyright (c) 2015 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import XCTest
import IndoorEngine
import IndoorModel


class Unit_100_JSONHelperTests: XCTestCase {
    
    
   
    
    override func setUp() {
        super.setUp()
        
    }
    
    
    
    
    
    
    func test_1_ThatThatJsonFileExist()
    {
        let path = Bundle.main.path(forResource: hardcodeJSON, ofType: "json")
        let filemgr = FileManager.default
        
        XCTAssertTrue(filemgr.fileExists(atPath: path!), "JSON file absent")
    }
    
    
    

    
    func test_2_ThatThatJsonIsCorrect()
    {
        do
        {
            _ = try JsonHelper().jsonAsArray(hardcodeJSON)
        } catch
        {
            XCTFail("Incorrect JSON")
        }
        
        
        
    }
    
    func test_3_ThatThatPalleteAsArrayFromJSON()
    {
        do
        {
            let jsonHelper = try JsonHelper().jsonAsArray(hardcodeJSON)
            XCTAssertNotNil(jsonHelper, "Incorrect Type Result")
        } catch IndError.JSONError.fileAbsent
        {
            XCTFail("Absent JSON File")
        } catch IndError.JSONError.parsingError
        {
            XCTFail("Error JSON parsing")
        } catch IndError.JSONError.readingError
        {
            XCTFail("JSON reading Error")
        } catch
        {
            XCTFail("Incorrect JSON")
        }
        
        
        
        
    }
    
   
    
    func test_4_ThatThatCheckCorrectWithFakeFile()
    {
        
        do
        {
            let jsonHelper = try JsonHelper().jsonAsArray("fake")
            XCTAssertEqual(jsonHelper.count, 0, "Incorrect Data Count")
        } catch IndError.JSONError.fileAbsent
        {
            
        } catch IndError.JSONError.parsingError
        {
           
        } catch IndError.JSONError.readingError
        {
            
        } catch
        {
           
        }
        
        
    }

    
}
