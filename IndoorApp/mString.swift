//
//  mString.swift
//  Motivator
//
//  Created by Oleg on 11.05.15.
//  Copyright (c) 2015 UranCompany. All rights reserved.
//
import Foundation

public extension String {
  
  var length: Int { return self.characters.count }
  
  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }
  
  func toDouble() -> Double {
    return NumberFormatter().number(from: self)!.doubleValue
  }
  
  func index(of string: String) -> String.Index? {
    return range(of: string)?.lowerBound
  }
  
  func extractId() -> String? {
    var res : String? = nil
    if self.index(of: prefixRootNodes) != nil {
      let startPos = prefixRootNodes.endIndex
      res = self.substring(from: startPos)
    } else {
      let strAsInt = Int(self)
      if strAsInt != nil {
        res = self
      }
    }
    
   
    return res
    
  }
  
}
