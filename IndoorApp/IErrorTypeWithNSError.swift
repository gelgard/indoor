//
//  IErrorTypeWithNSError.swift
//  Motivator
//
//  Created by Oleg on 19.09.15.
//  Copyright © 2015 UranCompany. All rights reserved.
//

import Foundation

protocol IErrorTypeWithNSError : Error {
  func getAsNSError() -> NSError
}
