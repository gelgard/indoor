//
//  DebugInfo.swift
//  IndoorApp
//
//  Created by Oleg on 05.06.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import MessageUI
import UIKit

class DebugInfo: NSObject, iDebugInfo {
  
  var resDict : [String : AnyObject] = [:]
  var points : [AnyObject] = []
  var pathes : [AnyObject] = []
  fileprivate var privateVC  : UIViewController
  fileprivate var pointId  : Int = 0
  fileprivate var dataDict : [String : AnyObject] = [:]

  //MARK: - IMplementation iDebugInfo
  
  required init(planId : Int, vc : UIViewController) {
    privateVC = vc
    
    resDict[JSONConstant.planIdKey] = planId as AnyObject?
    resDict[JSONConstant.dateKey] = Date().timeIntervalSince1970 as AnyObject?
    var configDict : [String : AnyObject] = [:]
    configDict[JSONConstant.beaconCountKey] = CalculationConstant.nearestBeaconsCount as AnyObject?
    configDict[JSONConstant.kalmanKey] = CalculationConstant.factorKalman as AnyObject?
    configDict[JSONConstant.periodKey] = CalculationConstant.updatePeriod as AnyObject?
    configDict[JSONConstant.maxRelocationKey] = CalculationConstant.maxRelocation as AnyObject?
    resDict[JSONConstant.algConfigKey] = configDict as AnyObject?
  }
  func addPointData(_ beaconData:[BeaconsData],resultPoint: [Int], beginTime : Double, endTime: Double)  -> Int {
    var beaconArr : [AnyObject] = []
    for beacVal in beaconData {
      var beaconArrVal : [AnyObject] = []
      beaconArrVal.append(Int(beacVal.minor) as AnyObject)
      beaconArrVal.append(Double(beacVal.distance) as AnyObject)
      beaconArr.append(beaconArrVal as AnyObject)
    }
    var resPointDict : [String : AnyObject] = [:]
    resPointDict[JSONConstant.beacDataKey] = beaconArr as AnyObject?
    
    resPointDict[JSONConstant.resultPointKey] = resultPoint as AnyObject?
    resPointDict[JSONConstant.startCalcKey] = beginTime as AnyObject?
    resPointDict[JSONConstant.endCalcKey] = endTime as AnyObject?
    resPointDict[JSONConstant.diffTimeKey] = Double(endTime - beginTime) as AnyObject?
    pointId = pointId + 1
    resPointDict[JSONConstant.pointId] = pointId as AnyObject?
    points.append(resPointDict as AnyObject)
    
    return pointId
    
    
  }
  func addPathData(_ move:Bool, startTime : Double , endTime : Double , stopPoint : [Int]?) {
    var resPathDict : [String : AnyObject] = [:]
    resPathDict[JSONConstant.moveKey] = move as AnyObject?
    resPathDict[JSONConstant.startActionKey] = startTime as AnyObject?
    resPathDict[JSONConstant.endActionKey] = endTime as AnyObject?
    resPathDict[JSONConstant.durationActionKey] = Double(endTime - startTime) as AnyObject?
    
    
    if let _ = stopPoint {
      resPathDict[JSONConstant.stopPointKey] = stopPoint as AnyObject?
    }
    
    pathes.append(resPathDict as AnyObject)
    
    
  }
  func generateEmail() {
    let contentFile = getJSONString()
//    let filePath = NSBundle.mainBundle().pathForResource("log", ofType: "json")
//    
//    do {
//      try contentFile.writeToFile(filePath!, atomically: true, encoding: NSUTF8StringEncoding)
//    } catch {
//      
//    }
    
    if(MFMailComposeViewController.canSendMail()){
      
      let mailComposer = MFMailComposeViewController()
      mailComposer.mailComposeDelegate = self
     
      mailComposer.setSubject("Debug_Info_\(Date().asStringForName())")
      mailComposer.setMessageBody("", isHTML: false)
      
      
      if let data = contentFile.data(using: String.Encoding.utf8){
        //Attach File
        mailComposer.addAttachmentData(data, mimeType: "text/plain", fileName: "debug_\(Date().asStringForName()).json")
        privateVC.present(mailComposer, animated: true, completion: nil)
      }
    }
    
  }
  func getJSONString() -> String {
    dataDict[JSONConstant.pointsKey] = points as AnyObject?
    
    dataDict[JSONConstant.pathKey] = pathes as AnyObject?
    resDict[JSONConstant.dataKey] = dataDict as AnyObject?
    
    var res : String = ""
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: resDict, options: JSONSerialization.WritingOptions.prettyPrinted)
      res = String(data: jsonData, encoding: String.Encoding.utf8)!
      // here "jsonData" is the dictionary encoded in JSON data
    } catch let error as NSError {
        NotificationCenter.default.post(name: Notification.Name(rawValue: errorNotification), object: error)
      }
    return res
  }
  
  
}

extension DebugInfo : MFMailComposeViewControllerDelegate {
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }
}
