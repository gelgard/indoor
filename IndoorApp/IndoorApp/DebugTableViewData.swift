//
//  DebugTableViewData.swift
//  IndoorApp
//
//  Created by Oleg on 18.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import IndoorEngine
import IndoorModel

class DebugTableViewData: NSObject, UITableViewDataSource {
  
  var logData : [iDebugLog] = []
  
  
  // MARK:  UITextFieldDelegate Methods
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return logData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell:DebugCell?
    
      // code here
   
     cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as? DebugCell
    if (cell == nil) {
      cell = DebugCell(style:UITableViewCellStyle.subtitle, reuseIdentifier:textCellIdentifier)
    }
    
    
    cell!.contentView.tag = (indexPath as NSIndexPath).row
    cell!.stepCount.text = "\(self.logData[(indexPath as NSIndexPath).row].stepCount)"
    cell!.pointIdLbl.text = "\(self.logData[(indexPath as NSIndexPath).row].pointId)"
    cell!.curPointLbl.text = "\(self.logData[(indexPath as NSIndexPath).row].curPoint.x),\(self.logData[(indexPath as NSIndexPath).row].curPoint.y)"
    cell!.prevPointLbl.text = "\(self.logData[(indexPath as NSIndexPath).row].prevPoint.x),\(self.logData[(indexPath as NSIndexPath).row].prevPoint.y)"

    //cell.delegate = self.parentController
    
    //cell!.selectionStyle = UITableViewCellSelectionStyle.None;
    
    
    return cell!
  }
  
  
  
  
}
