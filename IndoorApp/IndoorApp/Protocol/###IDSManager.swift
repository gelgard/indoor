//
//  IDSManager.swift
//  IndoorApp
//
//  Created by Oleg on 30.09.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation

typealias ParamValue = (paramId : String, value : String)

struct PlanData {
  var name : String
  var type: Int
  var base_angle : Int
  var beacon_major : Int
  var plan_width : Int
  var plan_length : Int
}

struct LayerData {
  var planId : Int
  var type : Int
  var index : Int
  var format : Int
}

struct ObjectData {
  var layerId : Int
  var type : Int
  var coordinates : String
  var parentId : Int
  var params : [ParamValue]
  /*
   [ParamValue] - массив туплов - параметов , которые добавляются в соответсвующие поля таблицы (что такое параметры описано и дальше и в доке справочкинов "Справочник object_param:"
   
   К примеру [("1010":"blabla"),("1012","bubu")]
   соответственно в поле param_1010 должно вставиться blabla , в поле param_1012 должно вставиться bubu
   
   
   */
}

protocol IDSManager {
  init() throws
  /*
  При ините в классе наследнике должно проверяться есть ли база, устанавливаться соединение , если нет база должна создаться и т.д (необходимо продумать все необходимые действия, чтобы избежать падения приложения при работе с базой данных
  В случае эксепшена ВО ВСЕХ ФУНКЦИЯХ возвращается объект имплементирующий протокол :
   protocol IErrorTypeWithNSError : Error {
   func getAsNSError() -> NSError
   }
   
   Пример объекта :
   enum ErrorBeacons: IErrorTypeWithNSError {
   case monitoringNotRonning;
   func getAsNSError() -> NSError
   {
     switch self
     {
       case .monitoringNotRonning:
       return NSError(domain: errDomain, code: 9001, userInfo: [NSLocalizedDescriptionKey : "ERR_MONITORING_NOT_RUNNING".localized])
       }
     }
   }
   */
  
  func addPlan(planData : PlanData) throws-> Int // возвращает Id добавленной записи
 
  func addLayer(layerData : LayerData) throws -> Int // возвращает Id добавленной записи
  
  func addObject(objectData : ObjectData) throws -> Int // возвращает Id добавленной записи
  
  func getObjectsFor(layerId : Int) throws -> [ObjectData]
 
  func getObjectBy(objectId : Int) throws -> ObjectData
  
  func getChildsFor(ownerId : Int) throws -> [ObjectData] //Возвращает массив объектов у которых parentId равно параметру ownerId
  
  func getChildsFor(ownerId : Int, withType : Int) throws -> [ObjectData] //Возвращает массив объектов у которых parentId равно параметру ownerId, а type = withType

  
}
