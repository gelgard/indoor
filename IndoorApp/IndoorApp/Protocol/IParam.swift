//
//  IParam.swift
//  IndoorApp
//
//  Created by Oleg on 10.02.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit

protocol IParam {
  var name : String? {get}
  var description : String? {get}
  var sponsor : Bool? {get}
  var model : String? {get}
  var openTime : Date? {get}
  var closeTime : Date? {get}
  var mainImage : UIImage? {get}
  var groupColor : UIColor? {get}
}
