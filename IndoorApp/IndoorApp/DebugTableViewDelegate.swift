//
//  DebugTableViewDelegate.swift
//  IndoorApp
//
//  Created by Oleg on 18.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import IndoorEngine
import IndoorModel

class DebugTableViewDelegate: NSObject, UITableViewDelegate {
  
  var selectedIndexPath : IndexPath?
  fileprivate let selectedBlock: IndAliases.SelectedIndex?
  
  init(selectedBlock: @escaping IndAliases.SelectedIndex) {
    self.selectedBlock = selectedBlock
  }
 
  
  // MARK:  UITableViewDelegate Methods
  
//  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//    
//  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if (selectedBlock != nil){
      self.selectedBlock!((indexPath as NSIndexPath).row)
    }
  }
  
  func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
  }
  
  
  
  
}
