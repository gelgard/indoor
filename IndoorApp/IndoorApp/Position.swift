//
//  Position.swift
//  IndoorApp
//
//  Created by Oleg on 17.05.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import Indoor_Entities


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class Position {
  
  // MARK: - Properties
  
  fileprivate var gridFilling : [String : Int]
  fileprivate var step : Int?
  fileprivate var gridXSize : CGFloat
  fileprivate var gridYSIze : CGFloat
  fileprivate var beacons : [IBeacon] = []
  fileprivate var beaconsDict : [Int : CGPoint] = [:]
  fileprivate var currentPosition : CGPoint = CGPoint.zero
  fileprivate var previousPosition : CGPoint = CGPoint.zero
  fileprivate var gridColumn : Int
  fileprivate var gridRow : Int
  fileprivate var currentFakePoint : Int = 0
  fileprivate var startCalcTime : TimeInterval?
  fileprivate var endCalcTime : TimeInterval?
  fileprivate var calculationLog : [String]! = []
  
  var debugData : iDebugInfo
  
  
  
  init(plan: IPlan, vc: UIViewController) throws {
    
    
    do {
      let layer = try plan.getLayerBy(IndTypes.LayerType.routesGrid)
      
      if let grid = layer.grid {
        let fillingCoord : [IndAliases.FillingCoordinate] = grid.filling
        
        step = grid.step
        gridColumn = Int(ceil(Double(plan.width) / Double(step!)))
        gridRow = Int(ceil(Double(plan.height) / Double(step!)))
        calculationLog.append("------------------------")
        calculationLog.append("Func: PrepareGridFilling")
        gridFilling = Position.prepareGridFilling(fillingCoord,gridColumn: gridColumn,gridRow: gridRow)
        calculationLog.append("Result PrepareGridFilling:")
        calculationLog.append(gridFilling.description)
        calculationLog.append("------------------------")
      } else {
        throw IndError.ConstructorError.absentBaseParametrs
      }
      
    } catch let error as IErrorTypeWithNSError
    {
      NotificationCenter.default.post(name: Notification.Name(rawValue: errorNotification), object: error.getAsNSError())
      throw IndError.ConstructorError.absentBaseParametrs
    } catch let error as NSError
    {
      NotificationCenter.default.post(name: Notification.Name(rawValue: errorNotification), object: error)
      throw IndError.ConstructorError.absentBaseParametrs
    }
    
    gridXSize = plan.width
    gridYSIze = plan.height
    
    
    
    beacons.append(contentsOf: plan.beaconsInInfoLayer)
    
    for beacon in beacons {
      beaconsDict[beacon.minor] = beacon.coordinates[0]
    }
    //HARDCODE PLAN ID
    debugData = DebugInfo(planId: 5, vc: vc)
    
  }
  
  
  //MARK: - Prepairing Data
  
  fileprivate class func prepareGridFilling(_ fillingCoordinate : [IndAliases.FillingCoordinate],gridColumn: Int,gridRow: Int) -> [String:Int] {
    
    var res : [String : Int] = [:]
    
    for x in 1...gridColumn {
      for y in 1...gridRow {
        let key : IndAliases.FillingCoordinate = (x,y)
        let keyVal : String = Position.getRoutKey(key)
        res[keyVal] = 1
      }
    }
    
    for coord in fillingCoordinate {
        res[Position.getRoutKey((coord.x,coord.y))] = 0
    }
    
    return res
  }
  
  func prepareBeaconsData(_ beaconsData : [BeaconsData] ) -> [Int : Double] {
    calculationLog.append("------------------------")
    calculationLog.append("Func: prepareBeaconsData")
    var avgArr : [Int : IndAliases.AvgDistance] = [:]
    var res : [Int : Double] = [:]
    for (minor,distanse) in beaconsData {
      if let avg = avgArr[minor] {
        if distanse > 0 {
            avgArr[minor] = (distance: avg.distance+distanse , values:avg.values+1)
        }
        
      } else {
        avgArr[minor] = (distance : distanse, values : 1)
      }
    }
    calculationLog.append("AVG \(avgArr.description)")
    
    for beacon in beacons {
      if let _ = avgArr[beacon.minor] {
        let dist : Double = avgArr[beacon.minor]!.distance
        let count : Double = Double(avgArr[beacon.minor]!.values)
        let roundAvg = myRound(dist / count)
        res[beacon.minor] = roundAvg
      } else {
        res[beacon.minor] = pow(10, 8)
      }
    }
    res = sortAndSlice(res,count: CalculationConstant.nearestBeaconsCount)
    calculationLog.append("Result prepareBeaconsData:")
    calculationLog.append(res.description)
    calculationLog.append("------------------------")
    return res
  }
  
  func sortAndSlice(_ beaconDist : [Int : Double], count : Int) -> [Int : Double] {
    calculationLog.append("------------------------")
    calculationLog.append("Func: sortAndSlice")
    calculationLog.append("beacon dist: \(beaconDist.description)")
    var reverseData : [Double : Int] = [:]
    var resDict : [Int : Double] = [:]
    var sortedArray : [Double] = []
    
    for (key, value) in beaconDist {
      reverseData[value] = key
    }
    let arr : [Double] = Array(reverseData.keys)
    sortedArray = arr.sorted { $0 < $1 }
    if CalculationConstant.showCalculationLog {
      calculationLog.append("sorted: \(sortedArray.description)")
    }
    
    
    if sortedArray.count < count {
      return resDict
    }
    
    
    for i in 0...count-1 {
      let valueAsKey : Double = sortedArray[i]
      let keyFromValue : Int = reverseData[valueAsKey]!
      resDict[keyFromValue] = valueAsKey
    }

    
    calculationLog.append("Result sortAndSlice:")
    calculationLog.append(resDict.description)
    calculationLog.append("------------------------")
    return resDict
  }
  
  func myRound(_ value : Double) -> Double {
    calculationLog.append("------------------------")
    calculationLog.append("Func: myRound")
    calculationLog.append("inValue: \(value)")
    let val100 = Double(round(value * 100))
    let val : Double = Double(val100 / 100)
    calculationLog.append("Result myRound:")
    calculationLog.append("\(val)")
    calculationLog.append("------------------------")
    return val
  }
  
  //MARK: - Navigation algorithms & filters
  
  func filterKalman(_ prevPosition : CGPoint , crntPosition : CGPoint) -> CGPoint {
    calculationLog.append("------------------------")
    calculationLog.append("Func: filterKalman")
    let xRes = currentPosition.x * CalculationConstant.factorKalman + (1-CalculationConstant.factorKalman)*prevPosition.x
    let yReZ = currentPosition.y * CalculationConstant.factorKalman + (1-CalculationConstant.factorKalman)*prevPosition.y
    calculationLog.append("Result filterKalman:")
    calculationLog.append("X:\(xRes),Y:\(yReZ)")
    calculationLog.append("------------------------")
    return CGPoint(x: xRes, y: yReZ)
  }
  
  func weightedCentroid(_ beaconsData : [Int : Double]) -> CGPoint {
    calculationLog.append("------------------------")
    calculationLog.append("Func: weightedCentroid")
    var resPoint : CGPoint = CGPoint.zero
    var x : Double = 0
    var y : Double = 0
    var s : Double = 0
    for (_, value) in beaconsData {
      s += 1 / (pow(value, 2))
    }
    
    for (key, value) in beaconsData  {
      let mu : Double = 1 / ((pow(value, 2)) * s)
      
      x += mu * Double((self.beaconsDict[key]?.x)!)
      y += mu * Double((self.beaconsDict[key]?.y)!)
    }
    
    resPoint = CGPoint(x: CGFloat(myRound(x)), y: CGFloat(myRound(y)))
    calculationLog.append("Result weightedCentroid:")
    calculationLog.append("X:\(resPoint.x),Y:\(resPoint.y)")
    calculationLog.append("------------------------")
    
    return resPoint
  }
  
  func nextFreeCell(_ prevCell :IndAliases.FillingCoordinate,currentCell : IndAliases.FillingCoordinate) -> IndAliases.FillingCoordinate {
    calculationLog.append("------------------------")
    calculationLog.append("Func: nextFreeCell")
    var res : IndAliases.FillingCoordinate = prevCell
    var newPoint : [Int:IndAliases.FillingCoordinate] = [:]
    var newPointPath : [Int:Int] = [:]
    var i = 0
    var ready : Bool = false
    
    for j in 1...8 {
      newPoint[j] = (0,0)
      newPointPath[j] = CalculationConstant.infinitePath
    }
    
    repeat {
      i += 1
      calculationLog.append("Repeat #:\(i)")
      
      if ((currentCell.x + i) < gridColumn) {
        let key = Position.getRoutKey((currentCell.x+i,currentCell.y))
        if (gridFilling[key] == 1) {
          newPoint[1]!.x = currentCell.x + i
          newPoint[1]!.y = currentCell.y
        }
      }
      
      if (((currentCell.x + i) < gridColumn) && ((currentCell.y - i) > 1 )) {
        let key = Position.getRoutKey((currentCell.x+i,currentCell.y-i))
        if (gridFilling[key] == 1) {
          newPoint[2]!.x = currentCell.x + i
          newPoint[2]!.y = currentCell.y - i
        }
      }
      
      if (currentCell.y - i) > 1 {
        let key = Position.getRoutKey((currentCell.x,currentCell.y-i))
        if (gridFilling[key] == 1) {
          newPoint[3]!.x = currentCell.x
          newPoint[3]!.y = currentCell.y - 1
        }
      }
      
      if (((currentCell.x - i) > 1) && ((currentCell.y - i) > 1 )) {
        let key = Position.getRoutKey((currentCell.x-i,currentCell.y-i))
        if (gridFilling[key] == 1) {
          newPoint[4]!.x = currentCell.x - i
          newPoint[4]!.y = currentCell.y - i
        }
      }
    
      if (currentCell.x - i) > 1 {
        let key = Position.getRoutKey((currentCell.x-i,currentCell.y))
        if (gridFilling[key] == 1) {
          newPoint[5]!.x = currentCell.x - i
          newPoint[5]!.y = currentCell.y
        }
      }
      
      if (((currentCell.x - i) > 1) && ((currentCell.y + i) < gridRow )) {
        let key = Position.getRoutKey((currentCell.x-i,currentCell.y+i))
        if (gridFilling[key] == 1) {
          newPoint[6]!.x = currentCell.x - i
          newPoint[6]!.y = currentCell.y + i
        }
      }
      
      if (currentCell.x + i) < gridRow {
        let key = Position.getRoutKey((currentCell.x,currentCell.y+i))
        if (gridFilling[key] == 1) {
          newPoint[7]!.x = currentCell.x
           newPoint[7]!.y = currentCell.y + i
        }
      }
      
      if (((currentCell.x + i) < gridColumn) && ((currentCell.y + i) < gridRow )) {
        let key = Position.getRoutKey((currentCell.x+i,currentCell.y+i))
        if (gridFilling[key] == 1) {
          newPoint[8]!.x = currentCell.x + i
          newPoint[8]!.y = currentCell.y + i
        }
      }
      
      for j in 1...8 {
        if newPointPath[j] == 0 {
          newPointPath[j] = CalculationConstant.infinitePath
        } else {
          newPointPath[j] = pathLength (prevCell , endGridCell : newPoint[j]!)
        }
        
        if newPointPath[j] != CalculationConstant.infinitePath {
          ready = true
        }
      }
      
      if i>500 {
        print("INFINITIVE LOOOP")
      }
      
    } while (ready == false && i < (gridColumn / 2))
   
    calculationLog.append("newPoint:")
    calculationLog.append(newPoint.description)
    calculationLog.append("newPointPath:")
    calculationLog.append(newPointPath.description)
    if ready {
      var newPoint2 : [Int : Double] = [:]
      for (key,value) in newPointPath {
        newPoint2[key] = Double(value)
      }
      
      
      let minPath = sortAndSlice(newPoint2,count: 1)
      calculationLog.append("minPath:")
      calculationLog.append(minPath.description)
      for (key,_) in minPath {
        res = newPoint[key]!
      }
      
      
    }
    calculationLog.append("Result nextFreeCell:")
     calculationLog.append("X:\(res.x),Y:\(res.y)")
    calculationLog.append("------------------------")
    return res
  }
  
  func pathLength (_ startGridCell : IndAliases.FillingCoordinate , endGridCell : IndAliases.FillingCoordinate) -> Int {
    calculationLog.append("------------------------")
    calculationLog.append("Func: pathLength")
    
    var res : Int = CalculationConstant.infinitePath
    var i : Int = 1
    var routMap : [String : Int] = [:]
    
    for x in 1...gridColumn {
      for y in 1...gridRow {
        routMap[Position.getRoutKey((x,y))] = 0
      }
    }
    
    routMap[Position.getRoutKey(startGridCell)] = 1
    
    repeat {
      i += 1
      for y in 1...gridRow {
        for x in 1...gridColumn {
          if routMap[Position.getRoutKey((x,y))] == (i-1) {
            if (y < gridRow && routMap[Position.getRoutKey((x,(y+1)))] == 0 && gridFilling[Position.getRoutKey((x,(y+1)))] == 1) {
                routMap[Position.getRoutKey((x,(y+1)))] = i
            }
            
            if (y > 1 && routMap[Position.getRoutKey((x,(y-1)))] == 0 && gridFilling[Position.getRoutKey((x,(y-1)))] == 1) {
              routMap[Position.getRoutKey((x,(y-1)))] = i
            }
            
            if (x < gridColumn && routMap[Position.getRoutKey(((x+1),y))] == 0 && gridFilling[Position.getRoutKey(((x+1),y))] == 1) {
              routMap[Position.getRoutKey(((x+1),y))] = i
            }
            
            if (x > 1 && routMap[Position.getRoutKey(((x-1),y))] == 0 && gridFilling[Position.getRoutKey(((x-1),y))] == 1) {
              routMap[Position.getRoutKey(((x-1),y))] = i
            }
            
          }
        }
      }
      if i>500 {
        print("INFINITIVE LOOOP 2 - \(i)")
      }
      calculationLog.append("Repeat #:\(i)")
    } while ((routMap[Position.getRoutKey(endGridCell)] == 0) && (i < (gridColumn * gridRow)))
    
    if routMap[Position.getRoutKey(endGridCell)] > 0 {
      res = i - 1
    }
    calculationLog.append("Result pathLength:")
    calculationLog.append("\(res)")
    calculationLog.append("------------------------")
    return res
  }
  
  func correctPosition(_ prevPosition : CGPoint, currentPosition : CGPoint ) -> CGPoint {
    calculationLog.append("------------------------")
    calculationLog.append("Func: correctPosition")
    let prevCell = convertPointToCell(prevPosition)
    let currentCell = convertPointToCell(currentPosition)
    var res = currentCell
    var fakePoint = false
    
    if (prevCell.x == currentCell.x && prevCell.y == currentCell.y) {
      return convertCellToPoint(res)
    }
    
    let key = Position.getRoutKey((currentCell.x,currentCell.y))
    if gridFilling[key] == 1 {
      let s = pathLength(prevCell, endGridCell: currentCell)
      
      if s == CalculationConstant.infinitePath {
        res = nextFreeCell(prevCell, currentCell: currentCell)
      } else {
        if (s * step!) > Int(CalculationConstant.maxRelocation) {
          fakePoint = true
        }
      }
    } else {
      res = nextFreeCell(prevCell, currentCell: currentCell)
    }
    
    if fakePoint && currentFakePoint < CalculationConstant.maxFakePoints {
      currentFakePoint += 1
      res = prevCell
    } else {
      currentFakePoint = 0
    }
    
    let resPoint = convertCellToPoint(res)
    calculationLog.append("Result correctPosition:")
    calculationLog.append("X:\(resPoint.x),Y:\(resPoint.y)")
    calculationLog.append("------------------------")
    return resPoint
  }
    
  //MARK: Helper functions
   func convertPointToCell(_ loc : CGPoint) -> IndAliases.FillingCoordinate {
    calculationLog.append("------------------------")
    calculationLog.append("Func: convertPointToCell")
    let res = (getCellIndex(loc.x),getCellIndex(loc.y))
    calculationLog.append("Result convertPointToCell:")
    calculationLog.append("\(res)")
    calculationLog.append("------------------------")
    return res
    
  }
  
  func getCellIndex(_ coordinate : CGFloat) -> Int {
    calculationLog.append("------------------------")
    calculationLog.append("Func: getLocation")
    let res = Int(ceil(Double(Double(coordinate)/Double(step!))))
    calculationLog.append("Result getLocation:")
    calculationLog.append("\(res)")
    calculationLog.append("------------------------")
    return res
  }
    
  static func getRoutKey(_ crd : IndAliases.FillingCoordinate) -> String {
    return "\(crd.x),\(crd.y)"
  }
  
  
  //MARK: - Get location
  
  
  
  func convertCellToPoint(_ cell : IndAliases.FillingCoordinate) -> CGPoint {
    calculationLog.append("------------------------")
    calculationLog.append("Func: convertCellToPoint")
    let x = CGFloat((Double(cell.x) - 0.5) * Double(step!))
    let y = CGFloat((Double(cell.y) - 0.5) * Double(step!))
    let resPoint = CGPoint(x: x, y: y)
    calculationLog.append("Result convertCellToPoint:")
    calculationLog.append("X:\(resPoint.x),Y:\(resPoint.y)")
    calculationLog.append("------------------------")
    return resPoint
  }
  
  func getLocation(_ inputData: [BeaconsData]) -> CGPoint {
    calculationLog.removeAll()
    calculationLog.append("------------------------")
    calculationLog.append("Func: getLocation")
    startCalcTime = Date().timeIntervalSince1970
    let beaconsData : [Int : Double] = prepareBeaconsData(inputData)
    
    if beaconsData.count == 0 {
      return previousPosition
    }

    
    currentPosition = weightedCentroid(beaconsData)
    if previousPosition == CGPoint.zero {
        previousPosition = currentPosition
    }
    
    let prevForLog = previousPosition
    currentPosition = filterKalman(previousPosition, crntPosition: currentPosition)
    currentPosition = correctPosition(previousPosition, currentPosition: currentPosition)
    previousPosition = currentPosition
    endCalcTime = Date().timeIntervalSince1970
    var resPointArr : [Int] = []
    resPointArr.append(Int(currentPosition.x))
    resPointArr.append(Int(currentPosition.y))
    let curPointId = debugData.addPointData(inputData, resultPoint: resPointArr, beginTime: startCalcTime!, endTime: endCalcTime!)
    let debugLog : iDebugLog = DebugLog(prevPoint: prevForLog, curPoint: currentPosition, calculationLog: calculationLog, startTime: startCalcTime!, endTime: endCalcTime!, pointId: curPointId)
    let wrappedStruct = WrapperStruct(theValue: debugLog)
    
    NotificationCenter.default.post(name: Notification.Name(rawValue: updateLogNotification), object: nil, userInfo: ["log" : wrappedStruct])
    
    calculationLog.append("Result getLocation:")
    calculationLog.append("X:\(currentPosition.x),Y:\(currentPosition.y)")
    calculationLog.append("------------------------")
    
    return currentPosition
  }
}
