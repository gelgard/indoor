//
//  VCSettings.swift
//  IndoorApp
//
//  Created by Oleg on 30.09.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import IndoorEngine
import IndoorModel
import IndoorAPIManager
//import IndoorDataStorageWrapper
import IndoorCDManager
import Crashlytics
import IndoorDownloadWrapper
import IndoorAlert


class VCSettings:  VCBase {
  
  // MARK: - Outlets
  
  @IBOutlet weak var showDebugLbl: UILabel!
  @IBOutlet weak var rotateLbl: UILabel!
  @IBOutlet weak var planeNameLbl: UILabel!
  @IBOutlet weak var planeIdLbl: UILabel!
  @IBOutlet weak var planeNameVal: UILabel!
  @IBOutlet weak var planeIdVal: UILabel!
  @IBOutlet weak var planeVersionLbl: UILabel!
  @IBOutlet weak var planeVersionVal: UILabel!
  @IBOutlet weak var loadImagesVal: UILabel!
  @IBOutlet weak var useBeaconsOnlyInRoomLbl: UILabel!
  @IBOutlet weak var loadPlanButton: UIButton!
  @IBOutlet weak var refreshPlanButton: UIButton!
  @IBOutlet weak var downloadPlanButton: UIButton!
  @IBOutlet weak var plansTableHeight: NSLayoutConstraint!
  @IBOutlet weak var plansTablePanel : UIView!
  @IBOutlet weak var jsonTable: UITableView!
  @IBOutlet weak var selectObject: UILabel!
  @IBOutlet weak var showDebugPanelSwitch: UISwitch!
  @IBOutlet weak var loadImages: UISwitch!
  @IBOutlet weak var useBeaconsOnlyInRoomSwitch: UISwitch!
  @IBOutlet weak var screenshotsSwitch: UISwitch!
  @IBOutlet weak var rotateSegment: UISegmentedControl!
  @IBOutlet weak var selectObjectSegment : UISegmentedControl!
  @IBOutlet weak var beaconLocationMarkerSwitch: UISwitch!
  @IBOutlet weak var tapLocationMarkerSwitch: UISwitch!
  @IBOutlet weak var lookMarkerSwitch: UISwitch!
  @IBOutlet weak var spotMarkerSwitch: UISwitch!
  @IBOutlet weak var beaconsSignalsMarkerSwitch: UISwitch!
  @IBOutlet weak var showGridSwitch: UISwitch!
  @IBOutlet weak var showIntersectedObject: UISwitch!

  
  // MARK: - Properties
  
  let textCellIdentifier = "JSONCell"
  
  //let locale = "en"
  let folderPath = "images"
  var alertController: IAlert?
  let planPanelBaseHeight = 130.0
  let planPanelExpandedHeight = 400.0
  var planPanelIsHidden = true
  var downloadWrapper: IDownloadWrapper = IndoorDownloadWrapper()
  
  fileprivate var updatedControls = false
  fileprivate var dbManager:IDSManager?
  fileprivate var apiManager = IndoorAPIManager()
  fileprivate var planNameArr : [String] = []
  fileprivate var planVersionArr : [String] = []
  fileprivate var planIdArr : [String] = []
  fileprivate var selectedId : Int = 0
  // MARK: - Init & override
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    plansTablePanel.isHidden = planPanelIsHidden
    plansTableHeight.constant = CGFloat(planPanelBaseHeight)
    self.jsonTable.delegate = self
    self.jsonTable.dataSource = self
    refreshPlanButton.isHidden = true
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    if self.dbManager == nil {
      do {
        self.dbManager = IndoorCDManager()
        getCurrentPlanData()
      } catch let error {
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
      }
      
    }
    
    
  }
  
  override func viewWillDisappear(_ animated: Bool)
  {
    super.viewWillDisappear(animated)

  }
  
  override func rotated() {
    
  }
  override func localize() {
    showDebugLbl.text = "SHOW_DEBUG_PANEL_LBL".localized
    loadImagesVal.text = "LOAD_IMAGES".localized
    rotateLbl.text = "ROTATE_LBL".localized
    selectObject.text = "SELECT_OBJECT_LBL".localized
    planeNameLbl.text = "PLANE_NAME".localized
    planeVersionLbl.text = "PLANE_VERSION".localized
    planeIdLbl.text = "PLANE_ID".localized
    useBeaconsOnlyInRoomLbl.text = "ONLY_IN_ROOM_BEACON_LBL".localized
    rotateSegment.setTitle("ROTATE_SEGMENT_0".localized, forSegmentAt: 0)
    rotateSegment.setTitle("ROTATE_SEGMENT_1".localized, forSegmentAt: 1)
    selectObjectSegment.setTitle("SELECT_OBJECT_SEGMENT_0".localized, forSegmentAt: 0)
    selectObjectSegment.setTitle("SELECT_OBJECT_SEGMENT_1".localized, forSegmentAt: 1)
    loadPlanButton.setTitle("LOAD_PLAN".localized, for: .normal)
    downloadPlanButton.setTitle("DOWNLOAD_PLAN".localized, for: .normal)
    refreshPlanButton.setTitle("REFRESH_PLAN".localized, for: .normal)
  }
  
  //MARK: inner logic 
  
  func getPlanList() {
    
    
    jsonTable.isHidden = true
    planNameArr.removeAll()
    planIdArr.removeAll()
    planVersionArr.removeAll()
    
    do {

      let planList = PlansList()
    
      let resultList : (([String:IPlan]) -> ()) = { (list)
        in
        for (key,plan) in list {
          self.planIdArr.append(key)
          self.planNameArr.append("\(plan.name) (version:\(plan.version))")
        }
        
        self.jsonTable.isHidden = false
        self.jsonTable.reloadData()
      }
      
      planList.getPlansListFullInfo(planList: resultList)
    } catch let error {
      ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }
    

  }
  
  func switchPlanPanel() {
    
    if planPanelIsHidden {
      UIView.animate(withDuration: 0.5, animations: {
        self.planPanelIsHidden = false
        self.plansTableHeight.constant = CGFloat(self.planPanelExpandedHeight)
        self.plansTablePanel.isHidden = self.planPanelIsHidden
        self.view.layoutIfNeeded()
      })
    } else {
      UIView.animate(withDuration: 0.5, animations: {
        self.planPanelIsHidden = true
        self.plansTableHeight.constant = CGFloat(self.planPanelBaseHeight)
        self.plansTablePanel.isHidden = self.planPanelIsHidden
        self.view.layoutIfNeeded()
      })
    }
    
    refreshPlanButton.isHidden = self.planPanelIsHidden
    
  }
  
  func getCurrentPlanData() {
    
    do {
      
      let planData = try dbManager?.planDataStorage.getCurrentPlan()
      
      if planData != nil {
        planeNameVal.text = planData?.name
        let planId : String! = String(describing: planData!.plan_id)
        let planVersion : String! = String(describing: dbManager!.planDataStorage.version)
        planeIdVal.text = planId!
        planeVersionVal.text = planVersion!
        
      }
      
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }
    
  }
  
  override func updateControlsFromSettings() {
    if !updatedControls {
      showDebugPanelSwitch.isOn = self.settings.showDebugPanel
      loadImages.isOn = self.settings.loadImages
      screenshotsSwitch.isOn = self.settings.screenshotsEnabled
      useBeaconsOnlyInRoomSwitch.isOn = self.settings.useBeaconsOnlyInRoom
      rotateSegment.selectedSegmentIndex = self.settings.rotate.rawValue
      selectObjectSegment.selectedSegmentIndex = self.settings.selectObject.rawValue
      
      beaconLocationMarkerSwitch.isOn = self.settings.beaconLocationMarker
      lookMarkerSwitch.isOn = self.settings.lookMarker
      spotMarkerSwitch.isOn = self.settings.spotMarker
      beaconsSignalsMarkerSwitch.isOn = self.settings.beaconsSignalsMarker
      showGridSwitch.isOn = self.settings.showGrid
      tapLocationMarkerSwitch.isOn = self.settings.tapLocationMarker
      showIntersectedObject.isOn = self.settings.showIntersectedObjects
      
      
      updatedControls = true
      
      
      
      
      //TABLE self.jsonTable.selectRow(at: IndexPath(row: self.settings.currentPlan,section: 0), animated: false, scrollPosition: .top)
    }
    
  }
  
  //MARK: Actions
  
  @IBAction func crashButtonTapped(_ sender: AnyObject) {
    Crashlytics.sharedInstance().crash()
  }

  
  @IBAction func refreshPlanTable(_ sender: AnyObject) {
    getPlanList()
  }
  
  @IBAction func expandTablePanel(_ sender: AnyObject) {
    getPlanList()
    switchPlanPanel()
  }
  
  @IBAction func updateSettings(_ sender: AnyObject) {
    
    self.settings.showDebugPanel = showDebugPanelSwitch.isOn
    self.settings.loadImages = loadImages.isOn
    self.settings.screenshotsEnabled = screenshotsSwitch.isOn
    self.settings.useBeaconsOnlyInRoom = useBeaconsOnlyInRoomSwitch.isOn
    self.settings.rotate = IndParams.SettingRotate(rawValue: rotateSegment.selectedSegmentIndex)!
    self.settings.selectObject = IndParams.SettingSelectObject(rawValue: selectObjectSegment.selectedSegmentIndex)!
    
    self.settings.beaconLocationMarker = beaconLocationMarkerSwitch.isOn
    self.settings.lookMarker = lookMarkerSwitch.isOn
    self.settings.spotMarker = spotMarkerSwitch.isOn
    self.settings.beaconsSignalsMarker = beaconsSignalsMarkerSwitch.isOn
    self.settings.showGrid = showGridSwitch.isOn
    self.settings.tapLocationMarker = tapLocationMarkerSwitch.isOn
    self.settings.showIntersectedObjects = showIntersectedObject.isOn
  }
  
  func showProgress() -> (progress: ((Float, String)->Void), complition: ((Error?)->Void)){
    
    if self.alertController == nil {
      self.alertController = IndoorAlertController()
    }
    
    let alertVC = self.alertController!
    
    alertVC.setTheme(theme: self.museumTheme)
    alertVC.setCloseButton(visible: false)
    
    alertVC.showAlert(in: nil, with: .progress, typeText: nil, message: "") { (_) -> (Void) in
    }
    
    let progress: ((Float, String)->Void) = { progress, message in
      alertVC.updateProgressView(progressValue: Double(progress), typeText: nil, message: message)
      //                progressView.progressView.progress = progress
      //                progressView.titleLabel.text = title
    }
    let complition: ((Error?)->Void) = { error in
      DispatchQueue.main.async {
        alertVC.setCloseButton(visible: true)
        if error != nil {
          alertVC.showAlert(with: .error, typeText: nil, message: "ERROR in progress:\n\(error!.localizedDescription)", callback: { _ in
            alertVC.closeAlert()
          })
          //                    progressView.titleLabel.text = "ERROR in progress:\n\(error!.localizedDescription)"
        } else {
          alertVC.showAlert(with: .info, typeText: nil, message: "Download success", callback: { (_) -> (Void) in
            alertVC.closeAlert()
          })
          //                    progressView.titleLabel.text = "Download success"
        }
      }
    }
    return (progress, complition)
  }
  
  
  @IBAction func getDataForPlan(_ sender: AnyObject) {
    
    
    //self.selectedId
    
    
    let handlers = self.showProgress()
    self.downloadWrapper.load(planById: self.selectedId, withContent: settings.loadImages).progress({ (operation, progress) in
      let title = String.init(format: "Operation: \(operation) \n in progress: %1.2f", progress)
      handlers.progress(Float(progress), title)
    }).completion { (error) in
      handlers.complition(error)
      guard error == nil else {
        print("ERROR (ContentLoader):")
        print(error!.localizedDescription)
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        
        return
      }
      
      self.settings.currentPlan = self.selectedId
      self.switchPlanPanel()
      self.getCurrentPlanData()
      print("SUCCESS (ContentLoader): ended download")
    }
    
  
    /*
    var progressV : ((Double) -> Void)?
    
    
    let progressValue : ((Double, IndTypes.TaskList) -> Void) = { (value : Double, task : IndTypes.TaskList)
      
      in
      
      if progressV != nil {
        progressV!(value)
      }
      
      
    }
    

    let currentTaskTest : ((IndTypes.TaskList,IndTypes.TaskProcess,String,String,IndTypes.DialogType) -> Void ) = {
      ( task : IndTypes.TaskList,process : IndTypes.TaskProcess,title : String,message : String,type : IndTypes.DialogType) in
      
      //DispatchQueue.main.async {
        if self.alertHelper.isShown == false {
          progressV = self.alertHelper.showAlertView(withType: type, typeText: title, message: message, theme: self.museumTheme, frame: self.view.bounds) { (result) in
            print("Result : \(result)")
            self.settings.currentPlan = self.selectedId
            self.switchPlanPanel()
            self.getCurrentPlanData()
          }
          
          
        } else {
          
          if (Thread.isMainThread == true) {
            self.alertHelper.updateAlertView(withType: type, typeText: type.labelText(), message: message)
          } else {
            DispatchQueue.main.sync() {
              self.alertHelper.updateAlertView(withType: type, typeText: type.labelText(), message: message)
            }
          }
          
          
          
        }
    
      
    }
    
    do {
      
      let wrapper = try DataStorageWrapper(planId: selectedId, locale: DBManagerConstants.localeForDevice!, folderPath: folderPath)
      wrapper.currentProgress = progressValue
      wrapper.currentTask = currentTaskTest
      wrapper.executeUpdate(completion: { (error) in
        
        
        
      })
      
      
      
    } catch {
      
      
    }
  */
  }
 
  
}

extension VCSettings : UITableViewDelegate, UITableViewDataSource {
  //MARK: - BeaconMonitoringDelegate
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return planIdArr.count
  }
  
  internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)
    
    let row = indexPath.row
    cell.textLabel?.text = planNameArr[row]
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print(indexPath.row)
    selectedId = Int(planIdArr[indexPath.row])!
  }
}

