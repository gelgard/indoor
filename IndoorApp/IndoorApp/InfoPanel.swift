//
//  InfoPanel.swift
//  IndoorApp
//
//  Created by Oleg on 21.03.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import IndoorEngine
import IndoorModel

class InfoPanel: UIView {
    
  @IBOutlet weak var view: UIView!

  
  @IBOutlet weak var collectionContentView: UIView!
  @IBOutlet var collectionView: UICollectionView!
  @IBOutlet private var nextButton: UIButton!
  @IBOutlet private var previousButton: UIButton!
  @IBOutlet var pageControl: UIPageControl!
  
    
  var storyBrd : UIStoryboard?
  var navig: UINavigationController?
  var currentObject : IObject?
  var dataSourceObjects : [IObject] = []
  var animationsCount = 0
  private var collectionViewLayout: LGHorizontalLinearFlowLayout!
  var pageWidth: CGFloat {
    return self.collectionViewLayout.itemSize.width + self.collectionViewLayout.minimumLineSpacing
  }
  
  var contentOffset: CGFloat {
    return self.collectionView.contentOffset.x + self.collectionView.contentInset.left
  }
  //MARK: - COnfiguring Collection View
  
  private func configureCollectionView() {
    self.collectionView.register(UINib(nibName: "ObjectCell", bundle: nil), forCellWithReuseIdentifier: "ObjectCell")
    self.collectionViewLayout = LGHorizontalLinearFlowLayout.configureLayout(collectionView: self.collectionView, itemSize: kCollectionSize
      , minimumLineSpacing: 0)
  }
  
  private func configurePageControl() {
    self.pageControl.numberOfPages = self.dataSourceObjects.count
  }
  
  func configureButtons() {
    self.nextButton.isEnabled = self.dataSourceObjects.count > 0 && self.pageControl.currentPage < self.dataSourceObjects.count - 1
    self.previousButton.isEnabled = self.pageControl.currentPage > 0
  }
  // MARK: Actions
  
  @IBAction private func pageControlValueChanged(sender: AnyObject) {
    self.scrollToPage(page: self.pageControl.currentPage, animated: true)
  }
  
  @IBAction private func nextButtonAction(sender: AnyObject) {
    self.scrollToPage(page: self.pageControl.currentPage + 1, animated: true)
  }
  
  @IBAction private func previousButtonAction(sender: AnyObject) {
    self.scrollToPage(page: self.pageControl.currentPage - 1, animated: true)
  }
  
  func scrollToPage(page: Int, animated: Bool) {
    self.collectionView.isUserInteractionEnabled = false
    self.animationsCount += 1
    let pageOffset = CGFloat(page) * self.pageWidth - self.collectionView.contentInset.left
    //CGPointMake(pageOffset, 0)
    self.collectionView.setContentOffset(CGPoint(x: pageOffset,y: 0), animated: true)
    self.pageControl.currentPage = page
    self.configureButtons()
  }
  
    //MARK : - Init
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required  init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
      view = loadViewFromNib()
      view.frame = bounds
      view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
      self.configureCollectionView()
      self.configurePageControl()
      self.configureButtons()
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "InfoPanel", bundle: bundle)
      
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
  
    //MARK : - Inner logic
  
    func loadData(objects : [IObject]) {
        //HARDCODE
      self.dataSourceObjects.removeAll()
      self.dataSourceObjects.append(contentsOf: objects)
      self.collectionView.reloadData()
      
        
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let vc = self.storyBrd?.instantiateViewController(withIdentifier: "detaileObject")
//        (vc as! VCObjectDetaile).currentObject = self.currentObject
//        self.navig?.pushViewController(vc!, animated: true)
//        
//    }
  
    
}


extension InfoPanel: UICollectionViewDataSource, UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.dataSourceObjects.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ObjectCell", for: indexPath) as! ObjectCell
    collectionViewCell.labelObject.text = "\(self.dataSourceObjects[indexPath.row].object_id)"
    return collectionViewCell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
      return
    }
    
    let selectedPage = indexPath.row
    
    if selectedPage == self.pageControl.currentPage {
      NSLog("Did select center item")
    }
    else {
      self.scrollToPage(page: selectedPage, animated: true)
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    self.pageControl.currentPage = Int(self.contentOffset / self.pageWidth)
    self.configureButtons()
  }
  
  func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    if self.animationsCount - 1  == 0 {
      self.collectionView.isUserInteractionEnabled = true
    }
  }
  
}
