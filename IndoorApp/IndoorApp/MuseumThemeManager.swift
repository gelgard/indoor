//
//  MuseumThemeManager.swift
//  museumGuideNav
//
//  Created by Oleg on 12.04.17.
//  Copyright © 2017 UranCompany. All rights reserved.
//

import UIKit
import Foundation
import IndoorModel




//"HelveticaNeueCyr-Heavy", "HelveticaNeueCyr-BoldItalic", "HelveticaNeueCyr-ThinItalic", "HelveticaNeueCyr-UltraLightItalic", "HelveticaNeueCyr-UltraLight", "HelveticaNeueCyr-BlackItalic", "HelveticaNeueCyr-Medium", "HelveticaNeueCyr-LightItalic", "HelveticaNeueCyr-Roman", "HelveticaNeueCyr-Black", "HelveticaNeueCyr-Thin", "HelveticaNeueCyr-Italic", "HelveticaNeueCyr-MediumItalic", "HelveticaNeueCyr-HeavyItalic", "HelveticaNeueCyr-Light", "HelveticaNeueCyr-Bold"

//"HelveticaNeueCyr-Heavy", "HelveticaNeueCyr-BoldItalic", "HelveticaNeueCyr-ThinItalic", "HelveticaNeueCyr-UltraLightItalic", "HelveticaNeueCyr-UltraLight", "HelveticaNeueCyr-BlackItalic", "HelveticaNeueCyr-Medium", "HelveticaNeueCyr-LightItalic", "HelveticaNeueCyr-Roman", "HelveticaNeueCyr-Black", "HelveticaNeueCyr-Thin", "HelveticaNeueCyr-Italic", "HelveticaNeueCyr-MediumItalic", "HelveticaNeueCyr-HeavyItalic", "HelveticaNeueCyr-Light", "HelveticaNeueCyr-Bold"

//["Helvetica-Condensed-Thin"]



public struct SwitcherColors {
  
  public init(backgroudColor : UIColor, activeColor : UIColor, nonActiveColor : UIColor) {
    self.backgroudColor = backgroudColor
    self.activeColor = activeColor
    self.nonActiveColor = nonActiveColor
  }
  
  public var backgroudColor : UIColor
  public var activeColor : UIColor
  public var nonActiveColor : UIColor
}

public class MuseumThemeManager: ThemeManager  {
  
  let mainRegularFontName = "HelveticaNeueCyr-Roman"
  let mainBoldFontName = "HelveticaNeueCyr-Bold"

  
  public var switcherColors : SwitcherColors {
    
    get {
      return SwitcherColors(backgroudColor: #colorLiteral(red: 0.9019607843, green: 0.9058823529, blue: 0.9098039216, alpha: 1) ,
                            activeColor: #colorLiteral(red: 0.3647058824, green: 0.368627451, blue: 0.7215686275, alpha: 1) ,
                            nonActiveColor: #colorLiteral(red: 0.3647058824, green: 0.368627451, blue: 0.7215686275, alpha: 1))
    }
  }
  
  override public var blockingViewTheme: BlockingViewTheme {
    return BlockingViewTheme(cornerRadius: 10.0, glyphImages: getDialogGlyphImages(), typeLabelColors: getTypeLabelColors(),
                             contentBackground: #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1) ,
                             messageFont: UIFont(name: mainRegularFontName, size: 22.0)!,
                             typeFont: UIFont(name: mainRegularFontName, size: 28.0)!,
                             infoViewMargin: 20.0)
  }
  
  override public var applicationsColors : ApplicationColors {
    
    get {
      return ApplicationColors(mainPanelColor: #colorLiteral(red: 0.3098039216, green: 0.3137254902, blue: 0.6666666667, alpha: 1) ,
                               mainPanelColorLight: #colorLiteral(red: 0.3529411765, green: 0.3607843137, blue: 0.7647058824, alpha: 1) ,
                               mainPanelColorDark: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.6078431373, alpha: 1) ,
                               additionalPanelColor: #colorLiteral(red: 0.3176470588, green: 0.7529411765, blue: 1, alpha: 1) ,
                               negativeButtonColor: #colorLiteral(red: 0.631372549, green: 0.6392156863, blue: 0.7647058824, alpha: 1) ,
                               buttonColor: #colorLiteral(red: 0.3176470588, green: 0.7529411765, blue: 1, alpha: 1) ,
                               mainTextClor: #colorLiteral(red: 0.2980392157, green: 0.2980392157, blue: 0.2980392157, alpha: 1) ,
                               buttonTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
  }
  
  override public var applicationFonts: ApplicationFonts {
    
    get {
      return ApplicationFonts(btnFont: UIFont(name: mainBoldFontName, size: 18.0)!, lblFont: UIFont(name: mainRegularFontName, size: 16.0)!)
    }
    
  }
  
  
  override public var navPanelImages: NavPanelImages {
    
    get {
      let homeBtnNoAct = UIImage(named: "homeNoActBtn")
      let menuBtnNoAct = UIImage(named: "menuNoactBtn")
      let settBtnNoAct = UIImage(named: "settNoactBtn")
      let listBtnNoAct = UIImage(named: "listNoactBtn")
      let homeBtnAct = UIImage(named: "homeActBtn")
      let menuBtnAct = UIImage(named: "menuActBtn")
      let settBtnAct = UIImage(named: "settActBtn")
      let listBtnAct = UIImage(named: "listActBtn")
      
      return NavPanelImages(activeImages: [homeBtnAct!,listBtnAct!,settBtnAct!,menuBtnAct!], nonActiveImages: [homeBtnNoAct!,listBtnNoAct!,settBtnNoAct!,menuBtnNoAct!])
    }
  }
  
  func getDialogGlyphImages() -> [IndTypes.DialogType : UIImage] {
    
    var res : [IndTypes.DialogType : UIImage] = [:]
    
    for dlgType in IndTypes.DialogType.allValues {
        let img = UIImage() //FIXME: remove from here
      res[dlgType] = img
    }
    
    return res
  }
  
  func getTypeLabelColors() -> [IndTypes.DialogType : UIColor] {
    
    var res : [IndTypes.DialogType : UIColor] = [:]
    
    res[IndTypes.DialogType.info] = #colorLiteral(red: 0.3176470588, green: 0.7529411765, blue: 1, alpha: 1)
    res[IndTypes.DialogType.alert] = #colorLiteral(red: 0.3176470588, green: 0.7529411765, blue: 1, alpha: 1)
    res[IndTypes.DialogType.error] = #colorLiteral(red: 0.9450980392, green: 0.1411764706, blue: 0.3882352941, alpha: 1)
    res[IndTypes.DialogType.warning] = #colorLiteral(red: 0.9450980392, green: 0.1411764706, blue: 0.3882352941, alpha: 1)
    res[IndTypes.DialogType.progress] = #colorLiteral(red: 0.3490196078, green: 0.368627451, blue: 0.6784313725, alpha: 1)
    
    return res
    
  }
 
  
  // MARK: - Theme for IndoorTable Header Categories
  override public var indoorTableCategoriesHeader: IndoorCategoriesViewThemeManager {
    return IndoorCategoriesViewThemeManager(backgroundColor: .clear,
                                            height: 50,
                                            contentInsets: .init(top: 0, left: 10, bottom: 0, right: 10),
                                            showScrollIndicator: false,
                                            itemSpasing: 5,
                                            borderColor: .red,
                                            borderWidth: 1,
                                            borderRadius: 5,
                                            cellBackground: .clear,
                                            cellSelectedColor: .darkGray,
                                            cellUnselectedColor: .lightGray,
                                            cellLableFont: .systemFont(ofSize: 18),
                                            cellLabelSelectedTextColor: .darkGray,
                                            cellLabelUnselectedTextColor: .lightGray,
                                            cellUnderLineHeight: 3,
                                            cellBorderColor: .clear,
                                            cellBorderWidth: 0,
                                            cellBorderRadius: 0,
                                            emptyPlaceholder: "Categories list is empty.",
                                            emptyTextColor: nil,
                                            emptyTextFont: nil)
  }
  
  // MARK: - Theme for IndoorTable Header Search
  override public var indoorTableSearchHeader: IndoorSearchViewThemeManager {
    return IndoorSearchViewThemeManager(height: 50,
                                        placeholder: "Search",
                                        promt: "",
                                        searchStyle: .default,
                                        barStyle: .default,
                                        transluent: true,
                                        barTintColor: applicationsColors.mainPanelColorLight,
                                        tintColor: nil,
                                        background: .clear,
                                        cancelEnabled: false)
  }
  
  // MARK: - Theme for IndoorTable
 
  
  override public var indoorGalleryCell: IndoorGalleryCellTheme {
    return IndoorGalleryCellTheme(colors:                   self.indoorGalleryCellColors,
                                  settings:                 self.indoorGalleryCellSettings,
                                  fonts:                    self.indoorGalleryCellFonts,
                                  images:                   self.indoorGalleryCellButtonsImages,
                                  imageCell:                self.indoorGalleryCellCollectionCell,
                                  carouselFlowLayoutTheme:  self.indoorGalleryCollectionFlowLayout)
  }
  
  override public var indoorGalleryCellCollectionCell: IndoorCollectionGalleryCell {
    return IndoorCollectionGalleryCell(background: .clear,
                                       textBackground: #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1215686275, alpha: 1).withAlphaComponent(0.7),
                                       borderColor: .clear,
                                       bordrtWidth: 0,
                                       contentRadius: 2,
                                       imageContentMode: .scaleAspectFill,
                                       labelFont: UIFont(name: mainRegularFontName, size: 18.0)!,
                                       labelBackgroundColor: #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 0.09770976027),
                                       textColor: .white)
  }
  
  // MARK: - Themes for IndoorGalleryCell
  override public var indoorGalleryCellColors: IndoorGalleryCellColors {
    return IndoorGalleryCellColors(background:      .clear,
                                   imageBackground: .lightGray,
                                   imageShadow:     UIColor.black.withAlphaComponent(0.9),
                                   buttonsTint:     .white,
                                   triangle:        .white,
                                   triangleShadow:  UIColor.black.withAlphaComponent(0.5),
                                   mainLabelText:   .black,
                                   imageLabelText:  .white,
                                   cellLabelText:   .yellow,
                                   infoButton:      UIColor(red:0.31, green:0.33, blue:0.65, alpha:1.0),
                                   locationButton:  UIColor(red:0.25, green:0.27, blue:0.58, alpha:1.0),
                                   mapButton:       UIColor(red:0.20, green:0.22, blue:0.51, alpha:1.0),
                                   collectionBackground: .clear,
                                   collectionBorder: .clear,
                                   collectionCellBorder: .black,
                                   collectionCellBackground: .clear,
                                   collectionCellBackgroundSelected: .clear,
                                   textBackground: #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1215686275, alpha: 1).withAlphaComponent(0.7))
  }
  override public var indoorGalleryCellFonts: IndoorGalleryCellFonts {
    return IndoorGalleryCellFonts(mainLabel:    UIFont(name: mainRegularFontName, size: 18.0)!,
                                  imageLabel:   UIFont(name: mainRegularFontName, size: 20.0)!,
                                  cellLabel:    UIFont(name: mainRegularFontName, size: 16.0)!)
  }
  override public var indoorGalleryCellSettings: IndoorGalleryCellSettings {
    return IndoorGalleryCellSettings(borderWidth:   0,
                                     borderRadius:  0,
                                     itemsSpacing:  20,
                                     shadowWidth: 7,
                                     shadowOffset: CGSize.init(width: -2, height: 0),
                                     mainLabelLeftInset: 40,
                                     mainLabelRightInset: 40,
                                     cellProportions: CGSize.init(width: 15, height: 9),
                                     cellHeightMultiplier: 0.5,
                                     cellScalingOffset: 100,
                                     cellMaxBlurFactor: 10,
                                     cellScalingMultiplier: 0.8,
                                     cellMinimumTextAlpha: 0,
                                     cellBorderRadius: 0,
                                     cellBorderWidth: 0,
                                     showScrollIndicator: false)
  }
  override public var indoorGalleryCellButtonsImages: IndoorGalleryCellCollectionButtonsImages {
    let bundle = Bundle.main
    let info = UIImage.init(named: "info", in: bundle, compatibleWith: nil)
    let location = UIImage.init(named: "location", in: bundle, compatibleWith: nil)
    let map = UIImage.init(named: "map", in: bundle, compatibleWith: nil)
    return IndoorGalleryCellCollectionButtonsImages(infoButtonImage: info,
                                                    locationButtonImage: location,
                                                    mapButtonImage: map)
  }
  
  // MARK: Themes for ObjectsInfoView
  override public var indoorObjectsInfoView: ObjectsInfoViewTheme {
    let bundle = Bundle.main
    let navigationBackIcon = UIImage.init(named: "backbtn", in: bundle, compatibleWith: nil)
    let navigationCloseIcon = UIImage.init(named: "infoCloseBtn", in: bundle, compatibleWith: nil)
    let nextBtn = UIImage.init(named: "arr_next", in: bundle, compatibleWith: nil)
    let prevBtn = UIImage.init(named: "arr_back", in: bundle, compatibleWith: nil)
    
    return ObjectsInfoViewTheme(backgroundColor: .clear,
                                titleBackgroundColor: .white,
                                titleTextFont: UIFont(name: mainBoldFontName, size: 24.0)!,
                                titleTextColor: .black,
                                titleLeftItemBackgroundColor: .clear,
                                titleLeftItemImage: prevBtn,
                                titleLeftItemTitle: "",
                                titleLeftItemTitleColor: nil,
                                titleRightItemBackgroundColor: .clear,
                                titleRightItemImage: nextBtn,
                                titleRightItemTitle: "",
                                titleRightItemTitleColor: nil,
                                enableScroll: false,
                                objectCellTheme: self.indoorObjectsInfoViewCell,
                                imagesViewTheme: self.indoorObjectsInfoViewImagesViewTheme,
                                navigationBackButtonImage:  navigationBackIcon,
                                navigationCloseButtonImage: navigationCloseIcon,
                                shouldShowChildObjects: true, loadingFileQuestion: "", loadingProgressMessage: "")
  }
  
  override public var indoorObjectsInfoViewCell: ObjectsInfoViewCellTheme {
    return ObjectsInfoViewCellTheme(backgroundColor: .white,
                                    galleryBackgroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
                                    descriptionBackgroundColor: .white,
                                    childrenBackgroundColor: .clear,
                                    descriptionTextFont: UIFont(name: mainRegularFontName, size: 19.0)!,
                                    descriptionTextColor: .black,
                                    regularWidthMinimum: 450,
                                    galleryToDescriptionOffset: 0,
                                    carouselTopOffset: 5,
                                    childrenViewHeight: 150,
                                    galleryCollectionView: indoorObjectsInfoViewGallery,
                                    childCarouselCollectionView: self.indoorObjectsInfoViewChildrenView)
  }
  override public var indoorObjectsInfoViewGallery: ObjectsInfoViewGalleryTheme {
    return ObjectsInfoViewGalleryTheme(backgroundColor: .blue,
                                       cellbackgroundColor: .yellow,
                                       direction: .horizontal,
                                       contetnInsets: .init(top: 5, left: 5, bottom: 5, right: 5),
                                       cellsOffset: 5,
                                       linesOffset: 5)
  }
  override public var indoorObjectsInfoViewChildrenView: IndoorCarouselCollectionView {
    return IndoorCarouselCollectionView(carouselFlowLayout: self.indoorGalleryCollectionFlowLayout,
                                        galleryCell: self.indoorGalleryCellCollectionCell)
  }
  
  override public var indoorObjectsInfoViewImagesViewTheme: ImagesGalleryViewTheme {
    
    let bundle = Bundle.main
    let front = UIImage.init(named: "arr_next", in: bundle, compatibleWith: nil) ?? UIImage()
    let back = UIImage.init(named: "arr_back", in: bundle, compatibleWith: nil) ?? UIImage()
    let close = UIImage.init(named: "infoCloseBtn", in: bundle, compatibleWith: nil) ?? UIImage()
    
    var imageCellTheme = self.indoorGalleryCellCollectionCell
    imageCellTheme.imageContentMode = .scaleAspectFit
    
    return ImagesGalleryViewTheme(backgroundEffect: .dark,
                                  backgroundColor: #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1).withAlphaComponent(0.25),
                                  backgroundAlpha: 1,
                                  imagesContentMode: .scaleAspectFit,
                                  carouselFlowLayoutTheme: self.indoorGalleryCollectionFlowLayout,
                                  imageCell: imageCellTheme,
                                  backImage: back,
                                  frontImage: front,
                                  closeImage: close,
                                  currentPageTintColor: .green,
                                  pageTintColor: #colorLiteral(red: 0.3176470588, green: 0.7529411765, blue: 1, alpha: 1))

  }
  override public var indoorGalleryCollectionFlowLayout: CarouselCollectionFlowLayoutTheme {
    return CarouselCollectionFlowLayoutTheme(scalingOffset: 100,
                                             minimumScaleFactor: 0.7,
                                             maxBlurFactor: 7,
                                             minimumTextAlpha: 0.5,
                                             scaleItems: true,
                                             itemProportions: CGSize.init(width: 2, height: 1),
                                             minimumInteritemSpacing: 10,
                                             minimumLineSpacing: 10,
                                             centerTextBackground: nil,//.blue,
                                             sideTextBackground: nil)//.green)
  }
  override public var iAlertResponseTheme: IAlertResponseViewTheme {
    return IAlertResponseViewTheme(sizeMultiplierForIPhone: CGSize(width: 0.9, height: 0.7),
                                   sizeMultiplierForIPad: CGSize(width: 0.8, height: 0.6),
                                   alertViewFrame: nil,
                                   titleColor: UIColor.color(hexString: "5DC1FD"),
                                   titleFont: .systemFont(ofSize: 17),
                                   responseColor: .black,
                                   responseFont: .systemFont(ofSize: 15),
                                   contentInsets: .init(top: 40, left: 20, bottom: 40, right: 20),
                                   componentsSpacing: 30,
                                   responseBorderWidth: 1, responseBorderColor: .lightGray,
                                   responseCornerRadius: 1)
  }
  
  
  override public var ialertTheme: IAlertTheme {
    let bundle = Bundle.main
    let close = UIImage.init(named: "closeBtn", in: bundle, compatibleWith: nil) ?? UIImage()
    
    return IAlertTheme(mainScreenBackgroundTransparency: 0.5,
                       alertViewCornerRadius: 5.0,
                       alertViewBackgroundColor: .white,
                       sizeMultiplierForIPhone: CGSize(width: 0.8, height: 0.6),
                       sizeMultiplierForIPad: CGSize(width: 0.5, height: 0.4),
                       alertViewFrame: nil,
                       titleFont:  UIFont.systemFont(ofSize: 20),
                       descriptionInfoFont: UIFont.systemFont(ofSize: 15),
                       descriptionColor: .black,
                       descriptionLabelAlignment: .left,
                       buttonOkBackgroundColor: UIColor(red:0.32, green:0.75, blue:1.00, alpha:1.0),
                       buttonYesBackgroundColor: UIColor(red:0.32, green:0.75, blue:1.00, alpha:1.0),
                       buttonNoBackgroundColor: .lightGray,
                       buttonSendBackgroundColor: UIColor(red:0.32, green:0.75, blue:1.00, alpha:1.0),
                       buttonCloseBackgroundColor: .clear,
                       buttonCloseImage: close,
                       buttonTitleFont:  UIFont.systemFont(ofSize: 14),
                       buttonTitleColor: .white,
                       buttonCornerRadius: 4.0,
                       closeButtonCorberRadius: 20.0,
                       closeButtonSize: CGSize(width: 40, height: 40),
                       closeButtonBorderWidth: 0.0,
                       closeButtonBorderColor: .lightGray,
                       progressTheme: animatedProgressThemeForAlert,
                       typedImages: [.error: #imageLiteral(resourceName: "error"),.warning: #imageLiteral(resourceName: "warning"),.info:#imageLiteral(resourceName: "InfoType"), .progress:#imageLiteral(resourceName: "progress"), .alert:#imageLiteral(resourceName: "InfoType"), .question:#imageLiteral(resourceName: "question")],
                       responseTheme: self.iAlertResponseTheme,
                       softTheme: self.iAlertSoftTheme, missingFeatureInternet: iAlertMissingInternet, missingFeaturePlan: iAlertMissingPlan, emptyMessagesList: iAlertEmptyMessagesList)
  }
  override public var iAlertEmptyMessagesList : IAlertMissingFeatureTheme {
    return IAlertMissingFeatureTheme(missingFeatureImage: #imageLiteral(resourceName: "error"), titleFont:  UIFont(name: mainRegularFontName, size: 24.0)!, titleColor: UIColor.white,  descFont: UIFont(name: mainRegularFontName, size: 20.0)!, descColor: defLblColor, button: IndoorButtonTheme(btnColor: UIColor.gray, btnTitleColor: .white, btnTitleFont: UIFont(name: mainBoldFontName, size: 18.0)!, btnImg: #imageLiteral(resourceName: "Location").withRenderingMode(.alwaysTemplate), btnRadius: 5.0))
  }
  override public var iAlertSoftTheme: IAlertSoftTheme {
    return IAlertSoftTheme(backgroudColor: UIColor(red:0.98, green:0.98, blue:0.96, alpha:1.0),
                           infoFont: .systemFont(ofSize: 15, weight: .regular),
                           infoTextColor: UIColor(red:0.45, green:0.45, blue:0.47, alpha:1.0),
                           buttonCornerRadius: 5,
                           cancelBtnBackgroundColor: UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0),
                           hideBtnBackgroundColor: UIColor(red:0.32, green:0.75, blue:1.00, alpha:1.0),
                           cancelBtnTitleColor: .white,
                           hideBtnTitleColor: .white,
                           cancelBtnTitleFont: .systemFont(ofSize: 14, weight: .semibold),
                           hideBtnTitleFont: .systemFont(ofSize: 14, weight: .semibold), minimalButtonWidthMultiplier: 0.3,
                           maxHorizontalButtonsCount: 2)
  }
  
  override public var iAlertMissingInternet : IAlertMissingFeatureTheme {
    return IAlertMissingFeatureTheme(missingFeatureImage: #imageLiteral(resourceName: "go"), titleFont:  .systemFont(ofSize: 24, weight: .semibold), titleColor: UIColor.black, descFont: .systemFont(ofSize: 20, weight: .regular), descColor: UIColor.blue, button: IndoorButtonTheme(btnColor: UIColor(red:0.32, green:0.75, blue:1.00, alpha:1.0), btnTitleColor: .white, btnTitleFont: .systemFont(ofSize: 18, weight: .semibold), btnImg: #imageLiteral(resourceName: "go"), btnRadius: 5.0))
  }
  
  override public var iAlertMissingPlan : IAlertMissingFeatureTheme {
    return IAlertMissingFeatureTheme(missingFeatureImage: #imageLiteral(resourceName: "go"), titleFont:  .systemFont(ofSize: 24, weight: .semibold)!, titleColor: UIColor.black, descFont:  .systemFont(ofSize: 20, weight: .regular), descColor: UIColor.blue, button: IndoorButtonTheme(btnColor: UIColor(red:0.32, green:0.75, blue:1.00, alpha:1.0), btnTitleColor: .white, btnTitleFont: .systemFont(ofSize: 18, weight: .semibold), btnImg:  #imageLiteral(resourceName: "go"), btnRadius: 5.0))
  }



}
