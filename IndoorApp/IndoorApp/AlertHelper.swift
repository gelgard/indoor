//
//  AlertHelper.swift
//  IndoorApp
//
//  Created by Oleg on 13.05.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import UIKit
import IndoorModel
import IndoorBlockingView

class AlertHelper: NSObject {
  
  var blockingViewBuilder : BlockingViewBuilder?
  var isShown : Bool = false
  
  func showAlertView(withType : IndTypes.DialogType, typeText: String , message : String , theme : IThemeManager , frame : CGRect , callback : @escaping ((Bool) -> Void)) -> ((Double) -> Void)? {
    
    
    blockingViewBuilder = BlockingViewBuilder()
    let blockingView = blockingViewBuilder?.getNotificationView(withType: withType, typeLabel: typeText, message: message, theme: theme, frame: frame)
    
    UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.33, options: [.curveEaseOut,.transitionCrossDissolve], animations: {
      UIApplication.shared.keyWindow?.addSubview((blockingView?.blockingView)!)
      self.isShown = true
    }, completion: nil)
    
    
    
    blockingViewBuilder?.resultCallBack = callback
    
    
    return blockingView?.progress
    
  }
  
  func updateAlertView(withType : IndTypes.DialogType, typeText: String , message : String ) {
    
    if blockingViewBuilder != nil {
      
      blockingViewBuilder!.updateNotificationView(withType: withType, typeLabel: typeText, message: message)
      
    }
    
  }
  
  
}



