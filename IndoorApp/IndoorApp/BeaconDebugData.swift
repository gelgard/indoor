//
//  BeaconDebugData.swift
//  IndoorApp
//
//  Created by Oleg on 16.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import Indoor_Entities
import IndoorPlan


class BeaconDebugData: IBeaconDebugData {
  
  //MARK: -Properties
  
  fileprivate var beaconsDataStack : [[BeaconsData]]
  
  //MARK: -init
  
  init(json : String) throws {
    
    let jsonHelper = JsonHelper()
    beaconsDataStack = []
    
    do {
      
      let jsonResultArr = try jsonHelper.jsonAsArray(json)
      let data = try jsonHelper.getFieldValue(IndJSONFields.jf_debug_data, dict: jsonResultArr)  as! NSDictionary
      let pointsDict : [NSDictionary] = try jsonHelper.getFieldValue(IndJSONFields.jf_debug_points, dict: data) as! [NSDictionary]
      
      for beaconsVals in pointsDict {
        let locationVals = try jsonHelper.getFieldValue(IndJSONFields.jf_debug_beaconData, dict: beaconsVals) as! NSArray
        var beaconData : [BeaconsData] = []
        
        for beaconOne in locationVals {
          if let bcnOne = beaconOne as? AnyObject {
            var one : Int = 0
            var two : Double = 0.0
            
            if let _ = bcnOne[bcnOne.firstIndex] {
              one = bcnOne[bcnOne.firstIndex] as! Int
            }
            
            
            if let _ = bcnOne[bcnOne.lastIndex]{
              two = bcnOne[bcnOne.lastIndex] as! Double
            }
              beaconData.append((one, two)) //TODO TEST attention
          }
          
        }
        
        beaconsDataStack.append(beaconData)
        
      }
    } catch let error as IErrorTypeWithNSError
    {
      NotificationCenter.default.post(name: Notification.Name(rawValue: errorNotification), object: error.getAsNSError())
    } catch let error as NSError
    {
      NotificationCenter.default.post(name: Notification.Name(rawValue: errorNotification), object: error)
    } catch
    {
      NotificationCenter.default.post(name: Notification.Name(rawValue: errorNotification), object: IndError.unknownError)
    }
  }
  
  //MARK: -BeaconDebugData implementation
  func getCurrentBeaconData() ->  [BeaconsData]? {
    
    if beaconsDataStack.count > 0 {
      var tempObject : [[BeaconsData]] = []
      tempObject.append(contentsOf: beaconsDataStack)
      beaconsDataStack.remove(at: 0)
      return tempObject[0]
    }
    
    return nil
    
  }
}
