//
//  DebugPanelDelegate.swift
//  IndoorApp
//
//  Created by Oleg on 17.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import IndoorEngine
import IndoorModel

protocol DebugPanelDelegate
{
  func startLocateFromDebug(_ beaconData : BeaconDebugData)
}
