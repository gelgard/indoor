//
//  IndConstants.swift
//  IndoorApp
//
//  Created by Oleg on 26.11.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation

public struct IndConstants {
  //MARK: JSONFields
  static let jf_plan_plan : KeyType = ("plan", JsonTypes.stringType)
  static let jf_plan_type : KeyType = ("type", JsonTypes.intType)
  static let jf_plan_unit : KeyType = ("unit", JsonTypes.stringType)
  static let jf_plan_width : KeyType = ("plan_width", JsonTypes.intType)
  static let jf_plan_base_angle : KeyType = ("base_angle", JsonTypes.intType)
  static let jf_plan_beacon_major : KeyType = ("beacon_major", JsonTypes.intType)
  static let jf_plan_height : KeyType = ("plan_length", JsonTypes.intType)
  static let jf_plan_layers : KeyType = ("layers", JsonTypes.dictType)
  
  static let plan_block : JsonKeyParams = ("", [jf_plan_plan,jf_plan_type,jf_plan_unit,jf_plan_width,jf_plan_height,jf_plan_layers])
  
  
  static let jf_layers_layerId : KeyType = ("layer_id", JsonTypes.intType)
  static let jf_layers_type : KeyType = ("type", JsonTypes.intType)
  static let jf_layers_index : KeyType = ("index",JsonTypes.intType)
  static let jf_layers_format : KeyType = ("format", JsonTypes.intType)
  static let jf_layers_unit : KeyType = ("unit",JsonTypes.stringType)
  static let jf_layers_objects : KeyType = ("objects",JsonTypes.dictType)
  static let jf_layers_step : KeyType = ("grid_step",JsonTypes.intType)
  static let jf_layers_filling : KeyType = ("grid_filling",JsonTypes.arrType)
  
  static let layers_block : JsonKeyParams = ("layers", [jf_layers_layerId,jf_layers_type,jf_layers_index,jf_layers_format,jf_layers_unit,jf_layers_objects])
  
  
  static let jf_object_id : KeyType = ("id", JsonTypes.intType)
  static let jf_parent_id : KeyType = ("parent_id", JsonTypes.intType)
  static let jf_object_type : KeyType = ("type",JsonTypes.intType)
  static let jf_object_params : KeyType = ("params",JsonTypes.dictType)
  static let jf_object_coord : KeyType = ("coordinates",JsonTypes.arrType)
  
  static let jf_debug_points : KeyType = ("points",JsonTypes.arrType)
  static let jf_debug_data : KeyType = ("data",JsonTypes.dictType)
  static let jf_debug_beaconData : KeyType = ("beaconData",JsonTypes.arrType)
  
  
  static let object_block : JsonKeyParams = ("objects", [jf_object_id,jf_object_type,jf_object_params,jf_object_coord])
  
  //MARK: Notifications constants
  
  static let errorNotification = "ErrorNotification"
  static let errorNotificationSelector: Selector = Selector(("errorReceived:"))
  static let updateLogNotification = "UpdateLog"
  static let updateLogNotificationSelector: Selector = Selector(("updateLog:"))
  
  static let enterBeaconRegion : String = "EnterBeaconRegion"
  static let enterBeaconRegionSelector: Selector = Selector(("beaconRegionInside:"))
  static let leaveBeaconRegion : String = "LeaveBeaconRegion"
  static let leaveBeaconRegionSelector: Selector = Selector(("beaconRegionOutside:"))
  static let nearByBeacon : String = "NearByBeacon"
  static let nearByBeaconSelector: Selector = Selector(("nearByBeaconFnc:"))
  static let unselectBeacon : String = "UnselectBeacon"
  static let unselectBeaconSelector: Selector = Selector(("unselectBeaconFnc:"))
  
  //let hardcodeJSON = "testJson"
  //let hardcodeJSON = "museum_demo"
  
  
  static let hardcodeJSON = "gelgard_flat_full"
  //let hardcodeJSON = "museum_json"
  //let hardcodeJSON = "office_json"
  
  static let debugJSON = "debug_34"
  
}
