//
//  DebugLog.swift
//  IndoorApp
//
//  Created by Oleg on 17.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit

struct DebugLog: iDebugLog {
  
  //MARK: -Properties
  
  let prevPoint: CGPoint
  var curPoint: CGPoint
  var calculationLog : [String]
  var startTime : Double
  var endTime : Double
  var pointId : Int
  
}
