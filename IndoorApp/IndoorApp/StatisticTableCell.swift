//
//  StatisticTableCell.swift
//  IndoorExpoTestApp
//
//  Created by Kostiantyn Potravnyi on 6/5/18.
//  Copyright © 2018 Uran. All rights reserved.
//

import UIKit
protocol StatisticTableCellDelegate: class {
  func statisticCellSendButtonTapped(_ cell: StatisticTableCell)
  func statisticCellRestoreButtonTapped(_ cell: StatisticTableCell)
  func statisticCellDeleteButtonTapped(_ cell: StatisticTableCell)
  func statisticCell(_ cell: StatisticTableCell, sliderValueUpdated value: Float)
}

class StatisticTableCell: UITableViewCell {
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var sizeLabel: UILabel!
  @IBOutlet weak var zippedStatisticsCount: UILabel!
  
  @IBOutlet weak var sendButton: UIButton!
  @IBOutlet weak var sizeSlider: UISlider!
  
  @IBOutlet weak var zippedDataStack: UIStackView!
  
  weak var delegate: StatisticTableCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.nameLabel.text = nil
    self.sizeLabel.text = nil
    self.sizeSlider.setValue(0, animated: false)
  }
  
  @IBAction func sliderMoved(_ sender: UISlider) {
    self.delegate?.statisticCell(self, sliderValueUpdated: sender.value)
  }
  
  @IBAction func sendTapped() {
    self.delegate?.statisticCellSendButtonTapped(self)
  }
  @IBAction func restoreTapped() {
    self.delegate?.statisticCellRestoreButtonTapped(self)
  }
  @IBAction func deleteTapped() {
    self.delegate?.statisticCellDeleteButtonTapped(self)
  }
  
}
