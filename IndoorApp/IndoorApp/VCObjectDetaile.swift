//
//  VCObjectDetaile.swift
//  IndoorApp
//
//  Created by Oleg on 21.03.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import IndoorEngine
import IndoorModel

class VCObjectDetaile:  VCBase
{
  // MARK: - Outlets
  
  @IBOutlet weak var imagePic: UIImageView!
  @IBOutlet weak var titleLbl: UILabel!
  @IBOutlet weak var desc: UITextView!
  @IBOutlet weak var playBtn: UIButton!
  
  // MARK: - Properties
  
  var currentObject : IObject?
  var isPlaying : Bool = false
  var audioPlayer : AVAudioPlayer?
  
  // MARK: - Init & override

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    if let _ = self.currentObject {
        loadData(self.currentObject!)
    }
  }

  func loadData(_ object : IObject) {
      //HARDCODE
    var filenName : String = "son"
    
    if object.object_id == 2005 {
      self.imagePic.image = UIImage(named: "sonn")
      filenName = "son"
    }
    
    if object.object_id == 2006 {
      self.imagePic.image = UIImage(named: "PIC_2006".localized)
      filenName = "galatey"
    }
    
    self.titleLbl.text = "TITLE_\(object.object_id)".localized
    self.desc.text = "DESC_\(object.object_id)".localized
    
    if isPlaying == false {
      playBtn.setImage(UIImage(named: "play"), for: UIControlState())
    } else {
      playBtn.setImage(UIImage(named: "pause"), for: UIControlState())
    }
    
    let coinSound = URL(fileURLWithPath: Bundle.main.path(forResource: filenName, ofType: "mp3")!)
    do {
      self.audioPlayer = try AVAudioPlayer(contentsOf:coinSound)
      audioPlayer!.prepareToPlay()
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }

    self.view.layoutIfNeeded()
  }

  //MARK: - Actions
  @IBAction func playPause(_ sender : AnyObject) {
    if isPlaying == false {
      playBtn.setImage(UIImage(named: "pause"), for: UIControlState())
      audioPlayer!.play()
      isPlaying = true
    } else {
      playBtn.setImage(UIImage(named: "play"), for: UIControlState())
      audioPlayer!.pause()
      isPlaying = false
    }
    self.view.layoutIfNeeded()
    
  }
}
