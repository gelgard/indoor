//
//  DataStorageWrapper.swift
//  museumGuideNav
//
//  Created by Oleg on 22.04.17.
//  Copyright © 2017 UranCompany. All rights reserved.
//

import UIKit
import IndoorModel
import IndoorAPIManager
import IndoorParser
import IndoorDBManager
import IndoorContentManager



class DataStorageWrapper {
  
  fileprivate var apiManager = IndoorAPIManager()
  fileprivate var dbManager:FMDBManager?
  
  typealias DataResultAlias = (error: NSError? , dictionary : NSDictionary?)
  
  var currentTask : ((IndTypes.TaskList,IndTypes.TaskProcess,String,String,IndTypes.DialogType) -> Void )?
  var currentProgress : ((Double, IndTypes.TaskList)->Void)?
  var completeTask : ((IErrorTypeWithNSError)->Void)?
  
  fileprivate var locale:String
  fileprivate var folderPath:String
  fileprivate var planId:Int?
  fileprivate var json:URL?
  fileprivate var sourceType : ParseType
  
  
  
  public var apiForGetData : APIMethod  {
    return .ActionFullplan
  }
  
  public var assets : [String]  {
    do {
      
      if self.dbManager == nil {
        self.dbManager = try FMDBManager(locale: self.locale, folderPath: self.folderPath)
      }

        let assets = try self.dbManager?.getsAssetsUrl()
        return assets!
      
      
    } catch {
      return []
    }
  }
  
  public var version:Int {
    get {
      //HARDCODE
      return 1
    }
  }
  
  public init(planId:Int,locale: String, folderPath: String) throws {

    self.planId = planId
    self.locale = locale
    self.folderPath = folderPath
    self.sourceType = .dictionary
    
  }
  
  public init(json:URL,locale: String, folderPath: String) throws {
    
    self.locale = locale
    self.folderPath = folderPath
    self.sourceType = .file
    self.json = json

    
    
  }

  internal func parseData(data: [String : Any],completition : @escaping ((NSError?) -> Void)) {
    
    if (self.currentProgress != nil) {
      self.currentProgress!(0,.parser)
    }
    
    let parser = IndoorParser(withDictionary: data, folderPath: self.folderPath, locale: self.locale, delegate: nil)
    do {
      
      let progressValue : ((Double) -> Void) = { (value : Double)
        in
        
        print("PROGRESS PARSE \(value)")
        
        DispatchQueue.main.async {
          // Update the UI
          self.currentProgress!(value,.parser)
        }
        
        
        
        
        if value > 1.0 {
          completition(IndoorParserErrors.wrongProgressValue.getAsNSError())
        }
        
        if value == 1.0 {
          completition(nil)
        }
        
      }
      
      let delay = DispatchTime.now() + .seconds(1)
      
      //DispatchQueue.main.asyncAfter(deadline: delay) {
      DispatchQueue.global(qos: .userInitiated).async {
        // Dodge this!
        do {
          try parser.parse(progress: progressValue)
        } catch {
          completition(ErrorHelper.errorProcessing(err: error as AnyObject))
        }
        

      }
      
      /*DispatchQueue.global(qos: .userInitiated).async {
        try parser.parse(progress: progressValue)
      }*/
      
      //try parser.parse
    } catch let error {
      completition(ErrorHelper.errorProcessing(err: error as AnyObject))
    }
  }
  
 
  internal func getDataFromSourceApi(completition : @escaping ((NSError?, NSDictionary?,Bool) -> Void)) {
  
    do {
      try apiManager.getAPIData(requestType: self.apiForGetData, params: ["id":self.planId,"lang":self.locale], completionHandler: {
        (result, data) in
        
        if !result || !(data?.succes)! {
          completition(IndoorAPIErrors.wrongServerAnswer.getAsNSError(), nil, false)
        } else {
          completition(nil,data?.data, true)
        }
        
      })
    } catch let error {
      completition(ErrorHelper.errorProcessing(err: error as AnyObject),nil, false)
    }
    
  }
  
  
  internal func getDataFromSourceFile(completition : @escaping ((NSError?, NSDictionary?,Bool) -> Void)) {
    
    do {
      
      let stringFromFile = try String(contentsOf: self.json!, encoding: String.Encoding.utf8)
      if let data = stringFromFile.data(using: .utf8) {
        do {
          let dataRes : NSDictionary  = try  JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
          completition(nil, dataRes, true)
          
        } catch {
          
          completition(ErrorHelper.errorProcessing(err: error as AnyObject),nil,false)
          
        }
      }
      
      
    } catch let error {
      completition(ErrorHelper.errorProcessing(err: IndoorParserErrors.errorReadingJSONfile as AnyObject),nil, false)
    }
    
  }
  
  internal func dataAdapter(error: NSError? , dictionary : NSDictionary?) {
    
  }


  
  internal func getDataFromSource(completition : @escaping ((NSError?, NSDictionary?,Bool) -> Void)) {
    
//    if (self.currentProgress != nil) {
//      self.currentProgress!(0, .api)
//    }
    
    
    
    let resultData : ((NSError?, NSDictionary?,Bool) -> Void) = { (err , dict, isNewData) in
      
      if dict == nil {
        completition(IndoorParserErrors.dataForParsingNil.getAsNSError(),nil,false)
        return
      }
      
      let version = dict![IndJSONFields.jf_plan_version.key] as? Int
      
      do {
        
        self.dbManager = try FMDBManager.init(locale: self.locale, folderPath: self.folderPath, version: version)
        
        if version == self.dbManager?.version {
          completition(err, dict,false)
        } else {
          completition(err, dict,true)
        }
  
      } catch let error  {
        completition(ErrorHelper.errorProcessing(err: error as AnyObject),nil,false)
      }
      
      
      
      
    
    }
    
    switch self.sourceType {
      case .dictionary:
        getDataFromSourceApi(completition: resultData)
      case .file:
        getDataFromSourceFile(completition: resultData)
      default:
        completition(nil,nil, false)
        return
    }
    
    
    
  }
  
  internal func downloadContent(completition : @escaping ((NSError?) -> Void)) {
    
    if (self.currentProgress != nil) {
      self.currentProgress!(0, .api)
    }
    
    do {
      
      let progressParts : Double = Double(1.0 / (Double(self.assets.count) * 2.0))
      var iterationNum : Double = 1
      var summaryProgress : Double = 0.0
      var currentPartProgress : Double = 0.0
      
      let progressValue : ((Double,IndTypes.TaskList) -> Void) = { (value : Double , type : IndTypes.TaskList )
        in

        currentPartProgress = summaryProgress + (value * progressParts )
        
        if value == 1 {
          
          currentPartProgress = iterationNum * progressParts
          summaryProgress = summaryProgress + progressParts
          iterationNum += 1
          
        }
        
        if self.currentProgress != nil {
          self.currentProgress!( currentPartProgress  ,type)
          ///print("summaryProgress \(summaryProgress)  VALUET \(value) currentPartProgress \(currentPartProgress) TYPE")
        }
        
        
      }
      
      if (self.currentProgress != nil) {
        self.currentProgress!(0,.allcontent)
      }
      
      if self.assets.count == 0 {
        completition(nil)
      }
      
      
      for asset in self.assets {
        
        _ = ContentDLManager.init(fileURL: asset, zipPassword: "", autoStart: true, delegate: nil, progress: progressValue, errorHandler: {
          error in
          
          if error != nil {
            completition(ErrorHelper.errorProcessing(err: error as AnyObject))
          } else {
            completition(nil)
            //HARDCODE тут рассчитано все на то что в файле один ASSET, надо заточить , что ответ идет после распаковки последнего
          }
          
        })
        
        
      }
      
      
    } catch let error {
      completition(ErrorHelper.errorProcessing(err: error as AnyObject))
      
    }

    
    
    
  }
  
  public func executeUpdateTEST(completion: @escaping ((NSError?) -> Void)) {
    
    let progressType : IndTypes.DialogType = .progress
    self.currentTask!(IndTypes.TaskList.api, IndTypes.TaskProcess.failed,progressType.labelText(),"API_TITLE".localized, progressType)
   
    
  }
  
  
  public func executeUpdate(completion: @escaping ((NSError?) -> Void)) {
    
    // API
    
    let progressType : IndTypes.DialogType = .progress
    print("#PROGRESS TITLE 1")
    self.currentTask!(IndTypes.TaskList.api, IndTypes.TaskProcess.failed,progressType.labelText(),"API_TITLE".localized, progressType)
    
    
    
    self.getDataFromSource { (error: NSError?, data: NSDictionary?, isNewData : Bool) in
      
      print("#PROGRESS RETURN 1 ")
      if !isNewData {
        self.currentTask!(IndTypes.TaskList.api, IndTypes.TaskProcess.complete, IndTypes.DialogType.error.labelText(), "COMPLETE".localized  , IndTypes.DialogType.info)
        completion(nil)
        return
      }
      
      if error != nil {
        
        self.currentTask!(IndTypes.TaskList.api, IndTypes.TaskProcess.failed, IndTypes.DialogType.error.labelText(), IndoorAPIErrors.wrongServerAnswer.getAsNSError().localizedDescription  , IndTypes.DialogType.error)
        completion(error)
        
      } else {
        
        //PARSING
        
        //DispatchQueue.main.async {
          print("#PROGRESS TITLE 2")
        
        
        if (Thread.isMainThread == true) {
          self.currentTask!(IndTypes.TaskList.parser, IndTypes.TaskProcess.start,progressType.labelText(),"PARSER_TITLE".localized, progressType)
        } else {
          DispatchQueue.main.sync() {
            self.currentTask!(IndTypes.TaskList.parser, IndTypes.TaskProcess.start,progressType.labelText(),"PARSER_TITLE".localized, progressType)
          }
        }
        
          
        //}
        
        
        
        self.parseData(data: data as! [String : Any], completition: { (error : NSError?) in
          
          print("#PROGRESS RETURN 2 ")
          
          if error != nil {
            
            self.currentTask!(IndTypes.TaskList.api, IndTypes.TaskProcess.failed, IndTypes.DialogType.error.labelText(), IndoorParserErrors.parsingSummuryError.getAsNSError().localizedDescription  , IndTypes.DialogType.error)
            completion(error)
            
          }
          
          print("#PROGRESS TITLE 3")
          self.currentTask!(IndTypes.TaskList.allcontent, IndTypes.TaskProcess.start,progressType.labelText(),"CONTENT_TITLE".localized, progressType)
          
          //CONTENT
          
          self.downloadContent(completition: { (error : NSError?) in
            
            print("#PROGRESS RETURN 3 ")
            
            if error != nil {
              
              self.currentTask!(IndTypes.TaskList.allcontent, IndTypes.TaskProcess.failed, IndTypes.DialogType.error.labelText(), IndoorParserErrors.parsingSummuryError.getAsNSError().localizedDescription  , IndTypes.DialogType.error)
              completion(error)
              
            } else {
              self.currentTask!(IndTypes.TaskList.api, IndTypes.TaskProcess.complete, IndTypes.DialogType.error.labelText(), "COMPLETE".localized  , IndTypes.DialogType.info)
              completion(nil)
              
            }
          })   
          
        })
    
      }
     
    }
    
  }
  
		

}
