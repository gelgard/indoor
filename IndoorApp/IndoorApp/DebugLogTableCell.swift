//
//  DebugLogTableCell.swift
//  IndoorExpoTestApp
//
//  Created by Kostiantyn Potravnyi on 6/8/18.
//  Copyright © 2018 Uran. All rights reserved.
//

import UIKit
import IndoorModel

protocol DebugLogTableCellDelegate: class {
    func tappedDeleteActionIn(_ cell: DebugLogTableCell)
}

class DebugLogTableCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    
    weak var delegate: DebugLogTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.mainLabel.text = nil
        self.subLabel.text = nil
        self.delegate = nil
    }
    @IBAction func buttonAction(_ sender: Any) {
        self.delegate?.tappedDeleteActionIn(self)
    }
    

}


