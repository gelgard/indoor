//
//  VCBase.swift
//  IndoorApp
//
//  Created by Oleg on 07.01.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import CoreBluetooth
import IndoorEngine
import IndoorModel
import Crashlytics

class VCBase: UIViewController {

  // MARK: - Properties
  

  let myBTManager = CBPeripheralManager()
  var lastStage = CLProximity.unknown
  var settings = Settings()
  var alertHelper : AlertHelper = AlertHelper()
  var museumTheme : IThemeManager = MuseumThemeManager()
  var navController : DLHamburguerNavigationController?

  
  // MARK: - Init & override
  
  required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
  }

  deinit {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.errorNotification), object: nil)
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    NotificationCenter.default.addObserver(self, selector: #selector(VCBase.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(VCBase.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(VCBase.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    NotificationCenter.default.addObserver(self, selector: NotificationConstant.errorNotificationSelector, name:NSNotification.Name(rawValue: NotificationConstant.errorNotification), object: nil)
    NotificationCenter.default.addObserver(self, selector: NotificationConstant.enterBeaconRegionSelector, name:NSNotification.Name(rawValue: NotificationConstant.enterBeaconRegion), object: nil)
    NotificationCenter.default.addObserver(self, selector: NotificationConstant.leaveBeaconRegionSelector, name:NSNotification.Name(rawValue: NotificationConstant.leaveBeaconRegion), object: nil)
    
    self.localize()
    
    print("BASE SHOWN")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.updateControlsFromSettings()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.errorNotification), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.enterBeaconRegion), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.leaveBeaconRegion), object: nil)
  }
  


  //MARK: - Notifications selectors

  @objc func errorReceived(_ notification: Notification) {
    let errInfo : String = "ERROR: \((notification.object as! NSError).localizedDescription)  In \(String(describing: (notification.object as! NSError).userInfo[IndError.userInfoPlace]))"
     Crashlytics.sharedInstance().recordError((notification.object as! NSError), withAdditionalUserInfo: (notification.object as! NSError).userInfo)
    print(errInfo)
    
    _ = self.alertHelper.showAlertView(withType: .error, typeText: IndTypes.DialogType.error.labelText(), message: (notification.object as! NSError).localizedDescription, theme: self.museumTheme, frame: self.view.bounds, callback: { _
    in
    }
    )
    //self.alertHelper.showAlertView(withType: .error, typeText: IndTypes.DialogType.error.labelText(), message: "PLAN_ABSENT".localized, theme: self.museumTheme, frame: self.view.bounds, callback: { result
    // AlertHelper.showAlert((notification.object as! NSError).localizedDescription, subtitle: "ERROR", type: IndTypes.DialogType.info)
  }

  @objc func keyboardWillShow(_ notification: Notification) {
      
  }

  @objc func keyboardWillHide(_ notification: Notification) {
      
  }

  func beaconRegionInside(_ notification: Notification) {
   // AlertHelper.showAlert("Region Inside", subtitle: "Region Inside", type: IndTypes.DialogType.info)
  }

  func beaconRegionOutside(_ notification: Notification) {
    //AlertHelper.showAlert("Region Outside", subtitle: "Region Outside", type: IndTypes.DialogType.info)
  }
  
  @objc func rotated() {
    
  }
  
  func localize() {
    
  }
  
  func updateControlsFromSettings() {
  }

  
}
