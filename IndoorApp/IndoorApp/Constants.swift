//
//  Constants.swift
//  IndoorApp
//
//  Created by Oleg on 14.12.15.
//  Copyright © 2015 UranCompany. All rights reserved.
//

import Foundation
import SpriteKit
import CoreLocation
import IndoorEngine

//MARK: Singltones



//MARK: Enum settings

enum SettingRotate : Int {
  case map = 0
  case onlyMarker = 1
}

enum SettingSelectObject : Int {
  case withTouch = 0
  case inLook = 1
}

enum SettingKeys : String {
  case rotate = "rotate"
  case showDebugPanel = "showDebugPanel"
  case selectObject = "selectObject"
}


//MARK: JSONFields


//MARK: Notifications constants







//MARK: - Constants
let currentPlan = 3
let textCellIdentifier = "DebugCell"
let kCollectionSize = CGSize(width: 300, height: 180)

let segues = ["motion","ibeacon","calibrate","settings","debugLog","staistic"]
let seguesСaptions = ["Sensors data","iBeacon","Calibrate","Settings","DebugLog","Statistic"]
let minCoordinates = 4



let infoPanelHeightValue : CGFloat = 200.0


let kDebugWidth = 300

//Locationmarker

//MARK - Debug Constant


//MARK - Navifation constants



//MARK - Debug JSON constants









let jsonArr = ["zp_office","office_exhibit","gelgard_flat_full","angolenko","nature_probe_1b","nature_probe_2b","nature_probe_3b","nature_probe_4b","museum_exhibit","sich"]
let jsonArrLbl = ["Офис на Правде","Офис Сливен","Квартира","Анголенко","NATURE_PROBE_1B","NATURE_PROBE_2B","NATURE_PROBE_3B","NATURE_PROBE_4B","Музей Сливен","Сич"]



//let hardcodeJSON = "testJson"
//let hardcodeJSON = "museum_demo"


//let hardcodeJSON = "zp_office"
let hardcodeJSON = "gelgard_flat_full"
//let hardcodeJSON = "office_exhibit"


//let hardcodeJSON = "museum_json"


let debugJSON = "debug_34"
