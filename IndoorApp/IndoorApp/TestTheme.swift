//
//  TestTheme.swift
//  IndoorTestApplication
//
//  Created by admin on 9/18/17.
//  Copyright © 2017 UranCompany. All rights reserved.
//

import UIKit
import IndoorModel

class TestTheme: ThemeManager {
  
  let mainRegularFontName = "HelveticaNeueCyr-Roman"
  let mainBoldFontName = "HelveticaNeueCyr-Bold"
  let titleColor :  UIColor = #colorLiteral(red: 0.3098039216, green: 0.3137254902, blue: 0.6666666667, alpha: 1)
  let descColor :  UIColor = .darkGray
  let btnBackground : UIColor = #colorLiteral(red: 0.3176470588, green: 0.7529411765, blue: 1, alpha: 1)
    override var indoorTableSearchHeader: IndoorSearchViewThemeManager {
        return IndoorSearchViewThemeManager(height: nil,
                                            placeholder: "Search",
                                            promt: "",
                                            searchStyle: .default,
                                            barStyle: .default,
                                            transluent: true,
                                            barTintColor: UIColor(red:0.34, green:0.76, blue:0.98, alpha:1.0),
                                            tintColor: nil,
                                            background: .clear,
                                            cancelEnabled: false)
    }
    override public var indoorTableCategoriesHeader: IndoorCategoriesViewThemeManager {
        return IndoorCategoriesViewThemeManager(backgroundColor: UIColor(red:0.34, green:0.76, blue:0.98, alpha:1.0),
                                                height: 55,
                                                contentInsets: .init(top: 0, left: 0, bottom: 0, right: 0),
                                                showScrollIndicator: false,
                                                itemSpasing: 20,
                                                borderColor: .clear,
                                                borderWidth: 0,
                                                borderRadius: 0,
                                                cellBackground: .clear,
                                                cellSelectedColor: .white,
                                                cellUnselectedColor: .clear,
                                                cellLableFont: .systemFont(ofSize: 17),
                                                cellLabelSelectedTextColor: .white,
                                                cellLabelUnselectedTextColor: .white,
                                                cellUnderLineHeight: 3,
                                                cellBorderColor: .clear,
                                                cellBorderWidth: 0,
                                                cellBorderRadius: 0,
                                                emptyPlaceholder: "Categories list is empty.",
                                                emptyTextColor: nil,
                                                emptyTextFont: nil)
    }
    

  override var indoorTableFitersHeaderTheme: IndoorFilteringHeaderViewTheme {
    let cellTheme = IndoorFilteringHeaderCVCellTheme(titleFont: .systemFont(ofSize: 16),
                                                     titleColor: UIColor(red:0.31, green:0.33, blue:0.66, alpha:1.0),
                                                     borderColor: UIColor(red:0.31, green:0.33, blue:0.66, alpha:1.0),
                                                     borderWidth: 2,
                                                     borderCornerRadius: 1,
                                                     titleLeftInset: 3,
                                                     titleRightInset: 3,
                                                     titleBorderInset: 4,
                                                     borderBottomInset: nil)
    return IndoorFilteringHeaderViewTheme(height: 55,
                                          background: UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0),
                                          emptyLabelFont: .systemFont(ofSize: 15),
                                          emptyLabelColor: UIColor(red:0.31, green:0.33, blue:0.66, alpha:1.0),
                                          emptyLabelText: "Нет доступных фильтров",
                                          contentInsets: .init(top: 0, left: 15, bottom: 0, right: 15),
                                          itemsSpacing: 20,
                                          itemTheme: cellTheme)
  }
    override public var indoorGalleryCellColors: IndoorGalleryCellColors {
        return IndoorGalleryCellColors(background:      .clear,
                                       imageBackground: .lightGray,
                                       imageShadow:     UIColor.black.withAlphaComponent(0.9),
                                       buttonsTint:     .white,
                                       triangle:        .white,
                                       triangleShadow:  UIColor.black.withAlphaComponent(0.5),
                                       mainLabelText:   .blue,
                                       imageLabelText:  .white,
                                       cellLabelText:   .white,
                                       infoButton:      UIColor(red:0.31, green:0.33, blue:0.65, alpha:1.0),
                                       locationButton:  UIColor(red:0.25, green:0.27, blue:0.58, alpha:1.0),
                                       mapButton:       UIColor(red:0.20, green:0.22, blue:0.51, alpha:1.0),
                                       collectionBackground: .clear,
                                       collectionBorder: .clear,
                                       collectionCellBorder: .lightGray,
                                       collectionCellBackground: .clear,
                                       collectionCellBackgroundSelected: .clear, textBackground: #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1215686275, alpha: 1).withAlphaComponent(0.7))
    }
    
  override public var indoorNavigationContainerNavBarTheme : NavBarTheme {
    class IndoorBarItem: BarItem {
      var icon: UIImage
      var title: String
      var tintColor: UIColor
      var selectedContentTintColor: UIColor?
      var selectedIcon: UIImage?
      
      init(icon: UIImage, title: String, tintColor: UIColor) {
        self.icon = icon
        self.title = title
        self.tintColor = tintColor
        self.selectedContentTintColor = tintColor
        self.selectedIcon = icon
      }
    }
    
    let bundle = Bundle.main
    let back = UIImage.init(named: "circleBackward", in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysOriginal) ?? UIImage()
    return NavBarTheme(titleColor: .black,
                       titleFont:  UIFont(name: mainRegularFontName, size: 16.0)!,
                       backgroundColor: nil,
                       backItem: IndoorBarItem(icon: #imageLiteral(resourceName: "Back"), title: "", tintColor: .clear),
                       menuItem: IndoorBarItem(icon: #imageLiteral(resourceName: "burger"), title: "", tintColor: .clear),
                       closeItem: nil,
                       showBackItem: true,
                       showMenuItem: true,
                       showCloseItem: false,
                       hideMenuItemOnPushedController: true)
  }
}
