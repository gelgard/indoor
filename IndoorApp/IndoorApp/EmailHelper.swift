//
//  EmailHelper.swift
//  IndoorApp
//
//  Created by Oleg on 09.08.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import MessageUI

class EmailHelper : NSObject {
  
  //MARK: -Properties
  fileprivate var privateFilePath : URL
  fileprivate var privateFilePrefix : String
  fileprivate var privateFileExtension : String
  fileprivate var privateVC  : UIViewController
  
  //MARK: -Init
  
  init (zipFile : URL, vc : UIViewController,filePrefix: String,fileExtension : String) {
    privateFilePath = zipFile
    privateFilePrefix = filePrefix
    privateFileExtension = fileExtension
    privateVC = vc
  }
  
  
  
  func sendEmail(){
    if(MFMailComposeViewController.canSendMail()){
      
      let mailComposer = MFMailComposeViewController()
      mailComposer.mailComposeDelegate = self
      
      mailComposer.setSubject("\(privateFilePrefix)_\(Date().asStringForName())")
      mailComposer.setMessageBody("", isHTML: false)
      
      
      
      if let data = NSData(contentsOf: privateFilePath) {
        //Attach File
        mailComposer.addAttachmentData(data as Data, mimeType: "application/zip", fileName: "\(privateFilePrefix)_\(Date().asStringForName()).\(privateFileExtension)")
        privateVC.present(mailComposer, animated: true, completion: nil)
      }
    }
  }
  
  
}

extension EmailHelper : MFMailComposeViewControllerDelegate {
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }
}

