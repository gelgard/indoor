//
//  DebugLogsScreen.swift
//  IndoorExpoTestApp
//
//  Created by Kostiantyn Potravnyi on 6/7/18.
//  Copyright © 2018 Uran. All rights reserved.
//

import Foundation
import UIKit
import IndoorModel
import IndoorDbDebugInfo
import IndoorCDManager
import CoreLocation
import IndoorAlert

class DebugLogsScreen: VCBase {
    
//    static func initiateFromStoryboard() -> DebugLogsScreen {
//        return UIStoryboard.list.DebugLogs.initiateStoryboard().initiateController(ofType: DebugLogsScreen.self)
//    }
  
    lazy var debugLogger: iDebugInfo = {
        return IndoorDbDebugInfo(manager: IndoorCDManager())
    }()
    
    @IBOutlet weak var table: UITableView?
    
    var logsList: [IndoorDebugLog] = []
    
    var dateFormatter: DateFormatter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm:ss"
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.readLogs()
    }
    
    func createLogAction() {
        do {
            try self.debugLogger.startNewLog(planId: 3)
            
            for i in 0..<3 {
                let beaconsData = (0..<3).map { BeaconsData(minor: $0, distance : Double($0)) }
                let beacons = (0..<3).map { TestBeacon(i: $0) }
                
                
                _ = try self.debugLogger.addPointData(beaconsData, resultPoint: [1,2], beginTime: Date().timeIntervalSince1970, endTime: Date().timeIntervalSince1970, stepCount: 10*i, maxRelocation: Double(i*4), beacons: beacons)
                
                try self.debugLogger.addPathData(true, startTime: Date().timeIntervalSince1970, endTime: Date().timeIntervalSince1970, stopPoint: [3*1,5*i])
            }
            
            
        } catch {
            print(error)
        }
    }
    
    func readLogs() {
        
        self.debugLogger.getLogs(lightWeight: true) { result in
            do {
                let logs = try result.resolve()
                self.logsList = logs
            } catch {
                self.logsList = []
                print(error)
            }
            
            self.table?.reloadData()
        }
    }

    func sendLog(withId id: String, completion handler: @escaping EmptyClosure) {
        let alert = IndoorAlertController()
        alert.setTheme(theme: self.museumTheme)
        alert.showAlert(in: nil, with: .info, typeText: nil, message: "creating File..", callback: {_ in})
        alert.setCloseButton(visible: false)
        
        self.debugLogger.sendLog(withId: id) { (result) in
            do {
                let _ = try result.resolve()
                alert.showAlert(with: .info, typeText: "Success", message: "Done", callback: { _ in alert.closeAlert() })
            } catch {
                print(error)
                alert.showAlert(with: .error, typeText: nil, message: "Done", callback: { _ in alert.closeAlert() })
            }
            handler()
        }
    }
    
    func deleteLog(with id: String) {
        let alert = IndoorAlertController()
        alert.setTheme(theme: self.museumTheme)
        alert.showAlert(in: nil, with: .question, typeText: nil, message: "Are you sure you want to delete log?") { response in
            if response == .yes {
                alert.showAlert(with: .info, typeText: nil, message: "Deleting..", callback: { _ in })
                self.debugLogger.deleteLog(withId: id, completion: { (deleteResult) in
                    self.readLogs()
                    
                    var message = ""
                    var type: IndTypes.DialogType = .info
                    do {
                        try deleteResult.resolve()
                        message = "Deleting success!"
                    } catch {
                        type = .error
                        message = "Deleting failure, \n error: \(error.localizedDescription)"
                    }
                    alert.showAlert(with: type, typeText: nil, message: message, callback: { _ in
                        alert.closeAlert()
                    })
                })
            } else {
                alert.closeAlert()
            }
        }
    }
}

extension DebugLogsScreen: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.logsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(cellClass: DebugLogTableCell.self, for: indexPath)
        guard let log = logsList.value(at: indexPath.row) else { return cell }
        cell.delegate = self
        cell.mainLabel.text = "Plan id: \(log.planId)"
        cell.subLabel.text = self.dateFormatter.string(from: Date.init(timeIntervalSince1970: log.timeStamp))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let log = logsList.value(at: indexPath.row) else { return }
        let cell = tableView.cellForRow(at: indexPath)
        self.sendLog(withId: log.id) {
            cell?.setSelected(false, animated: true)
        }
    }
}

extension DebugLogsScreen: DebugLogTableCellDelegate{
    func tappedDeleteActionIn(_ cell: DebugLogTableCell) {
        guard let indexPath = self.table?.indexPath(for: cell),
            let log = self.logsList.value(at: indexPath.row) else { return }
        
        self.deleteLog(with: log.id)
    }
}


class TestBeacon: IBeacon {
  var proximity: Int?
  
  var acurancy: Double?
  
  var installationHeight: Double?
  
    var uuid: String
    
    var minor: Int
    
    var major: Int
    
    var rssi: Int?
    
    var rssiCalculated: Double?
    
    var distanceCalc: Double?
    
    var distanceOldAlg: Double?
    
    var multiplication: Double?
    
    var measurePower: Int?
    
    var txPowerDistance: Int?
    
    var signalDispersal: Double?
    
    var txPowerFactory: Int?
    
    var signalDispersalFactory: Double?
    
    var parentObjectId: Int?
    
    var coreBeacon: CLBeacon?
    
    var coordinates: [CGPoint]
    
    var addingTimeStamp: Date
    
    func recalculateDistance() {
        
    }
    
    init(i: Int) {
        self.uuid = UUID().uuidString
        minor = 2*i
        major = 3*i
        rssi = 4*i
        rssiCalculated = Double(5*i)
        distanceCalc = Double(5*i)
        distanceOldAlg = Double(7*i)
        multiplication = Double(8*i)
        measurePower = 9*i
        txPowerDistance = 10*i
        signalDispersal = Double(11*i)
        txPowerFactory = 12*i
        signalDispersalFactory = Double(13*i)
        parentObjectId = 14*i
        coordinates = [CGPoint(x: i, y: 13*i)]
        addingTimeStamp = Date()
    }
    
}
