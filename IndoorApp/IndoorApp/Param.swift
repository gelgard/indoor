//
//  Param.swift
//  IndoorApp
//
//  Created by Oleg on 11.02.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit

class Param : IParam {
  
  // MARK: - Properties
  
  var privateName : String?
  var privateDescription : String?
  var privateSponsor : Bool?
  var privateModel : String?
  var privateOpenTime : Date?
  var privateCloseTime : Date?
  var privateMainImage : UIImage?
  var privateGroupColor : UIColor?
  
  var name : String? {
    return self.privateName
  }
  var description : String? {
    return self.privateDescription
  }
  var sponsor : Bool? {
    return self.privateSponsor
  }
  var model : String? {
    return self.privateModel
  }
  var openTime : Date? {
    return self.privateOpenTime
  }
  var closeTime : Date? {
    return self.privateCloseTime
  }
  var mainImage : UIImage? {
    return self.privateMainImage
  }
  var groupColor : UIColor? {
    return self.privateGroupColor
  }
  
  // MARK: - Init & override
  
  init (params: NSDictionary) throws {
   
    if let name = params["101"] {
      self.privateName = name as? String
    }      
  }
}
