//
//  PlanView.swift
//  IndoorApp
//
//  Created by Oleg on 14.05.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import SpriteKit
import IndoorEngine
import IndoorModel

class PlanView : SKView {
  
  // MARK: - Properties
  
  var planScene : IPlanScene?
  
  //MARK: - Inner Logic
  
  func enableDebugMode(_ active: Bool) {
    self.showsDrawCount = true
    self.showsNodeCount = true
    self.showsFPS = true
    self.ignoresSiblingOrder = false
  }
  
  func drawScene(_ plan: IPlan , delegate : SceneDelegate , dbManager : IDSManager) {
    planScene = PlaneScene(plan: plan, selectableObjects: [IndTypes.ObjectType.exponent,IndTypes.ObjectType.exhibit], theme: MuseumThemeManager(), dbManager : dbManager)
    planScene?.delegateObjects = delegate
    self.presentScene(planScene as? SKScene)
  }
  
}
