//
//  LogsViewController.swift
//  IndoorExpoTestApp
//
//  Created by Kostiantyn Potravnyi on 5/23/18.
//  Copyright © 2018 Uran. All rights reserved.
//

import UIKit
import IndoorModel
//import IndoorStatistic
import IndoorAlert

class LogsViewController: UIViewController {
  
//  static func initiateFromStoryboard() -> LogsViewController {
//    return UIStoryboard.list.Logs.initiateStoryboard().initiateController(ofType: LogsViewController.self)
//  }
//  
  @IBOutlet weak var table: UITableView!
  
  @IBOutlet weak var sizeLabel: UILabel!
  
  @IBOutlet weak var sizeSlider: UISlider!
  
  let dateFormatter = DateFormatter()
  
  let cellIdentifier = "logDetailsCell"
  
  let creationLogsData: [StatisticType] = [.news, .viewedObject, .navigatedObject, .chattedObject, .visitedObject, .location]
  var logsArray: [BaseLog] = []
  
  var curentAvaregeBytesSize: Int = 0
  
  let logger: IIndoorStatistic = AppDelegate.shared.logger
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm:ss"
    
    self.table.register(cellClass: StatisticTableCell.self)
    self.table.estimatedRowHeight = 100
    self.table.rowHeight = UITableViewAutomaticDimension
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.table.reloadData()
  }
  
  func getDescriptionForSlider(value: Float) -> String {
    if value < 1 {
      return "Will send all logs data"
    } else {
      let kbSize:Float = 1024
      let mbSize:Float = kbSize * 1024
      
      let currentValue = value
      if currentValue < kbSize {
        return "\(Int(value)) bytes."
      } else if currentValue >= kbSize && currentValue < mbSize {
        return "\(Int(value/kbSize)) Kb."
      } else if currentValue < mbSize {
        return "\(Int(value/mbSize)) Mb."
      } else {
        return "Error in calculating size"
      }
    }
  }
}

extension LogsViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.creationLogsData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell: StatisticTableCell = tableView.dequeueReusableCell(for: indexPath)
    self.fillCreation(cell: cell, atRow: indexPath.row)
    
    return cell
  }
  
  private func fillCreation(cell: StatisticTableCell, atRow index: Int) {
    guard let type = self.creationLogsData.value(at: index) else { return }
    
    let parameters = StatisticGetParameters()
    parameters.types = [type]
    
    let sliderValue = cell.sizeSlider.value
    DispatchQueue.global(qos: .background).async {
      var unZippedMaxSize = 0
      var unZippedCount = 0
      var zippedCount = 0
      var selectedAmount = 0
      
      do {
        parameters.states = [.unZipped]
        unZippedMaxSize = try self.logger.getCurrentAvarageBytesSize(forParameters: parameters)
        unZippedCount = try self.logger.getCurrentCount(forParameters: parameters)
        
        selectedAmount = self.logger.getCount(forSelectedBytesSize: Int(sliderValue))
        
        parameters.states = [.zipped]
        zippedCount = try self.logger.getCurrentCount(forParameters: parameters)
        
        
      } catch {
        print(error)
      }
      
      DispatchQueue.main.async {
        cell.sizeSlider.maximumValue = Float(unZippedMaxSize)
        
        let availiableState = unZippedMaxSize != 0
        cell.sendButton.isEnabled = availiableState
        cell.sizeSlider.isEnabled = availiableState
        
        let zipDesc = self.getDescriptionForSlider(value: sliderValue)
        let desctiption = "Выбрано %i из %i записей (zip: %@)".format(with: selectedAmount, unZippedCount, zipDesc)
        
        cell.sizeLabel.text = desctiption
        
        cell.zippedStatisticsCount.text = "\(zippedCount)"
        cell.zippedDataStack.isHidden = (zippedCount == 0)
      }
      
    }
    
    cell.delegate = self
    cell.nameLabel.text = type.rawValue
  }
}

extension LogsViewController: StatisticTableCellDelegate {
  func statisticCellSendButtonTapped(_ cell: StatisticTableCell) {
    guard let indexPath = self.table.indexPath(for: cell) else { return }
    guard let type = self.creationLogsData.value(at: indexPath.row) else { return }
    
    let size = (cell.sizeSlider.value < 1) ? 0 : Int(cell.sizeSlider.value)
    
    let alert = IndoorAlertController()
    
    
    alert.showAlert(with: .info, typeText: nil, message: "Creating Zip file, please wait...", callback: {_ in })
    alert.setCloseButton(visible: false)
    
    
    let parameters = StatisticGetParameters()
    parameters.bytesSize = size
    parameters.types = [type]
    parameters.states = [.unZipped]
    
    self.logger.sendLogs(withParameters: parameters, presentationController: nil) { (sendingResult) in
      do {
        try sendingResult.resolve()
        alert.closeAlert()
        self.table.reloadData()
        
      } catch {
        alert.setCloseButton(visible: true)
        alert.showAlert(with: .warning,
                        typeText: nil,
                        message: "Sharing Zip file Error",
                        callback: {_ in alert.closeAlert()})
        print(error)
      }
    }
  }
  
  func statisticCellRestoreButtonTapped(_ cell: StatisticTableCell) {
    guard let indexPath = self.table.indexPath(for: cell) else { return }
    guard let type = self.creationLogsData.value(at: indexPath.row) else { return }
    
    let alert = IndoorAlertController()
    alert.showAlert(with: .info, typeText: nil, message: "Restoring Zippped statistics, please wait...", callback: {_ in })
    alert.setCloseButton(visible: false)
    
    
    let parameters = StatisticGetParameters()
    parameters.types = [type]
    
    do {
      try self.logger.set(new: .unZipped, forStatisticsWith: parameters)
      alert.showAlert(with: .info,
                      typeText: nil,
                      message: "Restoring Zippes statistic success",
                      callback: {_ in alert.closeAlert()})
      self.table.reloadData()
    } catch {
      alert.setCloseButton(visible: true)
      alert.showAlert(with: .warning,
                      typeText: nil,
                      message: "Restoring Zippes statistic Error",
                      callback: {_ in alert.closeAlert()})
      print(error)
    }
  }
  
  func statisticCellDeleteButtonTapped(_ cell: StatisticTableCell) {
    guard let indexPath = self.table.indexPath(for: cell) else { return }
    guard let type = self.creationLogsData.value(at: indexPath.row) else { return }
    
    let alert = IndoorAlertController()
    alert.showAlert(with: .info, typeText: nil, message: "Deleting Zippped statistics, please wait...", callback: {_ in })
    alert.setCloseButton(visible: false)
    
    
    let parameters = StatisticGetParameters()
    parameters.types = [type]
    parameters.states = [.zipped]
    
    do {
      try self.logger.deleteStatistics(withParameters: parameters)
      alert.showAlert(with: .info,
                      typeText: nil,
                      message: "Deleting Zippes statistic success",
                      callback: {_ in alert.closeAlert()})
      self.table.reloadData()
    } catch {
      alert.setCloseButton(visible: true)
      alert.showAlert(with: .warning,
                      typeText: nil,
                      message: "Deleting Zippes statistic Error",
                      callback: {_ in alert.closeAlert()})
      print(error)
    }
  }
  
  func statisticCell(_ cell: StatisticTableCell, sliderValueUpdated value: Float) {
    guard let indexPath = self.table.indexPath(for: cell) else { return }
    guard let type = self.creationLogsData.value(at: indexPath.row) else { return }
    
    do {
      let params = StatisticGetParameters()
      params.types = [type]
      params.states = [.unZipped]
      
      let allAvaliable = try self.logger.getCurrentCount(forParameters: params)
      let selectedAmount = self.logger.getCount(forSelectedBytesSize: Int(value))
      let zipDesc = self.getDescriptionForSlider(value: value)
      let desctiption = "Выбрано %i из %i записей (zip: %@)".format(with: selectedAmount, allAvaliable, zipDesc)
      
      cell.sizeLabel.text = desctiption
      
    } catch {
      print(error)
    }
  }
  
  
}

extension LogsViewController: UITableViewDelegate {
  
  func createLog(withTypeAtIndex index: Int) {
//    guard let type = self.creationLogsData.value(at: index) else { return }
//    
//    switch type {
//    case .news:             logger.addNewsLog(newsId: 15)
//    case .viewedObject:     logger.addViewedObjectLog(objectId: 16)
//    case .navigatedObject:  logger.addNavigatedObjectLog(objectId: 17)
//    case .chattedObject:    logger.addChattedObjectLog(objectId: 18)
//    case .visitedObject:    logger.addVisitedObjectLog(objectId: 19)
//    case .location:         logger.addLocationLog(coordinates: .init(x: 12, y: 1.5), steps: 300)
//    }
  }
}

