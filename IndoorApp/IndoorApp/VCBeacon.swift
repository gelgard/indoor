//
//  VCBeacon.swift
//  IndoorApp
//
//  Created by Oleg on 10.03.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import SpriteKit
import CoreLocation
import CoreBluetooth
import IndoorEngine
import IndoorModel
import IndoorPlanBuilder

class VCBeacon:  VCBase
{
  // MARK: - Outlets
  
  @IBOutlet weak var beaconsText : UITextView!
  @IBOutlet weak var loadDataBtn : UIButton!
  
  // MARK: - Properties

  var hardwareData : iHardwareData?
  var plan : IPlan?

  // MARK: - Init & override
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //let planBuilder : PlanBuilder = PlanBuilder()
    do {
        //self.plan = try planBuilder.getPlan(hardcodeJSON)
      let builder = try IndoorPlanBuilder.init(locale: DBManagerConstants.localeForDevice!, folderPath: "")
      self.plan = try builder.buildPlan(withPlanId: self.settings.currentPlan) as? MutablePlan
      hardwareData = HardwareDataManager(beaconsInLayer: (self.plan?.beaconsInInfoLayer)!, indoorAlgorithm: nil)
        
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    hardwareData!.startMonitoringHardwareWith(BeaconConstant.uuid, major: CLBeaconMajorValue((self.plan?.beacon_major)!), minor: nil, stepCouting: false)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    hardwareData!.stopMonitoringHardware()
  }

  deinit {
    print("VCBeacon deinit")
  }

  //MARK - Actions
  
  @IBAction func loadDataFromBeaconStack(_ sender : AnyObject) {
    do {
        beaconsText.text = try hardwareData!.getBeaconStackAsCSV()
    } catch let error as IErrorTypeWithNSError {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstant.errorNotification), object: error.getAsNSError())
    } catch {
        
    }
  }
  
}
