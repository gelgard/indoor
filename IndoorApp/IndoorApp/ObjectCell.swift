//
//  CollectionViewCell.swift
//  LGLinearFlowView
//
//  Created by Luka Gabric on 16/08/15.
//  Copyright © 2015 Luka Gabric. All rights reserved.
//

import UIKit

public class ObjectCell: UICollectionViewCell {
    @IBOutlet public weak var labelObject: UILabel!
}
