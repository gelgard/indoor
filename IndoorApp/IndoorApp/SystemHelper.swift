//
//  SystemHelper.swift
//  IndoorApp
//
//  Created by Oleg on 15.09.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
class SystemHelper {
  static func isTestProcessing() -> Bool {
    let envir = ProcessInfo.processInfo.environment
    let targetName = envir["targetName"]
    return targetName == DebugConstant.testFlag
  }
  
  static func degreesToRadians(_ degrees: CGFloat) -> CGFloat {
    return CGFloat(degrees * CGFloat(M_PI) / 180)
  }
  
  static func radiansToDegrees(_ radians: CGFloat) -> CGFloat  { return (radians * 180) / CGFloat(M_PI) }
  
  static func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
  }

}
