//
//  DLDemoMenuViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit

class VCMenu: VCBase {
  
  // MARK: - Outlets

  @IBOutlet weak var tableView: UITableView!
  


  // MARK: - Init

  override func viewDidLoad() {
      super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
  }

  // MARK: - Navigation

//  func mainNavigationController() -> DLHamburguerNavigationController {
//      return self.storyboard?.instantiateViewControllerWithIdentifier("DLHamburguerNavigationController") as! DLHamburguerNavigationController
//  }
  
}



extension VCMenu : UITableViewDelegate, UITableViewDataSource {
  
  // MARK: - UITableViewDelegate&DataSource methods
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return segues.count
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)
    cell.textLabel?.text = seguesСaptions[(indexPath as NSIndexPath).row]
    return cell
    
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    
    
    if let hamburguerViewController = self.findHamburguerViewController() {
      hamburguerViewController.hideMenuViewControllerWithCompletion({ () -> Void in
//        nvc.visibleViewController!.performSegueWithIdentifier(segues[indexPath.row], sender: nil)
//        hamburguerViewController.contentViewController = nvc
        self.navController!.visibleViewController!.performSegue(withIdentifier: segues[(indexPath as NSIndexPath).row], sender: nil)
        

      })
    }
    
  }
  
}
