//
//  VCMotion.swift
//  IndoorApp
//
//  Created by Oleg on 08.08.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import IndoorEngine
import IndoorModel
import IndoorPlanBuilder
import Zip

class VCMotion: VCBase {
  
  // MARK: - Outlets
  @IBOutlet weak var intervalLbl: UILabel!
  @IBOutlet weak var stepCount: UILabel!
  @IBOutlet weak var updateInterval: UILabel!
  @IBOutlet weak var actnBtn : UIButton!
  @IBOutlet weak var intervalSldr: UISlider!
  @IBOutlet weak var accX : UILabel!
  @IBOutlet weak var accY : UILabel!
  @IBOutlet weak var accZ : UILabel!
  @IBOutlet weak var gravX : UILabel!
  @IBOutlet weak var gravY : UILabel!
  @IBOutlet weak var gravZ : UILabel!
  @IBOutlet weak var rotX : UILabel!
  @IBOutlet weak var rotY : UILabel!
  @IBOutlet weak var rotZ : UILabel!
  @IBOutlet weak var atitPitch : UILabel!
  @IBOutlet weak var atitRoll : UILabel!
  @IBOutlet weak var atitYaw : UILabel!
  
  
  var interval : Double = 0.7
  var emailHelper : EmailHelper?
  var timer : Timer?
   var hardwareData : iHardwareData?
  //var mutablePlan : MutablePlan?
  
  // MARK: - Properties
  var motionMngr : IMotionManager? = nil
  var isGettingData = false
  var plan : IPlan?
  var motionData : [iMotion] = []

  
  // MARK: - Init & override
  
  override func viewDidLoad() {
    super.viewDidLoad()
    actnBtn.setTitle("START".localized, for: UIControlState())
    
    motionMngr = MotionManager(delegete: self, updateInterval:interval )

    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    intervalSldr.value = Float(interval)
    updateSLiderLbl()
    self.updateInterval.text = "\(CalculationConstant.updatePeriod)"
//    let planBuilder : PlanBuilder = PlanBuilder()
//    do {
//      mutablePlan = try planBuilder.getPlan(hardcodeJSON)
//      
//      
//    } catch let error {
//      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
//    }

    
    
    
    
    //motionMngr?.startMonitoring()
    
  }
  
  @objc func calculateStepCount () {
    timer?.invalidate()
    let stepCount = Pedometr().getStepCount(motionData: motionData)
    print("MOTION DATA: \(motionData.count) STEP COUNT \(stepCount)")
    self.stepCount.text = "\(stepCount)"
    motionData.removeAll()
    startTimer()
    
  }
  
  func startTimer() {
  timer = Timer.scheduledTimer(timeInterval: CalculationConstant.updatePeriod, target: self, selector: #selector(calculateStepCount), userInfo: nil, repeats: false)
  
}

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    
  }
  
  deinit {
    print("VCCalibrate deinit")
  }
  
  //MARK: - Actions
  
  @IBAction func changeInterval(_ sender: AnyObject) {
    interval = Double((sender as! UISlider).value)
    updateSLiderLbl()
    
  }
  
  @IBAction func switchAction(_ sender: AnyObject) {
      switchBtn()
  }
  
  //MARk: inner logic
  
  func updateSLiderLbl() {
    intervalLbl.text = String(format: "%.1f",interval)
  }
  
  func sendEmailWithData() {
    do {
      let logContent = try motionMngr?.getMotionDataAsCSVAndClear()
      
      
      let nameForFile = "Sensors_data_\(Date().asStringForName())"
      
      let filename =  SystemHelper.getDocumentsDirectory().appendingPathComponent("\(nameForFile).csv")
      try logContent?.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
      let zipFilePath = try Zip.quickZipFiles([filename], fileName: "\(nameForFile).zip")
      
      
      emailHelper = EmailHelper(zipFile: zipFilePath, vc: self, filePrefix: "Sensors_data", fileExtension: "zip")
      emailHelper!.sendEmail()
      
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }

    
  }
  
  func switchBtn() {
    motionData.removeAll()
    if isGettingData {
      isGettingData = false
      motionMngr?.stopMonitoring()
      sendEmailWithData()
      actnBtn.setTitle("START".localized, for: UIControlState())
      timer?.invalidate()
      
    } else {
      isGettingData = true
      motionMngr?.startMonitoring()
      actnBtn.setTitle("STOP".localized, for: UIControlState())
      startTimer()
    }
  }
  
  //MARK: - Block
  
  
  func getRotateDegree(_ fromYAxis: CGFloat) {
    print("degrees \(fromYAxis)")
    //HARDCODE
//    let angle = currentAngle - fromYAxis
//    currentAngle = fromYAxis
//    self.run(SKAction.rotate(byAngle: degreesToRadians(angle), duration: 0.1))
    
    
  }

}

extension VCMotion : MotionManagerDelegate
{
  //MARK: - DebugPanelDelegate
  func receivedMotion(_ data : iMotion) {
    motionData.append(data)
    accX.text = String(format: "%.3f", data.accelerationX)
    accY.text = String(format: "%.3f", data.accelerationY)
    accZ.text = String(format: "%.3f", data.accelerationZ)
    gravX.text = String(format: "%.3f", data.gravityX)
    gravY.text = String(format: "%.3f", data.gravityY)
    gravZ.text = String(format: "%.3f", data.gravityZ)
    rotX.text = String(format: "%.3f", data.rotationX)
    rotY.text = String(format: "%.3f", data.rotationY)
    rotZ.text = String(format: "%.3f", data.rotationZ)
    atitPitch.text = String(format: "%.3f", data.altitudePitch)
    atitRoll.text = String(format: "%.3f", data.altitudeRoll)
    atitYaw.text = String(format: "%.3f", data.altitudeYaw)
    
    //print("RECIEVE DATA ATIT PITCH:\(data.altitudePitch) PITCH:\(data.altitudeRoll) PITCH:\(data.altitudeYaw)")
  }

  
}

extension VCMotion : BeaconMonitoringDelegate {
  //MARK: - BeaconMonitoringDelegate
  func monitoring(_ active: Bool) {
    
  }
}
