//
//  DebugPanel.swift
//  IndoorApp
//
//  Created by Oleg on 16.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import IndoorEngine
import IndoorModel


class DebugPanel: UIView {
  
  // MARK: - Outlets
  
  @IBOutlet weak var view: UIView!
  @IBOutlet weak var debugTable: UITableView!
  @IBOutlet weak var debugTextView: UITextView!
  @IBOutlet weak var magneticHeading: UILabel!
  @IBOutlet weak var latVal: UILabel!
  @IBOutlet weak var longVal: UILabel!
  @IBOutlet weak var xVal: UILabel!
  @IBOutlet weak var yVal: UILabel!
  
  //MARK: - Properties
  
  var delegate : DebugPanelDelegate?
  fileprivate var debugDataSource: DebugTableViewData = DebugTableViewData()
  fileprivate var debugDelegate: DebugTableViewDelegate!
  fileprivate var debugDS : [iDebugLog] = []
  
  
  //MARK : - Init
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
  }
  
  required  init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    xibSetup()
  }
  
  func xibSetup() {
    view = loadViewFromNib()
    view.frame = bounds
    view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
    
    addSubview(view)
    initDebugTable()
    if !SystemHelper.isTestProcessing() {
        NotificationCenter.default.addObserver(self, selector: NotificationConstant.updateLogNotificationSelector, name:NSNotification.Name(rawValue: NotificationConstant.updateLogNotification), object: nil)
    }
    
  }
  
  func loadViewFromNib() -> UIView {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "DebugPanel", bundle: bundle)
    
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }
  
  func initDebugTable() {
    debugDelegate = DebugTableViewDelegate(selectedBlock: objectSelected)
    debugTable.register(DebugCell.self, forCellReuseIdentifier: textCellIdentifier)
    let yourNibName = UINib(nibName: textCellIdentifier, bundle: nil)
    debugTable.register(yourNibName, forCellReuseIdentifier: textCellIdentifier)
    debugDataSource.logData = debugDS
    debugTable.dataSource = debugDataSource
    debugTable.delegate = debugDelegate
    debugTable.reloadData()
  }
  
  //MARK : - Inner logic
  
  func updateData() {
    DispatchQueue.main.async(execute: {
      self.debugDataSource.logData = self.debugDS
      self.debugTable.reloadData()
      let indexPath = IndexPath(item: self.debugDS.count-1, section: 0)
      self.debugTable.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: false)
    })
  }
  
  //MARK: Block
  
  func objectSelected(_ index: Int){
    let pointLog = debugDS[index].calculationLog
    var resText = ""
    for line in pointLog{
      resText += "\(line) \n"
      
    }
    debugTextView.text = resText
    UIPasteboard.general.string = resText
  }
  
  //MARK: - Actions
  
  @IBAction func loadDtaAndStartLocate(_ sender: AnyObject) {
    do {
      let debugData = try BeaconDebugData(json: debugJSON)
      self.delegate?.startLocateFromDebug(debugData)
      
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }

    
  }
  
  //MARK: - Notifications selectors
  
  @objc func updateLog(_ notification: Notification) {
    if (notification as NSNotification).userInfo!["log"] != nil {
      let strucVal : iDebugLog = ((notification as NSNotification).userInfo!["log"] as! WrapperStruct).wrappedValue
      debugDS.append(strucVal)
      updateData()
    }
    
  }

}

