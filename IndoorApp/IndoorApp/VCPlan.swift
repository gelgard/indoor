//
//  DLDemoMainContentViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import CoreLocation
import IndoorEngine
import IndoorModel
import IndoorPlanBuilder
import IndoorNavigationView
import Zip
import IndoorHardwareMonitoring
import IndoorCDManager
import IndoorStatistic



class VCPlan:  VCBase {
  
  // MARK: - Outlets
  
  @IBOutlet weak var navigationView: NavigationView!
  @IBOutlet weak var monitoringButton: UIButton! //-
  @IBOutlet weak var whereButton: UIButton!
  @IBOutlet weak var addTestLog: UIButton! //-
  @IBOutlet weak var goButton: UIButton! //-
  @IBOutlet weak var infoPanel: InfoPanel! //-
  @IBOutlet weak var debugPanel: DebugPanel! //-
  @IBOutlet weak var infoPanelHeight : NSLayoutConstraint! //-
  
  @IBOutlet weak var debugPanelWidth : NSLayoutConstraint! //-
  
  // MARK: - Properties
  let folderPath = "images"
  var mutablePlan : MutablePlan?
  var emailHelper : EmailHelper?
  var plan : IPlan?
  var hardwareData : iHardwareData?
  var hardwareMonitoring : IHardwareMonitoring?
  var timer : Timer?
  var timerHeading : Timer?
  
  fileprivate var beaconDataSource : IndTypes.BeaconDataSourceType = IndTypes.BeaconDataSourceType.notSet
  fileprivate var beaconDebugData : BeaconDebugData?
  fileprivate var beaconsInRoom : [IObject] = []
  fileprivate var firstLaunch = true
  var dbManager : IDSManager?
  
  var prevAngle : CGFloat? = nil
  var firstRotate : Bool = false
  
  fileprivate var actionState : IndTypes.ActionStateType = IndTypes.ActionStateType.stop //-
  fileprivate var stopTime : TimeInterval = Date().timeIntervalSince1970 //-
  fileprivate var goTime : TimeInterval  = Date().timeIntervalSince1970 //-
  fileprivate var stopPoint : [Int] = []
  
  var logger : IIndoorStatistic?
  
  // MARK: - Init & override
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    infoPanel.storyBrd = self.storyboard
    infoPanel.navig = self.navigationController
    debugPanel.delegate = self
    logger = AppDelegate.shared.logger
    
    whereButton.isHidden = true
    do {
      
      
      
    } catch let error {
      
      if error is IErrorTypeWithNSError {
        _ = ErrorHelper.errorProcessing(err: error, fileName: #file, funcName: #function, line: String(#line))
      }
      
      
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }
    
    self.navigationView.touchPoint = self.touchPoint(_:)
   
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
   // guard (self.navigationView.planViewSK as? SKView) != nil else { return }
    if firstLaunch {
      print("\(UIScreen.main.nativeBounds.width)")
      print("\(self.view.frame)")
      //self.navigationView.planViewSK.planScene?.parentFrame = UIScreen.main.nativeBounds
      self.navigationView.planViewSK.planScene?.parentFrame = self.view.frame
    //  self.navigationView.planViewSK.planScene?.setInitialScale()
      firstLaunch = false
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    NotificationCenter.default.addObserver(self, selector: NotificationConstant.nearByBeaconSelector, name:NSNotification.Name(rawValue: NotificationConstant.nearByBeacon), object: nil)
    NotificationCenter.default.addObserver(self, selector: NotificationConstant.centeredSettingsSelector, name:NSNotification.Name(rawValue: NotificationConstant.centeredSettings), object: nil)
//    NotificationCenter.default.addObserver(self, selector: NotificationConstant.unselectBeaconSelector, name:NSNotification.Name(rawValue: NotificationConstant.unselectBeacon), object: nil)
    var currentPlan : IndoorModel.PlanDataInsertWrapper?
    if self.dbManager == nil {
      do {
        
        self.dbManager = IndoorCDManager()
        
        
        
        
        
        
        
      } catch let error  {
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        self.findHamburguerViewController()?.showMenuViewController()
        self.dbManager = nil
        return
        
      }
    }
    do {
      currentPlan = try self.dbManager?.planDataStorage.getCurrentPlan()
    } catch let error {
      
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
      
    }

    
    
    if currentPlan == nil && hardwareMonitoring == nil {
      _ = ErrorHelper.errorProcessing(err: IndError.ErrorPlanObject.planAbsent, fileName: #file, funcName: #function, line: String(#line))
      self.findHamburguerViewController()?.showMenuViewController()
      self.dbManager = nil
    } else {
      do {
        let builder = try IndoorPlanBuilder.init(locale: DBManagerConstants.localeForDevice!, folderPath: "")
        let mutablePlan = try builder.buildPlan(withPlanId:(currentPlan?.plan_id)!) as? MutablePlan
        
        self.plan = try mutablePlan!.getImmutablePlan()
    
        try navigationView.planSetup(plan: self.plan!, withTheme: self.museumTheme, dbManager: dbManager!)
        
        let unixJSON : IUnityHelper = UnityHelper(plan: self.plan!)
        
        
      } catch let error {
        
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        
      }

      
    }
    
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    
  }
  
  
  override func viewWillDisappear(_ animated: Bool)
  {
    super.viewWillDisappear(animated)
    //    if beaconData != nil {
    //        beaconData!.stopMonitoringBeacon()
    //    }
    
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.nearByBeacon), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.unselectBeacon), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationConstant.centeredSettings), object: nil)
  }
  
  override func rotated() {
    
  }
  
  override func updateControlsFromSettings() {
    //-
    if settings.showDebugPanel {
      debugPanelWidth.constant = CGFloat(kDebugWidth)
    } else {
      debugPanelWidth.constant = 0
    }
    //-
  }
  //MARK: - Notifications selectors
  
  @objc func centeredSettings(_ notification: Notification) {
    
    if hardwareMonitoring != nil {
      if (hardwareMonitoring?.isMonitoring)! {
        whereButton.isHidden = (notification.userInfo!["value"] as! Bool)
      }
    }
    
  }
  
  //MARK: - Monitorig
  
  func startCollectData() throws {
    
    
    
    
    
  }
  
  //MARK: - Monitoring closure
  
  func currentLocation(loc: IndAliases.LocationResult, maxRelocation: Double){
    print("CURLOC \(loc)")
    self.navigationView.placeMarkers(location: loc)
    print("Statistic loc point:\(loc.point)");
    DispatchQueue.global().async { 
      self.logger?.addLocationLog(coordinates: loc.point, steps: Int(maxRelocation))
    }
  }
  
  func intersectedObjects(objects: [IObject]) {
    self.navigationView.selectObjects(objects: objects)
    
    DispatchQueue.global().async {
      print("Statistic objects : \(objects.description)");
      for object in objects{
        self.logger?.addVisitedObjectLog(objectId: object.object_id)
   
      }
      
    }
  }
  
  func newAngle(value: CGFloat) {
    self.navigationView.rotatePlan(angleInDegree: value)
  }
  
  //MARK: - Info PAnel
  
  func showInfoPanel(_ objects : [IObject]) {
    
    infoPanel.loadData(objects: objects)
    infoPanelHeight.constant = infoPanelHeightValue
    UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions(), animations: {
      self.view.layoutIfNeeded()
    }, completion: nil)
    
  }
  
  func closeInfoPanel() {
    infoPanelHeight.constant = 0
    UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions(), animations: {
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
  
  //MARK: - Actions
  
  @IBAction func whereIam (_ sender: Any) {
    self.navigationView.locateAtMarker()
    whereButton.isHidden = true
  }
  
  @IBAction func addTestLog(_ sender: AnyObject) {
    
      for i in 0..<3000 {
        print(i)
        self.logger?.addNewsLog(newsId: i)
        self.logger?.addViewedObjectLog(objectId: i*2)
        self.logger?.addChattedObjectLog(objectId: i*3)
        self.logger?.addVisitedObjectLog(objectId: i*4)
        self.logger?.addNavigatedObjectLog(objectId: i*5)
        self.logger?.addLocationLog(coordinates: CGPoint.init(x: i+3, y: i*2), steps: i)
      }
      

    
    
    
  }
  
  @IBAction func menuButtonTouched(_ sender: AnyObject) {
    
    self.findHamburguerViewController()?.showMenuViewController()
  }
  
  @IBAction func switchAction(_ sender : AnyObject) {
    if actionState == IndTypes.ActionStateType.go {
      actionState = IndTypes.ActionStateType.stop
      goButton.setImage(UIImage(named: "go"), for: UIControlState())
      goButton.alpha = 1
      stopTime = Date().timeIntervalSince1970
      
      if hardwareMonitoring != nil {
        do {
          try hardwareMonitoring!.indoorAlg?.debugData?.addPathData(true, startTime: goTime, endTime: stopTime, stopPoint: nil)
        } catch let error {
          
          _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
          
        }

        
      }

      
    } else {
      actionState = IndTypes.ActionStateType.go
      goButton.setImage(UIImage(named: "stop"), for: UIControlState())
      goButton.alpha = 0
      goTime = Date().timeIntervalSince1970
      
      if hardwareMonitoring != nil {
        do {
          try hardwareMonitoring!.indoorAlg?.debugData?.addPathData(false, startTime: stopTime, endTime: goTime, stopPoint: stopPoint)
        } catch let error {
          
          _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
          
        }

        
      }

    }
    
  }
  
  @IBAction func locate(_ sender : AnyObject) {
    goTime = Date().timeIntervalSince1970
    settings.centerAtPosition = true
      
      if hardwareMonitoring == nil {
        do {
          hardwareMonitoring = HardwareMonitoring(plan: self.plan!, dbManager: dbManager!)
          hardwareMonitoring?.newAngle = newAngle
          hardwareMonitoring?.currentIndoorPosition = currentLocation
          hardwareMonitoring?.intersected = intersectedObjects
          hardwareMonitoring?.switchMonitoring = self.monitoring(_:) //TODO разообраться
          hardwareMonitoring?.debugVc = self
          hardwareMonitoring?.crntGPSLocation = self.gpsLocation(locValue:)
          
//          let major = CLBeaconMajorValue((self.plan!.beacon_major))
//          try hardwareMonitoring?.startMonitoring(BeaconConstant.uuid, major: major, stepCouting: true, intersectionWith: [IndTypes.ObjectType.exhibit])
          
        } catch let error {
          _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        }
        
      }
      
      
      
    
    if hardwareMonitoring!.isMonitoring == false {
      do {
        let major = CLBeaconMajorValue((self.plan!.beacon_major))
        try hardwareMonitoring!.startMonitoring(BeaconConstant.uuid, major: major, stepCouting: true, intersectionWith: [IndTypes.ObjectType.exhibit,IndTypes.ObjectType.exponent])
        navigationView.isMonitoring = true
        try hardwareMonitoring?.indoorAlg?.debugData?.startNewLog(planId: (self.plan?.planId)!)
      } catch let error {
        
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        
      }
    } else {
      do {
        try hardwareMonitoring!.stopMonitoring()
        navigationView.isMonitoring = false
        hardwareMonitoring?.indoorAlg?.debugData?.finishCurrentLog()
        hardwareMonitoring = nil
      } catch let error {
        
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        
      }

    }
    
  }
  
  //MARK: - Inner Logic
  
  
  
  func switchMonitoringButton(_ active : Bool) {
    guard (hardwareMonitoring != nil) else {
      return
    }
    
    if hardwareMonitoring?.isMonitoring == false {
      monitoringButton.setImage(UIImage(named: "run"), for: UIControlState())
      let loc = CGPoint(x: -1000, y: -1000)
      /*
       self.planView.planScene?.placeMarker(withType: .locationMarker, atPoint: loc)
       */
      do {
        try hardwareMonitoring?.indoorAlg?.debugData?.addPathData(false, startTime: stopTime, endTime: goTime, stopPoint: stopPoint)
      } catch let error {
        
        _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
        
      }

 
      //self.navigationView.position.debugData?.generateEmail()
 //отправка емэйла
//      do {
//        let logContent = hardwareMonitoring?.position?.debugData?.getJSONString()
//
//      let nameForFile = "Debug_Info_\(Date().asStringForName())"
//      print("\(nameForFile).json")
//      let filename =  SystemHelper.getDocumentsDirectory().appendingPathComponent("\(nameForFile).json")
//        try logContent?.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
//      let zipFilePath = try Zip.quickZipFiles([filename], fileName: "\(nameForFile).zip")
//
//
//      emailHelper = EmailHelper(zipFile: zipFilePath, vc: self, filePrefix: "Debug_Info", fileExtension: "zip")
//      emailHelper!.sendEmail()
//
//    } catch let error {
//      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
//    }
      
    } else {
      monitoringButton.setImage(UIImage(named: "pause"), for: UIControlState())
    }
  }
  
  
  
  //MARK: - Closure
  
  
  func gpsLocation(locValue:CLLocationCoordinate2D) {
    self.debugPanel.latVal.text = String(describing: locValue.latitude)
    self.debugPanel.longVal.text = String(describing: locValue.longitude)
  }
  
  func touchPoint(_ point: CGPoint) {

    self.debugPanel.xVal.text = String(describing: point.x)
    self.debugPanel.yVal.text = String(describing: point.y)
    /*
     self.planView.planScene?.placeMarker(withType: .realPosMarker, atPoint: point)
     self.planView.planScene?.placeMarker(withType: IndTypes.MarkersTypes.lookMarker, atPoint: point)
     */
    //TODO временна для анализа сделано, что комната определяется по тапу
    
    /*
     if self.planView.planScene?.nodeDependencies != nil {
     beaconsInRoom.removeAll()
     let beaconsArr : [IObject] = (planView.planScene?.nodeDependencies?.getObjectsInRoom(byPoint: point, withType: IndTypes.ObjectType.beacon))!
     beaconsInRoom.append(contentsOf: beaconsArr)
     }
     */
    //
    
  
    
    
    
    stopPoint.removeAll()
    stopPoint.append(Int(point.x / ConstantsSingelton.sharedInstance.planScale))
    stopPoint.append(Int(point.y / ConstantsSingelton.sharedInstance.planScale))
    
    if monitoringButton.alpha == 0 {
      monitoringButton.alpha = 1
      stopTime = Date().timeIntervalSince1970
      goButton.alpha = 1
      
    } else {
      guard hardwareMonitoring != nil else {
        return
      }
      if hardwareMonitoring!.isMonitoring == true {
        self.switchAction(self)
      }
    }
  }
  
  
  
  
  func monitoring(_ active: Bool) {
    switchMonitoringButton(active)
  }
  
}

//-
extension VCPlan : DebugPanelDelegate
{
  //MARK: - DebugPanelDelegate
  func startLocateFromDebug(_ beaconData: BeaconDebugData) {
    beaconDataSource = IndTypes.BeaconDataSourceType.debugData
    self.beaconDebugData = beaconData
    guard (hardwareMonitoring != nil) else {
      return
    }
    do {
      let major = CLBeaconMajorValue((self.plan!.beacon_major))
      try hardwareMonitoring?.startMonitoring(BeaconConstant.uuid, major: major, stepCouting: true, intersectionWith: [IndTypes.ObjectType.exhibit])
    } catch let error {
      
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
      
    }

    
    //hardwareMonitoring!.startMonitoring()
  }
}
//-


