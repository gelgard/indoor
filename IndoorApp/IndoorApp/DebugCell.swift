//
//  DebugCell.swift
//  IndoorApp
//
//  Created by Oleg on 17.07.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit

class DebugCell: UITableViewCell {
  
  // MARK: - Outlets
  
  @IBOutlet weak var pointIdLbl: UILabel!
  @IBOutlet weak var prevPointLbl: UILabel!
  @IBOutlet weak var curPointLbl: UILabel!
   @IBOutlet weak var stepCount: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  

  
  
}
