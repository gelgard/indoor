 //
//  VCCalibrate.swift
//  IndoorApp
//
//  Created by Oleg on 26.03.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import IndoorEngine
import IndoorModel

class VCCalibrate: VCBase {
  
  // MARK: - Outlets
  
  @IBOutlet weak var calibrateText : UITextView!
  @IBOutlet weak var calibrateBtn : UIButton!
  @IBOutlet weak var majorText : UITextField!
  @IBOutlet weak var minorText : UITextField!
  @IBOutlet weak var infoText : UILabel!

  // MARK: - Properties
  
  var isCalibrating = false
  let hardwareData : iHardwareData = BeaconDataManagerCalibrate(beaconsInLayer: [IBeacon](), indoorAlgorithm: nil)

  // MARK: - Init & override
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    calibrateText.text = ""
    calibrateBtn.setTitle("START".localized, for: UIControlState())
  }
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)

    hardwareData.stopMonitoringHardware()
  }

  deinit {
    print("VCCalibrate deinit")
  }

  //MARK: - Actions
  
  @IBAction func calibrate(_ sender : AnyObject) {
    switchBtn()
  }

  //MARK: - Inner logic
  
  func switchBtn()
  {
    self.view.endEditing(true)
    if !self.isCalibrating {
      startCalibrating()
    } else {
      endCalibrating()
    }
  }

  func startCalibrating() {
    calibrateBtn.setTitle("STOP".localized, for: UIControlState())
    self.isCalibrating = true
    hardwareData.startMonitoringHardwareWith(BeaconConstant.uuid, major: UInt16(majorText.text!)! as CLBeaconMajorValue, minor:  UInt16(minorText.text!)! as CLBeaconMajorValue, stepCouting: false)
    infoText.text = "Держите устройство на расстоянии одного метра от Beacon"
    Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(VCCalibrate.moveDevice), userInfo: nil, repeats: false)
  }

   @objc func endCalibrating() {
    calibrateBtn.setTitle("START".localized, for: UIControlState())
    
    do {
        try fillWithData(hardwareData.getBeaconsStack())
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }

    
    self.isCalibrating = false
  }

  @objc dynamic func moveDevice() {
    do {
        let data = try hardwareData.getBeaconsStack()
        print("DATA COUNT : \(data.count)")
    } catch let error {
      _ = ErrorHelper.errorProcessing(err: error as AnyObject, fileName: #file, funcName: #function, line: String(#line))
    }

    infoText.text = "Перемещайте устройстов на 15 см вправо- влево"
    Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(VCCalibrate.endCalibrating), userInfo: nil, repeats: false)
  }

  func fillWithData(_ data : [BeaconLogs]) {
    var resString : String = ""
    var summary : Int = 0
    var countAvg : Int = 0
    print("DATA COUNT : \(data.count)")
    for beacon in data {
        if let _ = beacon.beacon.coreBeacon {
            if beacon.beacon.coreBeacon?.rssi != 0 {
                summary += beacon.beacon.coreBeacon!.rssi
                countAvg += 1
            }
            resString += "\(String(beacon.beacon.coreBeacon!.rssi))\n"
        }
    }
    calibrateText.text = resString
    
    if data.count == 0 {
      infoText.text = "Can`t find beacon"
    } else {
      let average : Double =  Double(summary / countAvg)
      infoText.text = "Tx Power: \(average)"
    }
    
  }
}
