//
//  ViewController.swift
//  IndoorApp
//
//  Created by Oleg on 13.12.15.
//  Copyright © 2015 UranCompany. All rights reserved.
//

import UIKit

class ViewController: DLHamburguerViewController {

    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
  


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func awakeFromNib() {
        self.contentViewController = self.storyboard!.instantiateViewController(withIdentifier: "DLHamburguerNavigationController")
      let menu = self.storyboard!.instantiateViewController(withIdentifier: "MenuController") as! VCMenu
      
      menu.navController = self.contentViewController as? DLHamburguerNavigationController
      
      self.menuViewController = menu

    }

}
