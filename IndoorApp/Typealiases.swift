//
//  Typealiases.swift
//  Indoor_constants
//
//  Created by Oleg on 19.11.16.
//  Copyright © 2016 UranCompany. All rights reserved.
//

import Foundation
import SpriteKit
import CoreLocation


//MARK: Typealias

  typealias KeyType = (key : String, type : JsonTypes)
  typealias AvgDistance = (distance : Double, values : Int)
  typealias FillingCoordinate = (x : Int, y : Int)
  
  typealias JsonKeyParams = (key : String, paramList : [KeyType])
  public typealias SelectedIndex = (_ index: Int) -> Void
  typealias ObjectTheme = (borderColor: SKColor, borderWidth : CGFloat,fillColor : SKColor, labelColor : SKColor)
  typealias LocationMarkerTheme = (borderColor: SKColor, borderWidth : CGFloat, mainColor : SKColor, outerColor : SKColor)
  typealias LookMarkerTheme = (borderColor: SKColor, fillColor : SKColor, borderWidth : CGFloat)
  typealias LayerNodeTheme = (bgColor: SKColor,fillSquareColor : SKColor)
  public typealias BeaconList = (_ list: [CLBeacon], _ heading : CGFloat) -> Void
  public typealias YAxisDegree = (_ degreeFromYAxis: CGFloat) -> Void


